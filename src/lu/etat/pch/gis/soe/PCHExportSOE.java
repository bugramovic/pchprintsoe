package lu.etat.pch.gis.soe;

import com.esri.arcgis.carto.Map;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polyline;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.IServerObjectExtension;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;
import com.esri.arcgis.system.IObjectConstruct;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.IRESTRequestHandler;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.beans.height.HeightResultBean;
import lu.etat.pch.gis.beans.htmlPopup.HtmlPopupQueryResult;
import lu.etat.pch.gis.beans.pk.PKResultBean;
import lu.etat.pch.gis.soe.tasks.*;
import lu.etat.pch.gis.soeClients.IPCHExportSOE;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonEnvelope;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonPolyline;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@ArcGISExtension
@ServerObjectExtProperties(
        displayName = "PCHExportSOE",
        allSOAPCapabilities = {
//                "getMetadataByID",
                "getPKsByCoord", "getPointForPK", "getLineForPKs",
                // "queryHtmlPopup",
                "getHeightForPoint", "getHeightForLine"
//                "exportLayers", "getLayerCount"
//                "1","2","3","4","5"
//                "1", "2", "3", "4", "5", "6","7"
        },
        description = "PCHExportSOE desc",
        supportsMSD = true)
public class PCHExportSOE implements IServerObjectExtension, IPCHExportSOE, IRESTRequestHandler, IObjectConstruct {
    private IServerObjectHelper soHelper;
    private SOELogger logger;
    private MapServer mapServer = null;

    private CalculatePKTask calculatePkTask;
    private HeightTask heightTask;
    private LegendTask legendTask;
    private MetadataTask metadataTask;
    private HtmlPopupQueryTask htmlPopupQueryTask;

    public void init(IServerObjectHelper soh) throws IOException {
        this.soHelper = soh;
        this.logger = new SOELogger(ServerUtilities.getServerLogger(), 8000);
        logger.debug("PCHExportSOE.init....");
        this.mapServer = (MapServer) soHelper.getServerObject();
        logger.debug("PCHExportSOE.init.finished");
    }

    public void shutdown() throws IOException {
        logger.debug("PCHExportSOE.shutdown...");
        this.soHelper = null;
        this.mapServer = null;
        logger.debug("PCHExportSOE.shutdown.finished");
    }

    public void construct(IPropertySet propertySet) throws IOException {
        logger.debug("PCHExportSOE.construct...");
        calculatePkTask = new CalculatePKTask(logger, mapServer, mapServer.getConfigurationName());
        heightTask = new HeightTask(logger, mapServer, mapServer.getConfigurationName());
        legendTask = new LegendTask(logger, mapServer, mapServer.getConfigurationName());
        metadataTask = new MetadataTask(logger, mapServer, mapServer.getConfigurationName());
        htmlPopupQueryTask = new HtmlPopupQueryTask(logger, mapServer, mapServer.getConfigurationName());
        htmlPopupQueryTask.cleanUpHtmlPopupOutputs();
        logger.debug("PCHExportSOE.construct.finished");
    }


    @Override
    public String getSchema() throws IOException {
        logger.debug("PCHExportSOE.getSchema()...");
        try {
            JSONObject pchExportSOE = RESTUtils.createResource("PCHExportSOE", "This is my first SOE", false);
            JSONArray operationArray = new JSONArray();
            operationArray.put(RESTUtils.createOperation("exportLegend", "width,height,dpi", "json,html"));
            operationArray.put(RESTUtils.createOperation("getPKsByCoord", "x,y,wkid,tolerance", "json,html"));
            operationArray.put(RESTUtils.createOperation("getPointForPK", "route,pk", "json,html"));
            operationArray.put(RESTUtils.createOperation("queryHtmlPopup", new String[]{"layerId", "text", "geometry", "objectIds", "whereClause", "outFields", "returnGeometry"}, "json,html"));

            operationArray.put(RESTUtils.createOperation("getLineForPKs", "route,pkFrom,pkTo", "json,html"));
            operationArray.put(RESTUtils.createOperation("getHeightForPoint", "x,y", "json,html"));
            operationArray.put(RESTUtils.createOperation("getHeightForLine", "geometry", "json,html"));
            operationArray.put(RESTUtils.createOperation("getMetadataByID", "layer", "json,html"));
            pchExportSOE.put("operations", operationArray);

            JSONArray subResourceArray = new JSONArray();
            subResourceArray.put(RESTUtils.createResource("exportLegends", "Export legends into d:/temp/layers", false));
            pchExportSOE.put("resources", subResourceArray);
            logger.debug("PCHExportSOE.construct.finished");
            return pchExportSOE.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            logger.debug("PCHExportSOE.construct.JSONException: " + e.getMessage());
        }
        return null;
    }

    public byte[] handleRESTRequest(String capabilities, String resourceName, String operationName, String operationInput, String outputFormat, String requestProperties, String[] responseProperties) throws IOException, AutomationException {
        try {
            logger.debug("In handleRESTRequest(): " + ";\ncapabilities: " + capabilities + ";\nresourceName: " + resourceName + ";\noperationName: " + operationName + ";\noperationName length: " + operationName.length() + ";\noperationInput: " + operationInput + ";\noutputFormat: " + outputFormat
                    + ";\nresponseProperties length: " + responseProperties.length);

            logger.debug("Operation is permitted: " + operationName);
            if (operationName.equalsIgnoreCase("getPKsByCoord")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"x", "y", "wkid", "tolerance"});
                logger.warning("Operation 'getPKsByCoord' with params: " + Arrays.toString(values) + " ...");
                try {
                    double xCoord = Double.parseDouble(values[0]);
                    double yCoord = Double.parseDouble(values[1]);
                    String wkid = values[2];
                    double tolerance = Double.parseDouble(values[3]);
                    List<PKResultBean> retString = calculatePkTask.getPKsByCoordArray(xCoord, yCoord, wkid, tolerance);
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("valid", "ok");
                    JSONArray array = new JSONArray();
                    for (PKResultBean pkResultBean : retString) {
                        array.put(pkResultBean.toJSON());
                    }
                    answerObject.put("results", array);
                    return answerObject.toString().getBytes();
                } catch (NumberFormatException e) {
                    logger.error("PChExportSOE.handleRESTRequest.NumberFormatException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NumberFormatException->Double",
                            values).getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("queryHtmlPopup")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"layerId", "text", "geometry", "objectIds", "whereClause", "outFields", "returnGeometry"});
                try {
                    if (values[0] != null) htmlPopupQueryTask.setLayerId(Integer.parseInt(values[0]));
                    htmlPopupQueryTask.setText(values[1]);
                    if (values[2] != null) {
                        JSONObject geomElem = new JSONObject(new JSONTokener(values[2]));
                        htmlPopupQueryTask.setGeometry(AgsJsonEnvelope.convertJsonToAgsJsonGeom(geomElem).toArcObject());
                    }
                    htmlPopupQueryTask.setObjectIds(values[3]);
                    htmlPopupQueryTask.setWhereClause(values[4]);
                    htmlPopupQueryTask.setOutFields(values[5]);
                    //htmlPopupQueryTask.setReturnGeometry(values[6].equalsIgnoreCase("true"));
                    HtmlPopupQueryResult htmlPopupQueryResult = htmlPopupQueryTask.queryHtmlPopup();
                    return htmlPopupQueryResult.toJSON().toString().getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                } catch (Exception e) {
                    logger.error("PChExportSOE.handleRESTRequest.Exception: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" Exception",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getPointForPK")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"route", "pk"});
                logger.warning("Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                String route = values[0];
                double pk = Double.parseDouble(values[1]);
                Point point = calculatePkTask.getPointForPK(route, pk);
                try {
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("route", route);
                    answerObject.put("pkFrom", pk);
                    answerObject.put("point", AgsJsonGeometry.convertGeomToJson(point));
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getLineForPKs")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"route", "pkFrom", "pkTo"});
                logger.warning("Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                String route = values[0];
                double pkFrom = Double.parseDouble(values[1]);
                double pkTo = Double.parseDouble(values[2]);
                Polyline polyline = calculatePkTask.getLineForPK(route, pkFrom, pkTo);
                try {
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("route", route);
                    answerObject.put("pkFrom", pkFrom);
                    answerObject.put("pkTo", pkTo);
                    answerObject.put("line", AgsJsonGeometry.convertGeomToJson(polyline));
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getHeightForPoint")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"x", "y"});
                logger.warning("Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                try {
                    double xCoord = Double.parseDouble(values[0]);
                    double yCoord = Double.parseDouble(values[1]);
                    double height = getHeightForPoint(xCoord, yCoord);
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("valid", "ok");
                    answerObject.put("height", height);
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"no details", " specified"}).getBytes();
                }
            } else if (operationName.equalsIgnoreCase("getHeightForLine")) {
                String[] values = RESTUtils.getParameterValueFromOperationInput(operationInput, new String[]{"geometry"});
                logger.warning("Operation '" + operationName + "' with params: " + Arrays.toString(values) + " ...");
                try {
                    JSONObject jsonGeom = new JSONObject(values[0]);
                    AgsJsonPolyline jsonPolyline = new AgsJsonPolyline(jsonGeom);
                    IPolyline inputLine = jsonPolyline.toArcObject();
                    double length2D = inputLine.getLength();
                    IPolyline polyline = heightTask.getHeightForLine(inputLine);
                    JSONObject answerObject = new JSONObject();
                    answerObject.put("name", operationName);
                    answerObject.put("valid", "ok");
                    answerObject.put("length2D", length2D);
                    answerObject.put("length3D", polyline.getLength());
                    answerObject.put("polyline3D", AgsJsonGeometry.convertGeomToJson(polyline));
                    return answerObject.toString().getBytes();
                } catch (JSONException e) {
                    logger.error("PChExportSOE.handleRESTRequest.JSONException: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" JSONException",
                            new String[]{"JSONException", e.getMessage()}).getBytes();
                } catch (Exception e) {
                    logger.error("PChExportSOE.handleRESTRequest.Exception: " + e.getMessage());
                    return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" Exception",
                            new String[]{"Exception", e.getMessage()}).getBytes();
                }
            } else {
                logger.warning("Operation is NOT YET IMPLEMENTED: " + operationName + " !!!");
                return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NOT YET IMPLEMENTED on resource [" + resourceName + "].",
                        new String[]{"no details", " specified"}).getBytes();
            }
        } catch (AutomationException e) {
            logger.error("PChExportSOE.handleRESTRequest.AutomationException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("PChExportSOE.handleRESTRequest.IOException: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getMetadataByID(int layerId) throws IOException {
        return metadataTask.getMetadataByID(layerId);
    }

    @Override
    public PKResultBean[] getPKsByCoord(double coordX, double coordY, String wkid, double tolerance) throws IOException {
        return (PKResultBean[]) calculatePkTask.getPKsByCoordArray(coordX, coordY, wkid, tolerance).toArray();
    }

    @Override
    public double getHeightForPoint(double coordX, double coordY) throws IOException {
        return heightTask.getHeightForPoint(coordX, coordY);
    }

    @Override
    public HeightResultBean[] getHeightForLine(String lineGeom) throws IOException, JSONException {
        Polyline polyline = (Polyline) heightTask.getHeightForLine(new AgsJsonPolyline(new JSONObject(lineGeom)).toArcObject());
        if (polyline.isZAware()) {
            HeightResultBean[] heightResultBeans = new HeightResultBean[polyline.getPointCount()];
            for (int i = 0; i < heightResultBeans.length; i++) {
                IPoint pt = polyline.getPoint(i);
                heightResultBeans[i] = new HeightResultBean(pt);
            }
            return heightResultBeans;
        }
        return null;
    }

    @Override
    public String exportLayers() {
        return legendTask.exportLayers();
    }

    @Override
    public int getLayerCount() throws IOException {
        Map map;
        logger.debug("getLayerCount.start...");
        try {
            map = (Map) mapServer.getMap(mapServer.getDefaultMapName());
            logger.debug("getLayerCount.map= " + map);
            return map.getLayerCount();
        } catch (AutomationException aEx) {
            logger.error("getLayerCount.AutomationException for MAPcast: Is " + soHelper.getServerObject().getConfigurationName() + " a MSD mapService ??");
        }
        return 0;
    }
}
