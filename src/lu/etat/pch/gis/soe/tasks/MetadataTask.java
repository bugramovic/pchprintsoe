package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.FeatureLayer;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.geodatabase.IMetadata;
import com.esri.arcgis.geodatabase.IXmlPropertySet2;
import com.esri.arcgis.system.IName;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 12, 2010
 * Time: 8:03:15 AM
 */
public class MetadataTask extends AbstractTask{
    public MetadataTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }
    public String getMetadataByID(int layerId) throws IOException {
        logger.debug( "PCHExportSOE.getMetadataByID.layerId: " + layerId);
        ILayer layer = this.mapServer.getLayer("", layerId);
        logger.debug( "PCHExportSOE.getMetadataByID.layer: " + layer);
        if (layer instanceof FeatureLayer) {
            FeatureLayer fLayer = (FeatureLayer) layer;
            logger.debug( "PCHExportSOE.getMetadataByID.fLayer: " + fLayer);
            IName fLayerDSName = fLayer.getDataSourceName();
            logger.debug( "PCHExportSOE.getMetadataByID.fLayerDSName: " + fLayerDSName);
            if (fLayerDSName instanceof IMetadata) {
                IMetadata metatdaDataSource = (IMetadata) fLayerDSName;
                logger.debug( "PCHExportSOE.getMetadataByID.metatdaDataSource: " + metatdaDataSource);
                IXmlPropertySet2 pPropertySet = (IXmlPropertySet2) metatdaDataSource.getMetadata();
                logger.debug( "PCHExportSOE.getMetadataByID.pPropertySet: " + pPropertySet);
                if (pPropertySet != null)
                    return pPropertySet.getXml("");
            }
        }
        return "";
    }
    
}
