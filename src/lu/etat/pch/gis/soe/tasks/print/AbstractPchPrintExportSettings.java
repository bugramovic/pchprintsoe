package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.output.IExport;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/11/11
 * Time: 6:10 AM
 */
public abstract class AbstractPchPrintExportSettings {
    public abstract JSONObject toJSON();
    public abstract IExport toArcObject() throws IOException;
}
