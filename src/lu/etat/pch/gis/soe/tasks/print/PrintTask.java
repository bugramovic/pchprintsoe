package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.*;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeographicCoordinateSystem;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.geoprocessing.GeoProcessor;
import com.esri.arcgis.geoprocessing.IGeoProcessorResult;
import com.esri.arcgis.geoprocessing.tools.datamanagementtools.PackageMap;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.output.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.tagRECT;
import lu.etat.pch.gis.soe.tasks.AbstractTask;
import lu.etat.pch.gis.utils.LayerUtils;
import lu.etat.pch.gis.utils.MxdUtils;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonElement;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonEnvelope;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Apr 28, 2010
 * Time: 6:01:55 AM
 */
public class PrintTask extends AbstractTask {
    private static final String exportFilePrefix = "PrintTask_";
    private static final boolean GENERATE_ALWAYS_MXD = true;

    private Map map;
    private PageLayout pageLayout;

    public PrintTask(SOELogger logger, IMapServerDataAccess mapServerDataAccess, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        IMapServer3 ms = (IMapServer3) mapServerDataAccess;
        try {
            IMapServerInfo mapServerInfo = ms.getServerInfo(ms.getDefaultMapName());
            String mapName = ms.getDefaultMapName();
            logger.debug("mapName: " + mapName);
            IMapLayerInfos layerInfos = mapServerInfo.getMapLayerInfos();
            int layerCount = layerInfos.getCount();
            logger.debug("layerCount: " + layerCount);

            IMapServerInfo3 mapServerInfo3 = (IMapServerInfo3) mapServerInfo;
            ILayerDescriptions layerDescriptions = mapServerInfo3
                    .getDefaultMapDescription().getLayerDescriptions();
            for (int i = 0; i < mapServerInfo.getMapLayerInfos().getCount(); i++) {
                LayerDescription layerDescription = (LayerDescription) layerDescriptions
                        .getElement(i);
                logger.debug("layerDescription["+i+"]: "+layerDescription.getID()+" "+layerDescription.getSourceID());
            }
            String defaultMapName = mapServer.getDefaultMapName();
            logger.debug("defaultMapName: " + defaultMapName);
            map = (Map)mapServer.getMap(defaultMapName);
            pageLayout = (PageLayout) mapServer.getPageLayout();
            updateLegend(true, true);
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PrintTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            String defaultMapName = mapServer.getDefaultMapName();
            logger.debug("defaultMapName: " + defaultMapName);
            map = (Map)mapServer.getMap(defaultMapName);
            pageLayout = (PageLayout) mapServer.getPageLayout();
            updateLegend(true, true);
        } catch (IOException e) {
            logger.error("PrintTask().IOException: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public String printMap(JSONObject jsonObject) {
        try {
            JSONObject mapExtent = jsonObject.getJSONObject("mapExtent");
            JSONObject printOutput = jsonObject.getJSONObject("printOutput");
            JSONArray mapElements = jsonObject.optJSONArray("mapElements");
            if (mapElements == null) mapElements = jsonObject.optJSONArray("mapElement");
            if (mapElements == null) mapElements = new JSONArray();
            JSONArray mapServices = (!jsonObject.isNull("mapServices") ? jsonObject.getJSONArray("mapServices") : new JSONArray());
            return print(false, mapExtent, printOutput, mapElements, null, mapServices);
        } catch (JSONException jsonEx) {
            return RESTUtils.sendError(0, "PrintTask.printMap", new String[]{"JSONException", jsonEx.getMessage()});
        }
    }

    public String printLayout(JSONObject jsonObject) {
        try {
            JSONObject mapExtent = jsonObject.getJSONObject("mapExtent");
            JSONObject printOutput = jsonObject.getJSONObject("printOutput");
            JSONArray mapElements = jsonObject.optJSONArray("mapElements");
            if (mapElements == null) mapElements = jsonObject.optJSONArray("mapElement");
            if (mapElements == null) mapElements = new JSONArray();
            JSONArray layoutElements = jsonObject.optJSONArray("layoutElements");
            if (layoutElements == null) layoutElements = jsonObject.optJSONArray("layoutElement");
            if (layoutElements == null) layoutElements = new JSONArray();
            JSONArray mapServices = (!jsonObject.isNull("mapServices") ? jsonObject.getJSONArray("mapServices") : new JSONArray());
            return print(true, mapExtent, printOutput, mapElements, layoutElements, mapServices);
        } catch (JSONException jsonEx) {
            return RESTUtils.sendError(0, "PrintTask.printLayout", new String[]{"JSONException", jsonEx.getMessage()});
        }
    }

    private String print(boolean layout, JSONObject mapExtentJSON, JSONObject printOutputJSON, JSONArray mapElementsJSON, JSONArray layoutElementsArray, JSONArray mapServicesJSON) {
        try {
            IEnvelope mapExtentEnvelope = new AgsJsonEnvelope(mapExtentJSON).toArcObject();
            PchPrintOutput printOutput = new PchPrintOutput(printOutputJSON);

            test_debug_Legend("A");
            clear_Legend("A_clear");
            test_debug_Legend("A_CLEARED");
            List<PchPrintMapService> printMapServices = PchPrintMapService.parseJsonArray(mapServicesJSON);
            List<IElement> mapElementsFromJson = AgsJsonElement.getElementsCollectionFromJson(logger, null, mapElementsJSON, null, null);

            map.setExtent(mapExtentEnvelope);

            test_debug_Legend("B");
            List<ILayer> originalMapServiceLayerList = LayerUtils.getMapLayers(logger, map, true);

            test_debug_Legend("C");

            prepareMap(mapExtentEnvelope, printOutput, printMapServices, mapElementsFromJson);
            map.refresh();
            map.contentsChanged();
            test_debug_Legend("D");
            List<IElement> addedLayoutElementList = null;
            if (layout) {
                test_debug_Legend("00");
                //initLegend(pageLayout);
                test_debug_Legend("00b");
                prepareScreenDisplay(pageLayout.getScreenDisplay(), printOutput);
                preparePageLayout(printOutput);
                addedLayoutElementList = configurePageLayoutElements(mapExtentEnvelope, printOutput, layoutElementsArray);
                map.contentsChanged();
                map.activate(0);
                pageLayout.refreshCaches();
                pageLayout.refresh();
                pageLayout.activate(0);
            }
            String mapServerOutputFolder = getMapServerOutputFolder(mapServer);
            String exportFileName = mapServerOutputFolder + exportFilePrefix + mapServiceName.replaceAll("/", "_") + "_" + System.currentTimeMillis() + (layout ? "_layout" : "_map") + "." + printOutput.getFormat();
            String outputUrl;
            if (layout) {
                outputUrl = exportViewToFile(exportFileName, printOutput, pageLayout);
            } else {
                outputUrl = exportViewToFile(exportFileName, printOutput, map);
            }

            if (addedLayoutElementList != null) {
                for (IElement iElement : addedLayoutElementList) {
                    System.out.println("iElement = " + iElement);
                    if (iElement != null) {
                        try {
                            pageLayout.deleteElement(iElement);
                        } catch (AutomationException e) {
                            logger.error("pageLayout.deleteElement.AutomationException: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
                pageLayout.refresh();
            }


            //cleanup
            for (IElement mapElement : mapElementsFromJson)
                map.getGraphicsContainer().deleteElement(mapElement);
            test_debug_Legend("08");

            LayerUtils.restoreMapLayers(logger, map, originalMapServiceLayerList, pageLayout);
            //map.clearLayers();
            test_debug_Legend("09");
            map.refresh();

            JSONObject retObject = new JSONObject();
            retObject.put("outputFile", outputUrl);
            test_debug_Legend("09a");
            updateLegend(true, true);
            test_debug_Legend("09b");
            return retObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return RESTUtils.sendError(0, "PrintTask.print", new String[]{"JSONException", e.getMessage()});
        } catch (IOException e) {
            e.printStackTrace();
            return RESTUtils.sendError(0, "PrintTask.print", new String[]{"IOException", e.getMessage()});
        }
    }

    private void preparePageLayout(PchPrintOutput printOutput) {
        try {
            Page page = (Page) pageLayout.getPage();
            page.setUnits(printOutput.getPageUnits());
            page.putCustomSize(printOutput.getWidth(), printOutput.getHeight());
            page.setStretchGraphicsWithPage(false);
            pageLayout.reset();
            pageLayout.zoomToWhole();
        } catch (AutomationException e) {
            logger.error("preparePageLayout.AutomationException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("preparePageLayout.IOException: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void prepareMap(IEnvelope mapExtentEnvelope, PchPrintOutput printOutput, List<PchPrintMapService> mapServices, List<IElement> mapElements) {
        try {
            prepareScreenDisplay(map.getScreenDisplay(), printOutput);
            List<ILayer> toPrintLayerList = PchPrintMapService.createLayersFromPchPrintMapServices(logger, mapServices);
            logger.debug("prepareMap.toPrintLayerList.size: " + toPrintLayerList.size());
            for (ILayer layerToAdd : toPrintLayerList) {
                if (layerToAdd != null) {
                    logger.debug("prepareMap.layerToAdd: " + layerToAdd.getName());
                    map.addLayer(layerToAdd);
                } else
                    logger.error("\n\nPrintTask.prepareMap.layerToAdd = null");
            }

            ISpatialReference spatialReference = mapExtentEnvelope.getSpatialReference();
            if (spatialReference != null) {
                map.setSpatialReferenceByRef(spatialReference);
            }
            map.setExtent(mapExtentEnvelope);

            for (IElement mapElement : mapElements)
                map.getGraphicsContainer().addElement(mapElement, 0);
            map.refresh();
            map.partialRefresh(esriViewDrawPhase.esriViewGraphics, null, null);
        } catch (AutomationException e) {
            logger.error("prepareMap.AutomationException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("prepareMap.IOException: " + e.getMessage());
            e.printStackTrace();
        }

    }

    private void prepareScreenDisplay(IScreenDisplay screenDisplay, PchPrintOutput printOutput) throws IOException {
        IDisplayTransformation displayTransformation = screenDisplay.getDisplayTransformation();

        double widthInPixel = printOutput.getWidthInPixel();
        double heightInPixel = printOutput.getHeightInPixel();
        IEnvelope pBoundsEnvelope = new Envelope();
        pBoundsEnvelope.putCoords(0.0, 0.0, widthInPixel, heightInPixel);

        tagRECT deviceRect = new tagRECT();
        deviceRect.left = 0;
        deviceRect.top = 0;
        deviceRect.right = (int) widthInPixel;
        deviceRect.bottom = (int) heightInPixel;

        displayTransformation.setVisibleBounds(pBoundsEnvelope);
        displayTransformation.setBounds(pBoundsEnvelope);
        displayTransformation.setDeviceFrame(deviceRect);
        displayTransformation.setResolution(printOutput.getResolution());
        displayTransformation.setRotation(printOutput.getMapRotation());
        screenDisplay.setDisplayTransformation(displayTransformation);
    }

    private List<IElement> configurePageLayoutElements(IEnvelope mapExtentEnvelope, PchPrintOutput printOutput, JSONArray toAddJsonLayoutElements) {
        try {
            test_debug_Legend("10");
            List<String> toRemoveLayoutElements = printOutput.getToRemoveLayoutElements();
            if (toRemoveLayoutElements.size() > 0) {
                List<IElement> removedElements = new ArrayList<IElement>();
                pageLayout.reset();
                IElement pageElement = pageLayout.next();
                while (pageElement != null) {
                    if (toRemoveLayoutElements.contains("*")) {
                        if (!(pageElement instanceof MapFrame)) {
                            removedElements.add(pageElement);
                        }
                    } else {
                        if (pageElement instanceof MapFrame) {
                            MapFrame mapFrame = (MapFrame) pageElement;
                            if (toRemoveLayoutElements.contains("MapFrame.*") || toRemoveLayoutElements.contains("MapFrame." + mapFrame.getName())) {
                                removedElements.add(pageElement);
                            }
                        } else if (pageElement instanceof MapSurroundFrame) {
                            MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) pageElement;
                            Object mapSurroundFrameObject = mapSurroundFrame.getObject();
                            if (mapSurroundFrameObject instanceof MarkerNorthArrow) {
                                MarkerNorthArrow markerNorthArrow = (MarkerNorthArrow) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("NorthArrow.*") || toRemoveLayoutElements.contains("NorthArrow." + markerNorthArrow.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof IScaleBar) {
                                IScaleBar scaleBar = (IScaleBar) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("ScaleBar.*") || toRemoveLayoutElements.contains("ScaleBar." + scaleBar.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof ScaleText) {
                                ScaleText scaleText = (ScaleText) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("ScaleText.*") || toRemoveLayoutElements.contains("ScaleText." + scaleText.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else if (mapSurroundFrameObject instanceof Legend) {
                                Legend legend = (Legend) mapSurroundFrameObject;
                                if (toRemoveLayoutElements.contains("Legend.*") || toRemoveLayoutElements.contains("Legend." + legend.getName())) {
                                    removedElements.add(pageElement);
                                }
                            } else {
                                logger.error("\n\n\n\nUnHandled MapSurroundFrameObject in PrintTask.mapSurroundFrameObject=" + mapSurroundFrameObject.getClass() + "  > " + mapSurroundFrameObject);
                            }
                        } else if (pageElement instanceof TextElement) {
                            TextElement textElement = (TextElement) pageElement;
                            if (toRemoveLayoutElements.contains("TextElement.*") || toRemoveLayoutElements.contains("TextElement." + textElement.getName())) {
                                removedElements.add(pageElement);
                            }
                        } else {
                            logger.error("\nPrintTask.nonMapFrameElement found: " + pageElement.getClass() + " >> " + pageElement);
                        }
                    }
                    pageElement = pageLayout.next();
                }
                for (IElement toRemoveElement : removedElements)
                    pageLayout.deleteElement(toRemoveElement);
                pageLayout.refresh();
            }
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            MapFrame mapFrame = (MapFrame) pageLayout.findFrame(map);
            logger.debug("configurePageLayoutElements.mapFrame: " + mapFrame);
            IEnvelope mapFrameEnv = printOutput.calculateMapFrameEnvelope();
            if (mapFrame == null) {
                mapFrame = new MapFrame();
                logger.debug("configurePageLayoutElements.mapFrame1: " + mapFrame);
                mapFrame.setMapByRef(map);
                logger.debug("configurePageLayoutElements.mapFrame2: " + mapFrame);
                mapFrame.setGeometry(mapFrameEnv);
                pageLayout.addElement(mapFrame, 0);
                logger.debug("configurePageLayoutElements.mapFrame3: " + mapFrame);
            } else {
                mapFrame.setGeometry(mapFrameEnv);
            }
            map.setExtent(mapExtentEnvelope);
            mapFrame.setMapByRef(map);

            pageLayout.selectElement(mapFrame);
            IEnumElement elemSel = pageLayout.getSelectedElements();
            pageLayout.sendToBack(elemSel);

            mapFrame.setExtentType(esriExtentTypeEnum.esriExtentScale);
            if (!(mapExtentEnvelope.getSpatialReference() instanceof IGeographicCoordinateSystem)) {
                mapFrame.setMapScale(printOutput.calculateScale(mapExtentEnvelope));
            }
            IEnvelope layoutEnvelope = pageLayout.getPage().getPrintableBounds();
            List<IElement> toAddLayoutElements = AgsJsonElement.getElementsCollectionFromJson(logger, mapFrame, toAddJsonLayoutElements, layoutEnvelope, pageLayout);

            for (IElement toAddElement : toAddLayoutElements) {
                if (toAddElement == null) {
                    logger.error("configurePageLayoutElements.toAddElement = null !!");
                } else {
                    if (toAddElement instanceof IMapSurround)
                        ((IMapSurround) toAddElement).setMapByRef(map);
                    try {
                        System.out.println("toAddElement = " + toAddElement);
                        pageLayout.addElement(toAddElement, 0);
                    } catch (AutomationException e) {
                        logger.error("configurePageLayoutElements.AutomationException: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
            pageLayout.refreshCaches();
            return toAddLayoutElements;
        } catch (AutomationException e) {
            logger.error("configurePageLayoutElements.AutomationException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("configurePageLayoutElements.IOException: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public void updateLegend(boolean removeAllItems, boolean queryBounds) {
        try {
            IElement elem;
            pageLayout.reset();
            while ((elem = pageLayout.next()) != null) {
                if (elem instanceof MapSurroundFrame) {
                    MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) elem;
                    Object mapSurroundObject = mapSurroundFrame.getObject();
                    if (mapSurroundObject instanceof Legend) {
                        map.contentsChanged();
                        Legend legend = (Legend) mapSurroundObject;
                        if (removeAllItems)
                            legend.clearItems();
                        if (queryBounds) {
                            Envelope newEnv = new Envelope();
                            legend.queryBounds(pageLayout.getScreenDisplay(), mapSurroundFrame.getGeometry().getEnvelope(), newEnv);
                            mapSurroundFrame.setGeometry(newEnv);
                            pageLayout.refresh();
                        }
                    }
                }
            }
        } catch (AutomationException e) {
            logger.error("updateLegend.AutomationException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("updateLegend.IOException: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void test_debug_Legend(String prefix) {
        if (1 == 1) return;
        if (map == null) return;
        try {
            map.contentsChanged();
            map.refresh();
            IElement elem = null;
            pageLayout.reset();
            while ((elem = pageLayout.next()) != null) {
                if (elem instanceof MapSurroundFrame) {
                    MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) elem;
                    Object mapSurroundObject = mapSurroundFrame.getObject();
                    if (mapSurroundObject instanceof Legend) {
                        map.contentsChanged();
                        Legend legend = (Legend) mapSurroundObject;
                        legend.refresh();
                        mapSurroundFrame.activate(map.getScreenDisplay());
                        AgsJsonEnvelope env = (AgsJsonEnvelope) AgsJsonGeometry.convertJsonToAgsJsonGeom(AgsJsonGeometry.convertGeomToJson(mapSurroundFrame.getGeometry().getEnvelope()));
                        System.err.println(prefix + "-legend[" + legend.getName() + "].env = " + env.toJSON().toString());

                        Envelope newEnv = new Envelope();
                        legend.queryBounds(pageLayout.getScreenDisplay(), mapSurroundFrame.getGeometry().getEnvelope(), newEnv);
                        //mapSurroundFrame.setGeometry(newEnv);
                        /*
                        AgsJsonEnvelope newJsonEnv = (AgsJsonEnvelope) AgsJsonGeometry.convertJsonToAgsJsonGeom(AgsJsonGeometry.convertGeomToJson(newEnv));
                        System.err.println(prefix + "-legend.new = " + newJsonEnv.toJSON().toString());
                        */
                        pageLayout.refresh();
                    }
                }
            }
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear_Legend(String prefix) {
        if (1 == 1) return;
        ;
        if (map == null) return;
        try {
            map.contentsChanged();
            map.refresh();
            IElement elem = null;
            pageLayout.reset();
            while ((elem = pageLayout.next()) != null) {
                if (elem instanceof MapSurroundFrame) {
                    MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) elem;
                    Object mapSurroundObject = mapSurroundFrame.getObject();
                    if (mapSurroundObject instanceof Legend) {
                        map.contentsChanged();
                        Legend legend = (Legend) mapSurroundObject;
                        legend.clearItems();
                        legend.refresh();
                        mapSurroundFrame.activate(map.getScreenDisplay());
                        AgsJsonEnvelope env = (AgsJsonEnvelope) AgsJsonGeometry.convertJsonToAgsJsonGeom(AgsJsonGeometry.convertGeomToJson(mapSurroundFrame.getGeometry().getEnvelope()));
                        System.err.println(prefix + "-legend.env = " + env.toJSON().toString());

                        Envelope newEnv = new Envelope();
                        legend.queryBounds(pageLayout.getScreenDisplay(), mapSurroundFrame.getGeometry().getEnvelope(), newEnv);
                        //mapSurroundFrame.setGeometry(newEnv);
                        /*
                        AgsJsonEnvelope newJsonEnv = (AgsJsonEnvelope) AgsJsonGeometry.convertJsonToAgsJsonGeom(AgsJsonGeometry.convertGeomToJson(newEnv));
                        System.err.println(prefix + "-legend.new = " + newJsonEnv.toJSON().toString());
                        */
                        pageLayout.refresh();
                    }
                }
            }
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String exportViewToFile(String exportFileName, PchPrintOutput printOutput, IActiveView activeView) throws IOException {
        String mxdFileName = "?";
        if (GENERATE_ALWAYS_MXD || printOutput.getFormat().equalsIgnoreCase("mxd")) {
            if (activeView instanceof Map) {
                mxdFileName = new MxdUtils().exportMapToMxdFile(logger,exportFileName, (Map) activeView);
            } else if (activeView instanceof PageLayout) {
                mxdFileName = new MxdUtils().exportLayoutToMxdFile(logger,exportFileName, (PageLayout) activeView);
            } else {
                logger.error("\n\n\n\nUnHandled exportViewToFile on IActiveView: " + activeView);
                return mxdFileName;
            }
        }
        logger.debug("exportViewToFile.mxdFileName: "+mxdFileName);
        if (printOutput.getFormat().equalsIgnoreCase("mxd")) {
            exportFileName = mxdFileName;
        } else if (printOutput.getFormat().equalsIgnoreCase("mpk")) {
            logger.debug("exportViewToFile.mkp....: "+exportFileName);
            PackageMap packageMapTool = new PackageMap(mxdFileName, exportFileName);
            packageMapTool.setConvertData("CONVERT");
            packageMapTool.setConvertArcsdeData("CONVERT_ARCSDE");
            if (activeView instanceof PageLayout) {
                PageLayout pageLayout = (PageLayout) activeView;
                IEnvelope env = ((Map) pageLayout.getFocusMap()).getExtent();
                String extentStr = env.getXMin() + " " + env.getYMin() + " " + env.getXMax() + " " + env.getYMax();
                packageMapTool.setExtent(extentStr);
            } else if (activeView instanceof Map) {
                IEnvelope env = activeView.getExtent();
                String extentStr = env.getXMin() + " " + env.getYMin() + " " + env.getXMax() + " " + env.getYMax();
                packageMapTool.setExtent(extentStr);
            }
            packageMapTool.setApplyExtentToArcsde("ALL");
            GeoProcessor geoProcessor = new GeoProcessor();
            IGeoProcessorResult result = geoProcessor.execute(packageMapTool, null);
            logger.debug("exportViewToFile.mpk: " + result.getReturnValue());
        } else if (!printOutput.getFormat().equalsIgnoreCase("mxd")) {
            IExport export = null;
            AbstractPchPrintExportSettings exportSettings = printOutput.getExportSettings();
            if (exportSettings != null) {
                export = exportSettings.toArcObject();
            } else {
                if (printOutput.getFormat().equalsIgnoreCase("pdf")) {
                    export = new PchPrintExportPDF().toArcObject();
                    ((ExportPDF) export).setImageCompression(esriExportImageCompression.esriExportImageCompressionLZW);
                } else if (printOutput.getFormat().equalsIgnoreCase("jpeg") || printOutput.getFormat().equalsIgnoreCase("jpg")) {
                    ExportJPEG exportJPEG = new ExportJPEG();
                    exportJPEG.setProgressiveMode(true);
                    exportJPEG.setQuality((short) 100);
                    exportJPEG.setImageType(esriExportImageType.esriExportImageTypeTrueColor);
                    export = exportJPEG;
                } else if (printOutput.getFormat().equalsIgnoreCase("png")) {
                    ExportPNG exportPNG = new ExportPNG();
                    export = exportPNG;
                } else if (printOutput.getFormat().equalsIgnoreCase("emf")) {
                    ExportEMF exportEMF = new ExportEMF();
                    export = exportEMF;
                } else if (printOutput.getFormat().equalsIgnoreCase("tif") || printOutput.getFormat().equalsIgnoreCase("tifw")) {
                    ExportTIFF exportTIFF = new ExportTIFF();
                    if (printOutput.getFormat().equalsIgnoreCase("tifw")) {
                        exportTIFF.setGeoTiff(true);
                        exportTIFF.setMapExtent(activeView.getExtent());
                        exportFileName = exportFileName.substring(0, exportFileName.length() - 1);
                    }
                    export = exportTIFF;
                }
            }
            if (export != null) {
                logger.debug("exportViewToFile.exporting....: "+exportFileName);
                export.setExportFileName(exportFileName);

                double outputResolution = printOutput.getResolution();
                export.setResolution(outputResolution);
                configureRasterOutputResampleRation(activeView, esriResampleRatio.esriRasterOutputBest);

                if (export instanceof IOutputRasterSettings)
                    ((IOutputRasterSettings) export).setResampleRatio(esriResampleRatio.esriRasterOutputBest);

                tagRECT exportRect = new tagRECT();
                exportRect.left = 0;
                exportRect.top = 0;
                exportRect.right = (int) (printOutput.getWidthInPixel());
                exportRect.bottom = (int) (printOutput.getHeightInPixel());

                Envelope pPixelBoundsEnv = new Envelope();
                pPixelBoundsEnv.putCoords(exportRect.left, exportRect.top, exportRect.right, exportRect.bottom);
                export.setPixelBounds(pPixelBoundsEnv);

                int hDC = export.startExporting();
                if (activeView instanceof PageLayout) {
                    logger.debug("ExportPdf.output.pageLayout...");
                    test_debug_Legend("aa1");
                    activeView.output(hDC, (int) outputResolution, exportRect, null, null);
                    test_debug_Legend("aa2");
                } else {
                    logger.debug("ExportPdf.output.map... ");
                    activeView.output(hDC, (int) outputResolution, exportRect, activeView.getExtent(), null);
                }
                export.finishExporting();
                logger.debug("ExportPdf.output.FINISHED");
                export.cleanup();
            }
        }
        String pOutput = mapServer.getPhysicalOutputDirectory();
        String vOutput = mapServer.getVirtualOutputDirectory();
        return exportFileName.replace(pOutput, vOutput);
    }


    private void configureRasterOutputResampleRation(IActiveView activeView, int resampleRation) throws IOException {
        IScreenDisplay screenDisplay = activeView.getScreenDisplay();
        DisplayTransformation displayTransformation = (DisplayTransformation) screenDisplay.getDisplayTransformation();
        displayTransformation.setResampleRatio(resampleRation);
        screenDisplay.setDisplayTransformation(displayTransformation);
        if (activeView instanceof PageLayout) {
            PageLayout pageLayout = (PageLayout) activeView;
            pageLayout.reset();
            IElement layoutElem;
            while ((layoutElem = pageLayout.next()) != null) {
                if (layoutElem instanceof IMapFrame) {
                    IMapFrame mapFrame = (IMapFrame) layoutElem;
                    Map layoutMap = (Map) mapFrame.getMap();
                    configureRasterOutputResampleRation(layoutMap, resampleRation);
                }
            }
        }
    }

    public void cleanupOldExports() throws IOException {
        File exportFileDirectory = new File(mapServer.getPhysicalOutputDirectory());
        File[] doDeleteFiles = exportFileDirectory.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                if (name.startsWith(exportFilePrefix + "engineTest_"))
                    return true;
                return name.startsWith(exportFilePrefix + mapServiceName.replaceAll("/", "_") + "_");
            }
        });
        for (File doDeleteFile : doDeleteFiles) {
            if (!doDeleteFile.delete()) {
                logger.error("unable to delete old exportFile: " + doDeleteFile.getName());
            }
        }
    }
}
