package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.display.esriPictureSymbolOptions;
import com.esri.arcgis.output.ExportPDF;
import com.esri.arcgis.output.esriExportColorspace;
import com.esri.arcgis.output.esriExportImageCompression;
import com.esri.arcgis.output.esriExportPDFLayerOptions;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/11/11
 * Time: 5:54 AM
 */
public class PchPrintExportPDF extends AbstractPchPrintExportSettings {
    private int colorspace = esriExportColorspace.esriExportColorspaceRGB; // esriExportColorspace
    private boolean compressed = true;
    private int imageCompression = esriExportImageCompression.esriExportImageCompressionNone; //esriExportImageCompression
    private int exportPictureSymbolOptions = esriPictureSymbolOptions.esriPSOVectorize; //esriPictureSymbolOptions
    private boolean polyginizeMarkers = false;
    private boolean embedFonts = true;
    private int exportPDFLayersAndFeatureAttributes = esriExportPDFLayerOptions.esriExportPDFLayerOptionsNone; //esriExportPDFLayerOptions
    private boolean exportMeasureInfo = false;

    public PchPrintExportPDF() {
    }

    public PchPrintExportPDF(JSONObject jsonObj) {
        if (!jsonObj.isNull("colorspace")) colorspace = jsonObj.getInt("colorspace");
        if (!jsonObj.isNull("compressed")) compressed = jsonObj.getBoolean("compressed");
        if (!jsonObj.isNull("imageCompression")) imageCompression = jsonObj.getInt("imageCompression");
        if (!jsonObj.isNull("exportPictureSymbolOptions"))
            exportPictureSymbolOptions = jsonObj.getInt("exportPictureSymbolOptions");
        if (!jsonObj.isNull("polyginizeMarkers")) polyginizeMarkers = jsonObj.getBoolean("polyginizeMarkers");
        if (!jsonObj.isNull("embedFonts")) embedFonts = jsonObj.getBoolean("embedFonts");
        if (!jsonObj.isNull("exportPDFLayersAndFeatureAttributes"))
            exportPDFLayersAndFeatureAttributes = jsonObj.getInt("exportPDFLayersAndFeatureAttributes");
        if (!jsonObj.isNull("exportMeasureInfo")) exportMeasureInfo = jsonObj.getBoolean("exportMeasureInfo");

    }

    public JSONObject toJSON() {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("colorspace", colorspace);
        jsonObj.put("compressed", compressed);
        jsonObj.put("imageCompression", imageCompression);
        jsonObj.put("exportPictureSymbolOptions", exportPictureSymbolOptions);
        jsonObj.put("polyginizeMarkers", polyginizeMarkers);
        jsonObj.put("embedFonts", embedFonts);
        jsonObj.put("exportPDFLayersAndFeatureAttributes", exportPDFLayersAndFeatureAttributes);
        jsonObj.put("exportMeasureInfo", exportMeasureInfo);
        return jsonObj;
    }

    public ExportPDF toArcObject() throws IOException {
        ExportPDF exportPDF = new ExportPDF();
        exportPDF.setColorspace(colorspace);
        exportPDF.setCompressed(compressed);
        exportPDF.setImageCompression(imageCompression);
        exportPDF.setExportPictureSymbolOptions(exportPictureSymbolOptions);
        exportPDF.setPolygonizeMarkers(polyginizeMarkers);
        exportPDF.setEmbedFonts(embedFonts);
        exportPDF.setExportPDFLayersAndFeatureAttributes(exportPDFLayersAndFeatureAttributes);
        exportPDF.setExportMeasureInfo(exportMeasureInfo);
        return exportPDF;
    }
}
