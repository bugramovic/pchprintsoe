package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.gisclient.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;
import com.esri.arcgis.system.IName;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;
import lu.etat.pch.gis.utils.LayerUtils;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lu.etat.pch.gis.utils.IOUtils;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 9, 2010
 * Time: 5:21:42 PM
 */
public class PchPrintMapService {

    

    
    private String url;
    private String type; //AGS, WMS, ...
    private String name;
    private String visibleIds;  // "*" or "0,1,5,7"
    private Double alpha = 1d;
    private String definitionExpression;

    public PchPrintMapService(String url, String type, String name, String visibleIds) {
        this.url = url;
        this.type = type;
        this.name = name;
        this.visibleIds = visibleIds;
        this.alpha = 1d;
    }

    public PchPrintMapService(String url, String type, String name, String visibleIds, Double alpha) {
        this.url = url;
        this.type = type;
        this.name = name;
        this.visibleIds = visibleIds;
        this.alpha = alpha;
    }

    public PchPrintMapService(JSONObject msObject) throws JSONException {
        if (!msObject.isNull("url")) url = msObject.getString("url");
        if (!msObject.isNull("name")) name = msObject.getString("name");
        if (!msObject.isNull("type")) type = msObject.getString("type");
        if (!msObject.isNull("visibleIds")) visibleIds = msObject.getString("visibleIds");
        if (!msObject.isNull("alpha")) alpha = msObject.getDouble("alpha");
        if (!msObject.isNull("definitionExpression")) definitionExpression = msObject.getString("definitionExpression");
    }

    public String getUrl() {
        if ("BING".equalsIgnoreCase( type)) {
            if (getVisibleIds().equalsIgnoreCase("aerial")) {
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Aerial.lyr";
                return IOUtils.CLASSPATH_PREFIX + "/layerfiles/Bing_Maps_Aerial.lyr";
            } else if (getVisibleIds().equalsIgnoreCase("road")) {
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Road.lyr";
                return IOUtils.CLASSPATH_PREFIX + "/layerfiles/Bing_Maps_Road.lyr";
            } else if (getVisibleIds().equalsIgnoreCase("hybrid")) {
                //from "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Hybrid.lyr";
                return IOUtils.CLASSPATH_PREFIX + "/layerfiles/Bing_Maps_Hybrid.lyr";
            } else {
                return null;
            }
        } else if ("OSM".equalsIgnoreCase( type) ) {
            //from "https://ago-item-storage.s3.amazonaws.com/3daa73d933b7415997c37145a6094fe0/OpenStreetMap.lyr?AWSAccessKeyId=AKIAJS2Y2E72HYCOE7BA&Expires=1322158388&Signature=R8ywdRWSP76nkP%2FRmuJRQzMXlPI%3D";
            return IOUtils.CLASSPATH_PREFIX + "/layerfiles/OpenStreetMap.lyr";
        } else {
            return url;
        }
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getVisibleIds() {
        return visibleIds;
    }

    public Double getAlpha() {
        return alpha;
    }

    public String getDefinitionExpression() {
        return definitionExpression;
    }

    public void setDefinitionExpression(String definitionExpression) {
        this.definitionExpression = definitionExpression;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("url", url);
        jsonObj.put("type", type);
        jsonObj.put("name", name);
        jsonObj.put("visibleIds", visibleIds);
        jsonObj.put("alpha", alpha);
        if (definitionExpression != null) jsonObj.put("definitionExpression", definitionExpression);
        return jsonObj;
    }

    public static List<PchPrintMapService> parseJsonArray(JSONArray mapServiceArray) {
        List<PchPrintMapService> printMapServices = new ArrayList<PchPrintMapService>();
        try {
            for (int i = 0; i < mapServiceArray.length(); i++)
                printMapServices.add(new PchPrintMapService(mapServiceArray.getJSONObject(i)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return printMapServices;
    }

    public static List<ILayer> createLayersFromPchPrintMapServices(SOELogger logger, List<PchPrintMapService> mapServices) {
        List<ILayer> layerList = new ArrayList<ILayer>();
        for (PchPrintMapService mapService : mapServices) {
            try {
                logger.debug("mapService: " + mapService.getUrl());
                double restAlpha = mapService.getAlpha();
                short agsAlpha = (short) ((1 - restAlpha) * 100);
                logger.debug("agsAlpha: " + agsAlpha);
                logger.debug("mapService.type: " + mapService.getType());
                ILayer layer = getLayerForMapService(logger, mapService, agsAlpha);
                if (layer != null) {
                    layerList.add(layer);
                    logger.debug("layerAdded: " + layer);
                } else logger.error("unable to add mapservice layers for :" + mapService.getUrl());
            } catch (Exception e) {
                logger.error("createLayersFromPchPrintMapServices.Exception: " + e.getMessage());
                e.printStackTrace();
            }
        }
        return layerList;
    }

    private static ILayer getLayerForMapService(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        ILayer layer;
        if        ( "WMS".equalsIgnoreCase( mapService.getType()) ) {
            layer = addWMSLayer(logger, mapService, agsAlpha);
        } else if ( "LYR".equalsIgnoreCase( mapService.getType()) ) {
            layer = addLyrLayer(logger, mapService, agsAlpha);
        //} else if ( "BING".equalsIgnoreCase( mapService.getType()) )  {
        //    layer = addBingLayer(logger, mapService, agsAlpha);
        //} else if ( "OSM".equalsIgnoreCase( mapService.getType()) ) {
        //    layer = addOsmLayer(logger, mapService, agsAlpha);
        } else {
            ILayer toAddLayer = addPChPrintSOEMapServiceLayer(logger, mapService, agsAlpha);
            logger.debug("toAddLayer.addPChPrintSOEMapServiceLayer: " + toAddLayer);

            if (toAddLayer == null) {
                toAddLayer = addAGSMapServiceByLayerFile(logger, agsAlpha, mapService);
                logger.debug("toAddLayer.addAGSMapServiceByLayerFile: " + toAddLayer);
            }

            if (toAddLayer == null) {
                toAddLayer = addAGSInternetLayer(logger, agsAlpha, mapService);
                logger.debug("toAddLayer.addAGSInternetLayer: " + toAddLayer);
            }
            layer = toAddLayer;
        }
        return layer;
    }

    private static ILayer addLyrLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        try {
            String lyrUrl = mapService.getUrl();
            logger.debug("addLyrLayer.lyrUrl = " + lyrUrl);
            if (!RESTUtils.urlExists(logger, lyrUrl)) {
                logger.debug("addLyrLayer.urlNotFound: " + lyrUrl);
                return null;
            }
            File agsTmpFile = File.createTempFile("LyrLayer", ".lyr");
            logger.debug("addLyrLayer.agsTmpFile = " + agsTmpFile);
            RESTUtils.downloadFile(logger, lyrUrl, agsTmpFile);
            String agsTmpFileName = agsTmpFile.getAbsolutePath();
            logger.debug("addLyrLayer.agsTmpFileName = " + agsTmpFileName);
            ILayer layer = null;
            LayerFile layerFile = new LayerFile();
            if (layerFile.isLayerFile(agsTmpFileName)) {
                logger.debug("addLyrLayer.isLayerFile");
                layerFile.open(agsTmpFileName);
                layer = layerFile.getLayer();
                layerFile.close();
                layer.setVisible(true);
                logger.debug("addLyrLayer.layer: " + layer);
                //System.out.println("layer = " + layer);
                if (layer instanceof ILayerEffects) {
                    ((ILayerEffects) layer).setTransparency(agsAlpha);
                }
                String mapServiceVisibleIds = mapService.visibleIds;
                logger.debug("addLyrLayer.mapServiceVisibleIds: " + mapServiceVisibleIds);
                if (mapServiceVisibleIds != null && layer instanceof MapServerLayer) {
                    configureVisibilityForMapService((MapServerLayer) layer, mapServiceVisibleIds);
                    logger.debug("addLyrLayer.visibilityConfigured");
                } else {
                    logger.debug("addLyrLayer.layer.class: " + layer.getClass());
                }
            } else {
                logger.error("Layer not found !!!! : " + lyrUrl);
            }
            boolean deleted = agsTmpFile.delete();
            logger.debug("addLyrLayer.agsTmpFile.deleted: " + deleted);
            return layer;
        } catch (IOException ioEx) {
            logger.error("addLyrLayer.IOException: " + ioEx.getMessage());
        }
        return null;
    }

       private static ILayer addBingLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        File tmpLyr = null;
        try {
            LayerFile layerFile = new LayerFile();
            String url = null;
            if (mapService.getVisibleIds().equalsIgnoreCase("aerial")) {
                url = "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Aerial.lyr";
            } else if (mapService.getVisibleIds().equalsIgnoreCase("road")) {
                url = "http://www.arcgis.com/sharing/content/items/b6969de2b84d441692f5bb8792e65d1f/item.pkinfo";
            } else if (mapService.getVisibleIds().equalsIgnoreCase("hybrid")) {
                url = "http://downloads2.esri.com/resources/arcgisdesktop/layers/Bing_Maps_Road.lyr";
            } else {
                logger.error("NO BingMapPackages defined for: " + mapService.getVisibleIds());
                return null;
            }
            if (url != null) {
                tmpLyr =  File.createTempFile( "bing_", ".lyr" );
                RESTUtils.downloadFile( logger, url, tmpLyr);    
                layerFile.open( tmpLyr.getAbsolutePath() );
            }
            ILayer layer = layerFile.getLayer();
            if (!layer.isValid()) {
                logger.error("addBingLayer.layerIsNotValid!!");
            }
            return layer;
        } catch (IOException ioEx) {
            logger.error("addBingLayer.IOException: " + ioEx.getMessage());
        } finally {
            if (tmpLyr != null) {
                tmpLyr.delete();
            }
        }
        return null;
    }
       
    private static ILayer addOsmLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        throw new UnsupportedOperationException( "Not yet implemented" );
    }

    public static boolean layerIsValid(SOELogger logger, ILayer layer) throws IOException {
        if (!layer.isValid()) {
            logger.debug("layerIsInvalid:" + layer.getName());
            return false;
        }

        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                if (!layerIsValid(logger, compositeLayer.getLayer(i))) return false;
            }
        }
        return true;
    }

    private static ILayer addAGSMapServiceByLayerFile(SOELogger logger, short agsAlpha, PchPrintMapService mapService) {
        try {
            String mapServiceUrl = mapService.getUrl();
            String agsMapServiceLayerUrl = mapServiceUrl.contains( ".lyr") ? mapServiceUrl :
                                             mapServiceUrl + "?f=lyr";
            //                               mapServiceUrl + "?f=lyr&v=9.3";
            logger.debug("addAGSMapServiceByLayerFile.agsMapServiceLayerUrl = " + agsMapServiceLayerUrl);
            //if (!RESTUtils.urlExists(logger, agsMapServiceLayerUrl)) {
            if (!IOUtils.resourceExists( logger, agsMapServiceLayerUrl)) {
                logger.warning( "addAGSMapServiceByLayerFile.urlNotFound: " + agsMapServiceLayerUrl);
                return null;
            }
            File agsTmpFile = File.createTempFile("AGSLayer", ".lyr");
            logger.debug("addAGSMapServiceByLayerFile.agsTmpFile = " + agsTmpFile);
            //RESTUtils.downloadFile(logger, agsMapServiceLayerUrl, agsTmpFile);
            IOUtils.getResourceAsFile( logger, agsMapServiceLayerUrl, agsTmpFile);
            String agsTmpFileName = agsTmpFile.getAbsolutePath();
            logger.debug("addAGSMapServiceByLayerFile.agsTmpFileName = " + agsTmpFileName);
            ILayer layer = null;
            LayerFile layerFile = new LayerFile();
            if (layerFile.isLayerFile(agsTmpFileName)) {
                logger.debug("addAGSMapServiceByLayerFile.isLayerFile");
                layerFile.open(agsTmpFileName);
                layer = layerFile.getLayer();
                layerFile.close();

                layer.setVisible(true);
                logger.debug("addAGSMapServiceByLayerFile.layer: " + layer);
                //System.out.println("layer = " + layer);
                if (layer instanceof ILayerEffects) {
                    ((ILayerEffects) layer).setTransparency(agsAlpha);
                }
                String mapServiceVisibleIds = mapService.visibleIds;
                logger.debug("addAGSMapServiceByLayerFile.mapServiceVisibleIds: " + mapServiceVisibleIds);
                if (mapServiceVisibleIds != null && layer instanceof MapServerLayer) {
                    configureVisibilityForMapService((MapServerLayer) layer, mapServiceVisibleIds);
                    logger.debug("addAGSMapServiceByLayerFile.visibilityConfigured");
                } else {
                    logger.debug("addAGSMapServiceByLayerFile.layer.class: " + layer.getClass());
                }
            } else {
                logger.error("Layer not found !!!! : " + agsMapServiceLayerUrl);
            }
            boolean deleted = agsTmpFile.delete();
            logger.debug("addAGSMapServiceByLayerFile.agsTmpFile.deleted: " + deleted);
            return layer;
        } catch (Exception ioEx) {
            logger.error("addAGSMapServiceByLayerFile.Exception: " + ioEx.getMessage());
        }
        return null;
    }

    private static GroupLayer addPChPrintSOEMapServiceLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        try {
            String mapServiceUrl = mapService.getUrl();
            String pchPrintSOELayerUrl = mapServiceUrl + "/exts/PCHPrintSOE?f=json";
            if (!RESTUtils.urlExists(logger, pchPrintSOELayerUrl)) {
                logger.warning("addPChPrintSOEMapServiceLayer.urlNotFound: " + pchPrintSOELayerUrl);
                return null;
            }
            logger.debug("mapServiceUrl.exists = " + mapServiceUrl);
            GroupLayer mapServiceGroupLayer = new GroupLayer();
            mapServiceGroupLayer.setName(mapService.getName());
            mapServiceGroupLayer.setVisible(true);
            mapServiceGroupLayer.setTransparency(agsAlpha);
            logger.debug("visibleIds: " + mapService.getVisibleIds());
            boolean mapServiceValid = true;
            if (mapService.getVisibleIds() != null) {
                if (mapService.getVisibleIds().equals("*")) {
                    pchPrintSOELayerUrl = mapServiceUrl + "/exts/PCHPrintSOE/layerFile?layerId=" + "layers_ALL" + "&f=json";
                    logger.debug("pchPrintSOELayerUrl: " + pchPrintSOELayerUrl);
                    String jsonResp = RESTUtils.downloadContent(logger, pchPrintSOELayerUrl);
                    logger.debug("jsonResp: " + jsonResp);
                    try {
                        if (jsonResp.startsWith("{")) {
                            JSONObject jsonObj = new JSONObject(new JSONTokener(jsonResp));
                            if (!jsonObj.isNull("layerFile")) {
                                String layerFileName = jsonObj.getString("layerFile");
                                logger.debug("layerFileName: " + layerFileName);
                                File agsTmpFile = File.createTempFile("PCHPrintSOELayer", ".lyr");
                                RESTUtils.downloadFile(logger, layerFileName, agsTmpFile);
                                String agsTmpFileName = agsTmpFile.getAbsolutePath();
                                logger.debug("agsTmpFileName: " + agsTmpFileName);
                                ILayer layer = null;
                                LayerFile layerFile = new LayerFile();
                                if (layerFile.isLayerFile(agsTmpFileName)) {
                                    layerFile.open(agsTmpFileName);
                                    layer = layerFile.getLayer();
                                    layerFile.close();
                                }
                                agsTmpFile.delete();
                                if (layer != null && layerIsValid(logger, layer)) {
                                    //Add layer to groupLayer
                                    layer.setVisible(true);
                                    mapServiceGroupLayer.add(layer);
                                } else {
                                    logger.debug("Layer Invalid found !!!");
                                    mapServiceValid = false;
                                }
                            } else {
                                logger.debug("No Layer File found !!!");
                                mapServiceValid = false;
                            }
                        } else {
                            mapServiceValid = false;
                        }
                    } catch (JSONException e) {
                        logger.error("addPChPrintSOEMapServiceLayer.JSONException: " + e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    String[] visibleLayerList = mapService.getVisibleIds().split(",");
                    for (String visibleLayerId : visibleLayerList) {
                        logger.debug("visibleLayerId: " + visibleLayerId);
                        pchPrintSOELayerUrl = mapServiceUrl + "/exts/PCHPrintSOE/layerFile?layerId=" + visibleLayerId + "&f=json";
                        logger.debug("pchPrintSOELayerUrl: " + pchPrintSOELayerUrl);
                        String jsonResp = RESTUtils.downloadContent(logger, pchPrintSOELayerUrl);
                        logger.debug("jsonResp: " + jsonResp);
                        try {
                            if (jsonResp.startsWith("{")) {
                                JSONObject jsonObj = new JSONObject(new JSONTokener(jsonResp));
                                if (!jsonObj.isNull("layerFile")) {
                                    String layerFileName = jsonObj.getString("layerFile");
                                    logger.debug("layerFileName: " + layerFileName);
                                    File agsTmpFile = File.createTempFile("PCHPrintSOELayer", ".lyr");
                                    logger.debug("agsTmpFile: " + agsTmpFile);
                                    RESTUtils.downloadFile(logger, layerFileName, agsTmpFile);
                                    String agsTmpFileName = agsTmpFile.getAbsolutePath();
                                    logger.debug("agsTmpFileName: " + agsTmpFileName);
                                    ILayer layer = null;
                                    LayerFile layerFile = new LayerFile();
                                    if (layerFile.isLayerFile(agsTmpFileName)) {
                                        layerFile.open(agsTmpFileName);
                                        layer = layerFile.getLayer();
                                        layerFile.close();
                                    }
                                    agsTmpFile.delete();
                                    if (layer != null && layerIsValid(logger, layer)) {
                                        //Add layer to groupLayer
                                        if (layer instanceof ILayerGeneralProperties) {
                                            ILayerGeneralProperties layerGeneralProperties = (ILayerGeneralProperties) layer;
                                            layerGeneralProperties.setLayerDescription(mapServiceUrl + "#" + visibleLayerId);
                                        }
                                        layer.setVisible(true);
                                        mapServiceGroupLayer.add(layer);
                                    } else {
                                        logger.debug("Layer Invalid found !!!");
                                        mapServiceValid = false;
                                        break;
                                    }
                                } else {
                                    logger.debug("No Layer File found !!!");
                                    mapServiceValid = false;
                                    break;
                                }
                            } else {
                                mapServiceValid = false;
                            }
                        } catch (JSONException e) {
                            logger.error("addPChPrintSOEMapServiceLayer.JSONException: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }
            logger.debug("addPChPrintSOEMapServiceLayer.mapServiceValid: " + mapServiceValid);
            if (mapServiceValid) {
                String definitionExpression = mapService.getDefinitionExpression();
                if (definitionExpression != null) {
                    logger.debug("trying to apply 'definitionExpression'=" + definitionExpression);
                    if (mapServiceGroupLayer.getCount() == 1) {
                        ILayer layer = mapServiceGroupLayer.getLayer(0);
                        if (layer instanceof FeatureLayer) {
                            FeatureLayer featureLayer = (FeatureLayer) layer;
                            featureLayer.setDefinitionExpression(mapService.getDefinitionExpression());
                            logger.debug("applied 'definitionExpression' to FeatureLayer. FeatureLayer.name= " + featureLayer.getName());
                        } else {
                            logger.error("UNABLE to apply 'definitionExpression' to a non-FeatureLayer! layer=" + layer);
                        }
                    } else {
                        logger.error("UNABLE to apply 'definitionExpression': LayerCount>1!!! count=" + mapServiceGroupLayer.getCount());
                    }
                }
                return mapServiceGroupLayer;
            }
        } catch (IOException ioEx) {
            logger.error("addPChPrintSOEMapServiceLayer.IOException: " + ioEx.getMessage());
        }
        return null;
    }

    ///////////////////////////////////////////////
    // Add Wms Layer Michaël Hansroul 21/02/2011 //
    ///////////////////////////////////////////////
    private static ILayer addWMSLayer(SOELogger logger, PchPrintMapService mapService, short agsAlpha) {
        try {
            logger.debug("Add Wms:" + mapService.getUrl());

            //TODO:Add to wms layer parameters

            //create and configure wms connection name
            //this is used to store the connection properties

            IWMSGroupLayer wmsMapLayer = new com.esri.arcgis.carto.WMSMapLayer();
            IWMSConnectionName connName = new WMSConnectionName();
            IPropertySet propSet = new PropertySet();
            int indexParams = mapService.getUrl().indexOf("?");
            String wmsUrl = mapService.getUrl();
            if (indexParams != -1)
                wmsUrl = mapService.getUrl().substring(0, indexParams + 1);


            propSet.setProperty("URL", wmsUrl);
            if (mapService.getUrl().toUpperCase().contains("VERSION")) {
                int indexversion = mapService.getUrl().toUpperCase().indexOf("VERSION");
                String version = mapService.getUrl().substring(indexversion + 8, indexversion + 13);
                propSet.setProperty("VERSION", version);
            } else {
                //needs to have a version information for the connect
                propSet.setProperty("VERSION", "1.3.0");
            }


            //TODO:Add to wms layer parameters
            //propSet.setProperty("VERSION", "1.1.0");
            connName.setConnectionProperties(propSet);

            //uses the name information to connect to the service
            logger.debug("Connect to the wms service");
            IDataLayer dataLayer = (IDataLayer) wmsMapLayer;
            dataLayer.connect((IName) connName);

            //get service description out of the layer
            //the service description contains inforamtion about the wms categories
            //and layers supported by the service
            logger.debug("Get wms Service Description");
            IWMSServiceDescription serviceDesc = wmsMapLayer.getWMSServiceDescription();


            //for each wms layer either add the stand-alone layer or
            //group layer to the WMSMapLayer which will be added to ArcMap
            logger.debug("Add each wms layer to a group layer");
            for (int i = 0; i < serviceDesc.getLayerDescriptionCount(); i++) {
                IWMSLayerDescription layerDesc = serviceDesc.getLayerDescription(i);
                ILayer newLayer;

                if (layerDesc.getLayerDescriptionCount() == 0) {
                    IWMSLayer newWMSLayer = wmsMapLayer.createWMSLayer(layerDesc);
                    newLayer = (ILayer) newWMSLayer;
                } else {
                    IWMSGroupLayer grpLayer = wmsMapLayer.createWMSGroupLayers(layerDesc);
                    grpLayer.setExpanded(true);
                    newLayer = (ILayer) grpLayer;
                }
                newLayer.setVisible(true);
            }

            // Configure the layer before adding it to the map
            ILayer layer = (ILayer) wmsMapLayer;
            layer.setName(serviceDesc.getWMSTitle());

            //Add Layer To map
            layer.setVisible(true);

            //Set visibility
            if (mapService.getVisibleIds() != null) {
                if (mapService.getVisibleIds().equals("*")) {
                    LayerUtils.setVisibilityForLayerAndSubLayers(layer, true);
                } else {
                    String[] visibleLayerList = mapService.getVisibleIds().split(",");
                    if (visibleLayerList.length > 0 && !visibleLayerList[0].equals("")) {
                        LayerUtils.setVisibilityForLayerAndSubLayers(layer, visibleLayerList);
                    }
                }
            } else {
                LayerUtils.setVisibilityForLayerAndSubLayers(layer, true);
            }

            ((ILayerEffects) layer).setTransparency(agsAlpha);
            return layer;
        } catch (IOException ioEx) {
            logger.error("addWMSLayer.IOException: " + ioEx.getMessage());
            ioEx.printStackTrace();
        }
        return null;
    }

    //////////////////////////////////////////////////////////////////
    // Add ArcGIS Server Internet Layer Michaël Hansroul 22/02/2011 //
    //////////////////////////////////////////////////////////////////
    private static ILayer addAGSInternetLayer(SOELogger logger, short agsAlpha, PchPrintMapService mapService) {
        try {
            //create a property set to hold connection properties
            IPropertySet connectionProps = new PropertySet();
            //specify the URL for the server
            int indexServices = mapService.getUrl().toUpperCase().indexOf("SERVICES");
            int indexMapServer = mapService.getUrl().toUpperCase().indexOf("MAPSERVER");

            String servicesUrl = mapService.getUrl().substring(0, indexServices + 8);
            servicesUrl = servicesUrl.replace("rest/", "");

            String mapServiceName = mapService.getUrl().substring(indexServices + 9, indexMapServer - 1);

            connectionProps.setProperty("URL", servicesUrl);
            connectionProps.setProperty("CONNECTIONTYPE", "esriConnectionTypeInternet");

            IAGSServerConnectionFactory connectionFactory = new AGSServerConnectionFactory();
            IAGSServerConnection gisServer = connectionFactory.open(connectionProps, 0);

            IAGSEnumServerObjectName soNames = gisServer.getServerObjectNames();
            IAGSServerObjectName soName;


            soName = soNames.next();
            do {
                //logger.debug(soName.getName().toUpperCase() + " # " + mapServiceName.toUpperCase());
                if ((soName.getType().equals("MapServer")) && (soName.getName().toUpperCase().equals(mapServiceName.toUpperCase()))) {
                    break;  //found it
                }
                //keep searching the services ...
                soName = soNames.next();
            } while (soName != null);
            if (soName != null) {
                IName name = (IName) soName;
                IMapServer mapserver = (IMapServer) name.open();

                String mapname = mapserver.getDefaultMapName();

                MapServerLayer mapServerLayer = new MapServerLayer();
                mapServerLayer.serverConnect(soName, mapname);

                //Set Visible layer in the mapservice layer
                String mapServiceVisibleIds = mapService.getVisibleIds();
                configureVisibilityForMapService(mapServerLayer, mapServiceVisibleIds);

                mapServerLayer.setTransparency(agsAlpha);
                if (mapServerLayer.isValid()) {
                    return mapServerLayer;
                }
            }
        } catch (IOException ioEx) {
            logger.error("addAGSInternetLayer.IOException: " + ioEx.getMessage());
        }
        return null;
    }
    
    /**
     * checks if the configured url of the service (the SOAP-Endpoint) is reachable
     * and the content makes sense (more or less)
     * @param mapServerLayer
     * @return
     * @throws IOException
     * @throws AutomationException 
     */
    public static  boolean isSoapEndPointReachable(IMapServerLayer mapServerLayer, SOELogger logger) throws IOException, AutomationException {
        
        IAGSServerObjectName[] serverName = new IAGSServerObjectName[1];
        mapServerLayer.getConnectionInfo( serverName, new String[1], new String[1] );
        IPropertySet props =  serverName[0].getAGSServerConnectionName().getConnectionProperties();
        String soapUrl = ""+ props.getProperty( "url");
        logger.warning("Soap-Url:" + soapUrl);
        String content = RESTUtils.downloadContent( logger, soapUrl );
        //TODO: remove !
        logger.warning( "Request-Content:" + content);
        if (content == null) {
            return false;
        }
        //arcgis online for example returns an html error page, saying that
        //we can't access the soap endpoint for security reasons
        return (content.trim().startsWith( 
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"" ) ||
                content.contains( "for security reasons") );
    }
    
    
    @Override
    public String toString() {
        return "PchPrintMapService{" + "url=" + url + ", type=" + type + ", name=" + name + ", visibleIds=" + visibleIds + ", alpha=" + alpha + ", definitionExpression=" + definitionExpression + '}';
    }


    private static void configureVisibilityForMapService(MapServerLayer layer, String mapServiceVisibleIds) throws IOException {
        if (mapServiceVisibleIds != null && mapServiceVisibleIds.length() > 0) {
            if (mapServiceVisibleIds.equals("*")) {
                LayerUtils.setVisibilityForLayerAndSubLayers(layer, true);
            } else {
                String[] visibleLayerList = mapServiceVisibleIds.split(",");
                if (visibleLayerList.length > 0) {
                    List<Integer> listVisibleLayer = new ArrayList<Integer>();
                    for (String aVisibleLayerList : visibleLayerList)
                        listVisibleLayer.add(Integer.decode(aVisibleLayerList));

                    if (layer != null) {
                        int currentLayerId = 0;
                        for (int i = 0; i < layer.getCount(); i++) {
                            currentLayerId = LayerUtils.setVisibilityForAGSInternetSubLayers(layer.getLayer(i), listVisibleLayer, currentLayerId);
                        }
                    }
                }
            }
        }
    }
}
