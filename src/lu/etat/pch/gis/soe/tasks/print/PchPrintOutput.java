package lu.etat.pch.gis.soe.tasks.print;

import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IProjectedCoordinateSystem;
import com.esri.arcgis.geometry.ISpatialReference;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.esriUnits;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 9, 2010
 * Time: 5:21:26 PM
 */
public class PchPrintOutput {
    private int pageUnits = esriUnits.esriCentimeters;
    private double height = 1, width = 1, resolution = 1;
    private double mapRotation = 0;
    private String format = "pdf";
    private double[] borderWidth = {0, 0, 0, 0};       //{left,right,top,bottom}
    private List<String> toRemoveLayoutElements = new ArrayList<String>();
    private AbstractPchPrintExportSettings exportSettings;

    public PchPrintOutput(int pageUnits, double height, double width, double resolution, double mapRotation, String format) {
        this.pageUnits = pageUnits;
        this.height = height;
        this.width = width;
        this.resolution = resolution;
        this.mapRotation = mapRotation;
        this.format = format;
    }

    public PchPrintOutput(int pageUnits, double height, double width, double resolution, double mapRotation, String format, double[] borderWidth) {
        this.pageUnits = pageUnits;
        this.height = height;
        this.width = width;
        this.resolution = resolution;
        this.mapRotation = mapRotation;
        this.format = format;
        this.borderWidth = borderWidth;
    }

    public PchPrintOutput(int pageUnits, double height, double width, double resolution, double mapRotation, String format, double[] borderWidth, double scale) {
        this.pageUnits = pageUnits;
        this.height = height;
        this.width = width;
        this.resolution = resolution;
        this.mapRotation = mapRotation;
        this.format = format;
        this.borderWidth = borderWidth;
    }

    public PchPrintOutput(JSONObject jsonObj) throws JSONException {
        if (!jsonObj.isNull("pageUnits")) pageUnits = jsonObj.getInt("pageUnits");
        if (!jsonObj.isNull("height")) height = jsonObj.getDouble("height");
        if (!jsonObj.isNull("width")) width = jsonObj.getDouble("width");
        if (!jsonObj.isNull("resolution")) resolution = jsonObj.getDouble("resolution");

        if (!jsonObj.isNull("mapRotation"))
            mapRotation = jsonObj.getDouble("mapRotation");
        if (!jsonObj.isNull("format")) format = jsonObj.getString("format");
        if (!jsonObj.isNull("borderWidth")) {
            //JSONArray jsonArray = jsonObj.getJSONArray("borderWidth");
            Object obj = jsonObj.get("borderWidth");
            if (obj instanceof double[]) {
                double[] tmpArray = (double[]) obj;
                if (tmpArray.length == 4) {
                    borderWidth = tmpArray;
                }
            } else if (obj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) obj;
                if (jsonArray.length() == 4) {
                    borderWidth = new double[4];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        borderWidth[i] = jsonArray.getDouble(i);
                    }
                }
            }
        }
        if (!jsonObj.isNull("toRemoveLayoutElements")) {
            toRemoveLayoutElements.clear();
            JSONArray toRemoveLayoutELementsJsonArray = jsonObj.getJSONArray("toRemoveLayoutElements");
            for (int i = 0; i < toRemoveLayoutELementsJsonArray.length(); i++) {
                toRemoveLayoutElements.add(toRemoveLayoutELementsJsonArray.getString(i));
            }
        }

        if (!jsonObj.isNull("exportSettings")) {
            if (format.equals("pdf"))
                exportSettings = new PchPrintExportPDF(jsonObj.getJSONObject("exportSettings"));
        }

    }

    public int getPageUnits() {
        return pageUnits;
    }

    public void setPageUnits(int pageUnits) {
        this.pageUnits = pageUnits;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public double getMapRotation() {
        return mapRotation;
    }

    public void setMapRotation(double mapRotation) {
        this.mapRotation = mapRotation;
    }


    public void setFormat(String format) {
        this.format = format;
    }

    public double getResolution() {
        return resolution;
    }

    public double getWidth() {
        return width;
    }

    public String getFormat() {
        return format;
    }

    // left,right,top,bottom
    public double[] getBorderWidth() {
        return borderWidth;
    }

    public double getBorderLeft() {
        return borderWidth[0];
    }

    public double getBorderRight() {
        return borderWidth[1];
    }

    public double getBorderTop() {
        return borderWidth[2];
    }

    public double getBorderBottom() {
        return borderWidth[3];
    }

    public List<String> getToRemoveLayoutElements() {
        return toRemoveLayoutElements;
    }

    public void setToRemoveLayoutElements(List<String> toRemoveLayoutElements) {
        this.toRemoveLayoutElements = toRemoveLayoutElements;
    }

    public void setBorderWidth(double[] borderWidth) {
        this.borderWidth = borderWidth;
    }

    public AbstractPchPrintExportSettings getExportSettings() {
        return exportSettings;
    }

    public void setExportSettings(AbstractPchPrintExportSettings exportSettings) {
        this.exportSettings = exportSettings;
    }

    public double calculateScale(IEnvelope mapExtentEnvelope) {
        try {
            double mapWidth = width - borderWidth[0] - borderWidth[1];
            double mapWidthInches = convertToInches(mapWidth, pageUnits);
            double mapWidthMeters = mapWidthInches * 2.54 / 100;

            double mapextentWidth = mapExtentEnvelope.getWidth();
            double meterPerSpatialRefUnit = 1;
            ISpatialReference spatialReference = mapExtentEnvelope.getSpatialReference();
            if (spatialReference != null) {
                if (spatialReference instanceof IProjectedCoordinateSystem) {
                    IProjectedCoordinateSystem projectedCoordinateSystem = (IProjectedCoordinateSystem) spatialReference;
                    meterPerSpatialRefUnit = projectedCoordinateSystem.getCoordinateUnit().getMetersPerUnit();
                }
            }
            double mapextentWidthMeters = mapextentWidth * meterPerSpatialRefUnit;
            return mapextentWidthMeters / mapWidthMeters;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("pageUnits", pageUnits);
        jsonObj.put("height", height);
        jsonObj.put("width", width);
        jsonObj.put("resolution", resolution);
        jsonObj.put("mapRotation", mapRotation);
        jsonObj.put("format", format);
        jsonObj.put("borderWidth", Arrays.asList(borderWidth).get(0));
        if (toRemoveLayoutElements.size() > 0) {
            JSONArray array = new JSONArray();
            for (String toRemoveLayoutElement : toRemoveLayoutElements) {
                array.put(toRemoveLayoutElement);
            }
            jsonObj.put("toRemoveLayoutElements", array);
        }
        if (exportSettings != null) {
            jsonObj.put("exportSettings", exportSettings.toJSON());
        }
        return jsonObj;
    }

    public IEnvelope calculateMapFrameEnvelope() {
        try {
            Envelope envelope = new Envelope();
            envelope.putCoords(getBorderLeft(), getBorderBottom(), getWidth() - getBorderRight(), getHeight() - getBorderTop());
            return envelope;
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public double getHeightInPixel() {
        return convertToInches(height, pageUnits) * resolution;
    }

    public double getWidthInPixel() {
        return convertToInches(width, pageUnits) * resolution;
    }

    private static double convertToInches(double value, int unit) {
        if (unit == esriUnits.esriCentimeters) {
            return value / 2.54;
        } else if (unit == esriUnits.esriInches) {
            return value;
        }
        System.err.println("non implemented unit in 'convertToInches': " + unit);
        return value;

    }

    @Override
    public String toString() {
        return "PchPrintOutput{" + "pageUnits=" + pageUnits + ", height=" + height + ", width=" + width + ", resolution=" + resolution + ", mapRotation=" + mapRotation + ", format=" + format + ", borderWidth=" + borderWidth + ", toRemoveLayoutElements=" + toRemoveLayoutElements + ", exportSettings=" + exportSettings + '}';
    }

    
}
