package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.MapServer;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:06:39 PM
 */
public class AbstractTask {
    public SOELogger logger;
    public MapServer mapServer;
    public String mapServiceName;

    private static final String[] possibleArcgisServerHomes = {
            "C:\\Program Files\\ArcGIS\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server9.4\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server9.4\\server\\user\\cfg",
            "C:\\Program Files\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "C:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "D:\\Program Files\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "D:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "E:\\Program Files)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "E:\\Program Files (x86)\\ArcGIS\\Server10.0\\server\\user\\cfg",
            "/data/arcgis/server9.4/server/user/cfg",
            "/opt/arcgis/server9.4/server/user/cfg",
            "/opt/arcgis/server10.0/server/user/cfg",
            "/opt/arcgis/server10.1/server/user/cfg"
    };


    public AbstractTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        this.logger = logger;
        this.mapServer = mapServer;
        this.mapServiceName = mapServiceName;
    }

    public File getPhysicalMapServiceOutputFolder(String subFolder) throws IOException {
        if (mapServer == null) {
            return File.listRoots()[0];
        }
        logger.debug("getPhysicalMapServiceOutputFolder.start: ");
        String mapServerOutputFolder = getMapServerOutputFolder(mapServer);
        logger.debug("getPhysicalMapServiceOutputFolder.mapServerOutputFolder: " + mapServerOutputFolder);
        String mapServiceOutputFolder = mapServerOutputFolder + mapServiceName.replace("/", "_") + "_MapServer";
        logger.debug("getPhysicalMapServiceOutputFolder.mapServiceOutputFolder: " + mapServiceOutputFolder);
        File mapServiceOutputFolderFile = new File(mapServiceOutputFolder);
        if (subFolder != null && subFolder.trim().length() > 0) {
            logger.debug("getPhysicalMapServiceOutputFolder.mapServiceOutputFolder.subFolder: " + subFolder);
            File msSubFolder = new File(mapServiceOutputFolder, subFolder);
            if (msSubFolder.exists()) {
                return msSubFolder;
            } else if (msSubFolder.mkdirs()) {
                return msSubFolder;
            }
        }
        return mapServiceOutputFolderFile;
    }

    public String getVirtualMapServiceOutputFolder(String subFolder) throws IOException {
        if (mapServer == null) {
            return "??";
        }
        logger.debug("getPhysicalMapServiceOutputFolder.start: ");
        String mapServerOutputFolder = mapServer.getVirtualOutputDirectory();
        logger.debug("getPhysicalMapServiceOutputFolder.mapServerOutputFolder: " + mapServerOutputFolder);
        String mapServiceOutputFolder = mapServerOutputFolder + mapServiceName.replace("/", "_") + "_MapServer";
        logger.debug("getPhysicalMapServiceOutputFolder.mapServiceOutputFolder: " + mapServiceOutputFolder);
        if (subFolder != null && subFolder.trim().length() > 0) {
            logger.debug("getPhysicalMapServiceOutputFolder.mapServiceOutputFolder.subFolder: " + subFolder);
            return mapServiceOutputFolder + "/" + subFolder;
        }
        return mapServiceOutputFolder;
    }

    public String getMapServerOutputFolder(MapServer mapServer) throws IOException {
        String mapServerOutputFolder = mapServer.getPhysicalOutputDirectory();
        if (mapServerOutputFolder.contains("\\AppData\\Local\\Temp\\")) {
            //for local testint
            mapServerOutputFolder = "D:\\arcgisserver\\arcgisoutput\\";
        }
        return mapServerOutputFolder;
    }

    public String getMapServiceMxdFile() throws IOException {
        String filePath = mapServer.getFilePath();
        logger.debug("Got configFileName from mapServer: " + filePath);
        if (filePath == null || filePath.trim().length() == 0) {
            logger.warning("FilePath not found in mapserver! Reading configuration file manually...");
            String arcgisServerHome = null;
            for (String agHome : possibleArcgisServerHomes) {
                File agHomeFile = new File(agHome);
                if (agHomeFile.canRead()) {
                    arcgisServerHome = agHome;
                    break;
                }

            }
            if (arcgisServerHome == null) {
                logger.error("ArcGIS Server home not found !!!!");
            } else {
                logger.debug("Using ArcGIS Server home: " + arcgisServerHome);

                String configFileName = arcgisServerHome + File.separator + mapServer.getConfigurationName() + ".MapServer.cfg";
                File configFile = new File(configFileName);
                if (configFile.canRead()) {
                    BufferedReader buffReader = new BufferedReader(new FileReader(configFile));
                    String configLine;
                    filePath = null;
                    while ((configLine = buffReader.readLine()) != null) {
                        if (configLine.indexOf("<FilePath>") > 0) {
                            filePath = configLine.substring(configLine.indexOf("<FilePath>") + 10);
                            filePath = filePath.substring(0, filePath.indexOf("</FilePath>"));
                            logger.debug("FilePath found in configuration file: " + filePath);
                            break;
                        }
                    }
                    if (filePath != null && filePath.endsWith(".msd")) {
                        filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                        logger.warning("Reading mxd file instead of msd file: " + filePath);
                    }
                } else {
                    logger.error("Cannot read mapService-configuration: " + configFileName);
                }
            }
        } else {
            if (filePath.endsWith(".msd")) {
                logger.debug("found MSD file: " + filePath);
                filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                logger.debug("using MXD instead of MSD file: " + filePath);
            }
        }
        return filePath;
    }


}
