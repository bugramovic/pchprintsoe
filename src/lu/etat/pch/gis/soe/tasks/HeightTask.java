package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.analyst3d.RasterSurface;
import com.esri.arcgis.carto.ILayer;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.carto.RasterLayer;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.geometry.Polyline;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 11, 2010
 * Time: 7:09:11 PM
 */
public class HeightTask extends AbstractTask {
    private int demLayerIndex = 1;

    public HeightTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
    }

    public double getHeightForPoint(double coordX, double coordY) throws IOException {
        Point searchPoint = new Point();
        searchPoint.setX(coordX);
        searchPoint.setY(coordY);
        logger.debug("Got searchPoint: " + searchPoint);
        ILayer layer = this.mapServer.getLayer("", demLayerIndex);
        RasterLayer rLayer = (RasterLayer) layer;
        logger.debug("Got rasterLayer: " + rLayer.getName());
        RasterSurface rasterSurface = new RasterSurface();
        rasterSurface.putRaster(rLayer.getRaster(), 0);
        double height = rasterSurface.getElevation(searchPoint);
        logger.debug("Got height: " + height);
        if (Double.isNaN(height)) return 0;
        return height;
    }

    public IPolyline getHeightForLine(IPolyline polyline) throws IOException {
        logger.debug("getHeightForLine.inputLine.length : " + polyline.getLength());
        ILayer layer = this.mapServer.getLayer("", demLayerIndex);
        if (!layer.isValid()) {
            logger.error("Layer '" + layer.getName() + "' is not valid");
            return null;
        }
        RasterLayer rLayer = (RasterLayer) layer;
        logger.debug("Got rasterLayer: " + rLayer.getName());
        RasterSurface rasterSurface = new RasterSurface();
        Polyline[] retGeometry = new Polyline[1];
        int stepValue = Math.max(5, (int) (polyline.getLength() / 200));
        logger.debug("Got stepValue: " + stepValue);
        rasterSurface.putRaster(rLayer.getRaster(), 0);
        rasterSurface.interpolateShape(polyline, retGeometry, stepValue);
        if (retGeometry[0] != null && retGeometry[0] instanceof Polyline) {
            return retGeometry[0];
        }
        return null;
    }

}
