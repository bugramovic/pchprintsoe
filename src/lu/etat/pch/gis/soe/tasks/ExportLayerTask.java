package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.File;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:17:09 PM
 */
public class ExportLayerTask extends AbstractTask {
    public static final String lyrSubDirectoryName = "lyr";
    public static final String lyrFileExtension = ".lyr";
    private File physicalLayerFileDirectory;
    private String virtualLayerFileDirectory;


    public ExportLayerTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            physicalLayerFileDirectory = getPhysicalMapServiceOutputFolder(lyrSubDirectoryName);
            virtualLayerFileDirectory = getVirtualMapServiceOutputFolder(lyrSubDirectoryName);
        } catch (IOException ex) {
            logger.error("ExportLayerTask.physicalLayerFileDirectory-ERROR: " + ex.getMessage());
        }
    }

    public String exportLayers() {
        if (!physicalLayerFileDirectory.canRead()) {
            logger.error(" CAN't read the physicalLayerFileDirectory: " + physicalLayerFileDirectory);
        }
        String filePath;
        StringBuilder sb = new StringBuilder();
        try {
            filePath = getMapServiceMxdFile();
            if (filePath != null) {
                MapReader mapReader = new MapReader();
                logger.normal("Using FilePath: " + filePath);
                mapReader.open(filePath);
                IMap map = mapReader.getMap(0);
                logger.debug("open.getMap: " + map.getName());

                GroupLayer myGroupLayer = new GroupLayer();
                myGroupLayer.setName(mapServiceName.replace("/", "_"));
                int layerIndex = 0;
                for (int i = 0; i < map.getLayerCount(); i++) {
                    ILayer layer = map.getLayer(i);
                    layerIndex = exportLayerFile(layerIndex, layer);
                    myGroupLayer.add(layer);
                    layerIndex++;
                }
                writeLayerFile(myGroupLayer, physicalLayerFileDirectory + File.separator + "layers_ALL" + lyrFileExtension);
                mapReader.close();
            }
        } catch (AutomationException e) {
            e.printStackTrace();
            sb.append(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            sb.append(e.getMessage());
        }
        sb.append("new");
        return sb.toString();
    }

    private int exportLayerFile(int layerIndex, ILayer myLayer) throws IOException {
        myLayer.setVisible(true);
        if (myLayer instanceof ILayerGeneralProperties) {
            ILayerGeneralProperties layerGeneralProperties = (ILayerGeneralProperties) myLayer;
            layerGeneralProperties.setLayerDescription("" + layerIndex);
        }
        String layerName = myLayer.getName();
        layerName = layerName.replace(':', '_');
        layerName = layerName.replace(' ', '_');
        logger.debug("exportLayerFile[" + layerIndex + "]: " + layerName + "         " + myLayer);
        writeLayerFile(myLayer, physicalLayerFileDirectory + File.separator + layerIndex + "_" + layerName + lyrFileExtension);
        writeLayerFile(myLayer, physicalLayerFileDirectory + File.separator + layerIndex + lyrFileExtension);

        if (myLayer instanceof FDOGraphicsLayer) {
            FDOGraphicsLayer fdoGraphicsLayer = (FDOGraphicsLayer) myLayer;
            writeFDOGraphicLayerFile(fdoGraphicsLayer,layerIndex);
            for (int i = 0; i < fdoGraphicsLayer.getCount(); i++) {
                layerIndex++;
                //layerIndex = exportLayerFile(layerIndex, fdoGraphicsLayer.getLayer(i));
            }
        } else if (myLayer instanceof ICompositeLayer) {
            ICompositeLayer groupLayer = (ICompositeLayer) myLayer;
            logger.debug("groupLayer found [" + layerName + "] with [" + groupLayer.getCount() + "] childs");
            for (int i = 0; i < groupLayer.getCount(); i++) {
                layerIndex++;
                layerIndex = exportLayerFile(layerIndex, groupLayer.getLayer(i));
            }
        }
        return layerIndex;
    }

    private void writeFDOGraphicLayerFile(FDOGraphicsLayer fdoGraphicsLayer,int layerIndex) throws IOException {
        fdoGraphicsLayer.setVisible(true);
        int subLayerCount = fdoGraphicsLayer.getCount();
        boolean[] origState = new boolean[subLayerCount];
        for (int i = 0; i < subLayerCount; i++) {
            origState[i] = fdoGraphicsLayer.getLayer(i).isVisible();
        }
        String layerName = fdoGraphicsLayer.getName();
        writeLayerFile(fdoGraphicsLayer, physicalLayerFileDirectory + File.separator + layerIndex + "_" + layerName + lyrFileExtension);
        writeLayerFile(fdoGraphicsLayer, physicalLayerFileDirectory + File.separator + layerIndex + lyrFileExtension);
        for (int i = 0; i < subLayerCount; i++) {
            for (int j = 0; j < subLayerCount; j++) {
                ILayer subLayer = fdoGraphicsLayer.getLayer(j);
                subLayer.setVisible(i == j);
            }
            layerName = fdoGraphicsLayer.getName()+" ["+i+"]";
            int subLayerIndex = layerIndex+1+i;
            writeLayerFile(fdoGraphicsLayer, physicalLayerFileDirectory + File.separator + subLayerIndex + "_" + layerName + lyrFileExtension);
            writeLayerFile(fdoGraphicsLayer, physicalLayerFileDirectory + File.separator + subLayerIndex + lyrFileExtension);
        }
        for (int i = 0; i < subLayerCount; i++) {
             fdoGraphicsLayer.getLayer(i).setVisible(origState[i]);
        }
    }


    private void writeLayerFile(ILayer myLayer, String lyrFilename) throws IOException {
        logger.debug("writeLayerFile.lyrFile: " + lyrFilename);
        ILayerFile layerFileById = new LayerFile();
        if (layerFileById.isPresent(lyrFilename)) {
            layerFileById.open(lyrFilename);
        } else {
            layerFileById.esri_new(lyrFilename);
        }
        layerFileById.replaceContents(myLayer);
        layerFileById.save();
        layerFileById.close();
        logger.debug("lyrFile written: " + lyrFilename);
    }

    public String getLayerLyrFile(String layerId) {
        logger.debug("virtualLayerFileDirectory: " + virtualLayerFileDirectory);
        logger.debug("layerId: " + layerId);
        logger.debug("lyrFileExtension: " + lyrFileExtension);
        String retFile = virtualLayerFileDirectory + "/" + layerId + lyrFileExtension;
        JSONObject answerObject = new JSONObject();
        answerObject.put("layerFile", retFile);
        return answerObject.toString();
    }
}
