package lu.etat.pch.gis.soe.tasks;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 6, 2010
 * Time: 6:05:36 PM
 */
public class LegendTask extends AbstractTask {
    private static String legendFileExtension = ".png";
    private File legendFileDirectory;
    public LegendTask(SOELogger logger, MapServer mapServer, String mapServiceName) {
        super(logger, mapServer, mapServiceName);
        try {
            legendFileDirectory = getPhysicalMapServiceOutputFolder("legend");
        } catch (IOException ex) {
            logger.error("ExportLayerTask.layerFileDirectory-ERROR: " + ex.getMessage());
        }
    }

    public String exportLayers() {
        String filePath;
        StringBuffer sb = new StringBuffer();
        try {
            filePath = mapServer.getFilePath();
            logger.debug( "Got configFileName from mapServer: " + filePath);
            if (filePath == null || filePath.trim().length() == 0) {
                logger.warning("FilePath not found in mapserver.Reading configuration file.");
                String configFileName = "C:\\Program Files\\ArcGIS\\server\\user\\cfg\\" + mapServer.getConfigurationName() + ".MapServer.cfg";
                File configFile = new File(configFileName);
                if (!configFile.canRead()) {
                    configFileName = "/data/arcgis/server9.4/server/user/cfg";
                    configFile = new File(configFileName);
                }
                if (configFile.canRead()) {
                    BufferedReader buffReader = new BufferedReader(new FileReader(configFile));
                    String configLine;
                    filePath = null;
                    while ((configLine = buffReader.readLine()) != null) {
                        if (configLine.indexOf("<FilePath>") > 0) {
                            filePath = configLine.substring(configLine.indexOf("<FilePath>") + 10);
                            filePath = filePath.substring(0, filePath.indexOf("</FilePath>"));
                            logger.debug( "FilePath found in coniguration file: " + filePath);
                            break;
                        }
                    }
                }
            }
            if (filePath != null) {
                if (filePath.endsWith(".msd")) {
                    filePath = filePath.substring(0, filePath.length() - 4) + ".mxd";
                    logger.warning("Reading mxd file instead of msd file: " + filePath);
                }
                MapReader mapReader = new MapReader();
                logger.normal("Using FilePath: " + filePath);
                mapReader.open(filePath);
                IMap map = mapReader.getMap(0);
                logger.debug( "open.getMap: " + map.getName());
                for (int i = 0; i < map.getLayerCount(); i++) {
                    ILayer myLayer = map.getLayer(i);
                    myLayer.setVisible(true);
                    logger.debug( "gotLayer: " + myLayer.getName());

                    ImageType imageType2 = new ImageType();
                    imageType2.setFormat(esriImageFormat.esriImagePNG);
                    imageType2.setReturnType(esriImageReturnType.esriImageReturnMimeData);
                    ImageDisplay imageDisplay = new ImageDisplay();
                    imageDisplay.setHeight(500);
                    imageDisplay.setWidth(500);
                    imageDisplay.setDeviceResolution(96);
                    ImageDescription imageDesc = new ImageDescription();
                    imageDesc.setType(imageType2);
                    imageDesc.setDisplay(imageDisplay);
                    IMapDescription mapDescription = mapServer.getDefaultPageDescription().getMapFrames().getElement(0).getMapDescription();
                    ILayerDescriptions layerDescriptions = mapDescription.getLayerDescriptions();
                    for (int j = 0; j < layerDescriptions.getCount(); j++) {
                        ILayerDescription layerDescription = layerDescriptions.getElement(j);
                        logger.debug( "setVisibleFALSE: " + i + " " + j + " " + layerDescription.getID());
                        layerDescription.setVisible(false);
                        if (i == j) {
                            layerDescription.setVisible(true);
                            logger.debug( "setVisibleTRUE: " + i + " " + j + " " + layerDescription.getID());
                        }
                    }
                    mapDescription.setLayerDescriptions(layerDescriptions);

                    UID uid = new UID();
                    uid.setValue(Legend.class);
                    ILegend legend = (ILegend) map.createMapSurround(uid, null);
                    legend.setTitle("");

                    for (int j = 0; j < layerDescriptions.getCount(); j++) {
                        // ILegendItem legendItm = legend.getItem(j);
                        legend.removeItem(i);
                    }
                    HorizontalBarLegendItem legendItem = new HorizontalBarLegendItem();
                    legendItem.setLayerByRef(myLayer);
                    legendItem.setShowLayerName(false);
                    legendItem.setShowHeading(false);
                    legend.addItem(legendItem);

                    IImageResult imageResult = mapServer.exportLegend(legend, mapDescription, imageDisplay, null, imageDesc);
                    byte[] bytes = imageResult.getMimeData();


                    if (mapServiceName.indexOf("/") > 0) {
                        String mapServiceFolder = mapServiceName.substring(0, mapServiceName.indexOf("/"));
                        logger.debug( "mapServiceFolder found: " + mapServiceFolder);
                        File mapServiceFolderDirectory = new File(legendFileDirectory, mapServiceFolder);
                        if (!mapServiceFolderDirectory.exists()) {
                            if (mapServiceFolderDirectory.mkdir())
                                logger.debug( "directory created: " + mapServiceFolderDirectory);
                            else
                                logger.error("directory NOT created: " + mapServiceFolderDirectory);
                        }
                    }

                    File legendFilesDirectory = new File(legendFileDirectory, mapServiceName);
                    if (!legendFilesDirectory.exists()) {
                        if (legendFilesDirectory.mkdir())
                            logger.debug( "directory created: " + legendFilesDirectory);
                        else
                            logger.error("directory NOT created: " + legendFilesDirectory);
                    }
                    String legendFilename = legendFilesDirectory + File.separator + myLayer.getName() + legendFileExtension;
                    //String legendFilename = legendFileDirectory + File.separator + myLayer.getName() + ".png";
                    logger.detailed("trying to write legendFilename: " + legendFilename);
                    File layerLegendImageFile = new File(legendFilename);
                    FileOutputStream fileOutputStream = new FileOutputStream(layerLegendImageFile);
                    fileOutputStream.write(bytes);
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    logger.debug( "legendFilename written: " + legendFilename);

                }
                mapReader.close();
            }
        } catch (AutomationException e) {
            e.printStackTrace();
            sb.append(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            sb.append(e.getMessage());
        }
        sb.append("new");
        return sb.toString();
    }

}
