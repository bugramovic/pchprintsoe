package lu.etat.pch.gis.soe.print;

import com.esri.arcgis.carto.IMapServerDataAccess;
import com.esri.arcgis.carto.MapServer;
import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.interop.extn.ServerObjectExtProperties;
import com.esri.arcgis.server.IServerObjectExtension;
import com.esri.arcgis.server.IServerObjectHelper;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IObjectConstruct;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.IRESTRequestHandler;
import com.esri.arcgis.system.ServerUtilities;
import lu.etat.pch.gis.soe.tasks.ExportLayerTask;
import lu.etat.pch.gis.soe.tasks.print.PrintTask;
import lu.etat.pch.gis.soeClients.IPCHPrintSOE;
import lu.etat.pch.gis.utils.RESTUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 1, 2010
 * Time: 7:15:14 AM
 */
@ArcGISExtension
@ServerObjectExtProperties(
        displayName = "PCHPrintSOE",
        allSOAPCapabilities = {"printMap","printLayout"},
        description = "PCHPrintSOE, multi-mapservices print. http://bitbucket.schuller.lu",
        supportsMSD = true)
public class PCHPrintSOE implements IServerObjectExtension, IPCHPrintSOE, IObjectConstruct, IRESTRequestHandler {
    private IServerObjectHelper soHelper;
    private SOELogger logger;
    private MapServer mapServer = null;
    private IMapServerDataAccess mapServerDataAccess;

    private PrintTask printTask;
    private ExportLayerTask exportLayerTask;

    public void init(IServerObjectHelper soh) throws IOException {
        this.soHelper = soh;
        this.mapServerDataAccess = (IMapServerDataAccess) soh.getServerObject();
        this.logger = new SOELogger(ServerUtilities.getServerLogger(), 8001);
        this.mapServer = (MapServer) soHelper.getServerObject();
        logger.debug("PCHPrintSOE.init...");
    }

    public void shutdown() throws IOException {
        this.soHelper = null;
        this.mapServer = null;
        this.logger = null;
        logger.debug("PCHPrintSOE.shutdown...");
    }

    @Override
    public void construct(IPropertySet propertySet) throws IOException {
        logger.debug("PCHPrintSOE.construct...starting");
        printTask = new PrintTask(logger,mapServerDataAccess, mapServer, mapServer.getConfigurationName());
        exportLayerTask = new ExportLayerTask(logger, mapServer, mapServer.getConfigurationName());
        exportLayerTask.exportLayers();
        printTask.cleanupOldExports();
        printTask.test_debug_Legend("PCHPrintSOE.contrsuct.A");
        printTask.clear_Legend("PCHPrintSOE.contrsuct.B");
        printTask.test_debug_Legend("PCHPrintSOE.contrsuct.C");
        logger.debug("PCHPrintSOE.construct ...FINISHED");
    }

    public byte[] handleRESTRequest(String capabilities, String resourceName, String operationName, String operationInput, String outputFormat, String requestProperties, String[] responseProperties) throws IOException {
        logger.debug("In handleRESTRequest(): " + ";\ncapabilities: " + capabilities + ";\nresourceName: " + resourceName + ";\noperationName: " + operationName + ";\noperationName length: " + operationName.length() + ";\noperationInput: " + operationInput + ";\noutputFormat: " + outputFormat
                + ";\nresponseProperties length: " + responseProperties.length);
        logger.debug("Operation is permitted: " + operationName);
        try {
            if (operationName.length() == 0) {
                return getResource(resourceName);
            } else {
                JSONObject jsonObject = new JSONObject(operationInput);
                String answer;
                if (operationName.equalsIgnoreCase("printMap")) {
                    answer = printMap(jsonObject);
                } else if (operationName.equalsIgnoreCase("printLayout")) {
                    answer = printLayout(jsonObject);
                } else if (operationName.equalsIgnoreCase("layerFile")) {
                    String layerId = jsonObject.getString("layerId");
                    answer = exportLayerTask.getLayerLyrFile(layerId);
                } else {
                    logger.warning("Operation is NOT YET IMPLEMENTED: " + operationName + " !!!");
                    answer = RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" NOT YET IMPLEMENTED on resource [" + resourceName + "].",
                            new String[]{"details", "operation is not supported"});
                }
                logger.debug("handleRESTRequest.answer: " + answer);
                return answer.getBytes();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            logger.error("handleRESTRequest-JSONException: " + e.getMessage());
            return RESTUtils.sendError(0, "Operation " + "\"" + operationName + "\" ERROR [" + resourceName + "].",
                    new String[]{"JSONException", e.getMessage()}).getBytes();
        }
    }

    private byte[] getResource(String resourceName) {
        logger.debug("PChPrintSOE.getResource()...");
        try {
            if (resourceName.equalsIgnoreCase("") || resourceName.length() == 0) {
                return getRootResourceDescription().toString().getBytes();
            }
            if (resourceName.equalsIgnoreCase("documentation")) {
                URL url = PCHPrintSOE.class.getClassLoader().getResource("META-INF/docu.html");
                logger.debug("url: " + url);
                if (url != null) {
                    try {
                        InputStreamReader ins = new InputStreamReader(url.openStream());
                        BufferedReader buffReader = new BufferedReader(ins);
                        StringBuffer sb = new StringBuffer();
                        String fileLine;
                        while ((fileLine = buffReader.readLine()) != null) {
                            logger.debug("fileLine: " + fileLine);
                            sb.append(fileLine);
                        }
                        return sb.toString().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                        logger.error("PChPrintSOE.getResource.IOException: " + e.getMessage());
                    }
                }
                String retString = "<h1>!!! no help-file found !!!</h1><h2>developed by <a href=\"mailto:tom.schuller@pch.etat.lu\">tom.schuller@pch.etat.lu</a></h2>";
                return retString.getBytes();
            }
        } catch (Exception e) {
            logger.error("PChPrintSOE.getResource.Exception: " + e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    private JSONObject getRootResourceDescription() {
        return RESTUtils.createResource("PCHPrintSOE", "PCHPrintSOE, multi-mapservices print", false);
    }

    @Override
    public String getSchema() throws IOException {
        logger.debug("PChPrintSOE.getSchema()...");
        try {
            JSONObject pchExportSOE = getRootResourceDescription();
            JSONArray operationArray = new JSONArray();
            operationArray.put(RESTUtils.createOperation("printMap", "mapExtent,printOutput,mapElements,mapServices", "json"));
            operationArray.put(RESTUtils.createOperation("printLayout", "mapExtent,printOutput,mapElements,layoutElements,mapServices", "json"));
            operationArray.put(RESTUtils.createOperation("layerFile", "layerId", "json"));
            pchExportSOE.put("operations", operationArray);

            JSONArray subResourceArray = new JSONArray();
            JSONObject helpResource = RESTUtils.createResource("documentation", "documentation concerning the usage for this SOE", false);
            subResourceArray.put(helpResource);
            pchExportSOE.put("resources", subResourceArray);
            logger.debug("PChPrintSOE.getSchema().finished");
            return pchExportSOE.toString();
        } catch (JSONException e) {
            logger.error("PChPrintSOE.getSchema().JSONException: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String printMap(JSONObject json) {
        return printTask.printMap(json);
    }

    @Override
    public String printLayout(JSONObject json) {
        return printTask.printLayout(json);
    }

    @Override
    public String printMap(String jsonString) {
        try {
            return printMap(new JSONObject(jsonString));
        } catch (JSONException jsonEx) {
            logger.error("printMap(String): " + jsonEx.getMessage());
            return null;
        }
    }

    @Override
    public String printLayout(String jsonString) {
        try {
            return printLayout(new JSONObject(jsonString));
        } catch (JSONException jsonEx) {
            logger.error("printLayout(String): " + jsonEx.getMessage());
            return null;
        }
    }
}
