package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.IScreenDisplay;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.display.esriTextHorizontalAlignment;
import com.esri.arcgis.display.esriTextVerticalAlignment;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;

import java.io.IOException;

public class LayoutUtils {

    public static <T extends ITextElement> void positionTextElement(T frame, IGeometry point, int anchor) throws IOException {
        ITextSymbol textSymbol = frame.getSymbol();
        switch (anchor) {
            default:
            case esriAnchorPointEnum.esriBottomLeftCorner:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHALeft);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVABottom);
                break;
            case esriAnchorPointEnum.esriBottomMidPoint:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHACenter);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVABottom);
                break;
            case esriAnchorPointEnum.esriBottomRightCorner:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHARight);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVABottom);
                break;
            case esriAnchorPointEnum.esriCenterPoint:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHACenter);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVACenter);
                break;
            case esriAnchorPointEnum.esriLeftMidPoint:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHALeft);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVACenter);
                break;
            case esriAnchorPointEnum.esriRightMidPoint:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHARight);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVACenter);
                break;
            case esriAnchorPointEnum.esriTopLeftCorner:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHALeft);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVATop);
                break;
            case esriAnchorPointEnum.esriTopMidPoint:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHACenter);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVATop);
                break;
            case esriAnchorPointEnum.esriTopRightCorner:
                textSymbol.setHorizontalAlignment(esriTextHorizontalAlignment.esriTHARight);
                textSymbol.setVerticalAlignment(esriTextVerticalAlignment.esriTVATop);
                break;
        }
        frame.setSymbol(textSymbol);
        ((IElement) frame).setGeometry(point);
    }

    //--------------------------------------------------------------------------
    //
    //  FORMAT BOUNDING BOX
    //
    //--------------------------------------------------------------------------
    public static <T extends IFrameElement & IElement> void positionElement(T element, IPoint point, int anchor, IScreenDisplay screenDisplay) throws IOException {
        double x = point.getX();
        double y = point.getY();

        MapSurroundFrame mapSurroundFrame = (MapSurroundFrame) element;
        IMapSurround mapSurround = mapSurroundFrame.getMapSurround();

        IEnvelope newEnv = new Envelope();
        mapSurround.queryBounds(screenDisplay, null, newEnv);


        double height = newEnv.getHeight();
        double width = newEnv.getWidth();

        try {
            Envelope envelope = new Envelope();
            switch (anchor) {
                case esriAnchorPointEnum.esriBottomLeftCorner:
                    // default one
                    envelope.putCoords(
                            x,
                            y,
                            x + width,
                            y + height
                    );
                    break;
                case esriAnchorPointEnum.esriBottomMidPoint:
                    envelope.putCoords(
                            x - (width / 2),
                            y,
                            x + (width / 2),
                            y + height
                    );
                    break;
                case esriAnchorPointEnum.esriBottomRightCorner:
                    envelope.putCoords(
                            x - width,
                            y,
                            x,
                            y + height
                    );
                    break;
                case esriAnchorPointEnum.esriCenterPoint:
                    envelope.putCoords(
                            x - (width / 2),
                            y - (height / 2),
                            x + (width / 2),
                            y + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriLeftMidPoint:
                    envelope.putCoords(
                            x,
                            y - (height / 2),
                            x + width,
                            y + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriRightMidPoint:
                    envelope.putCoords(
                            x - width,
                            y - (height / 2),
                            x,
                            y + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriTopLeftCorner:
                    envelope.putCoords(
                            x,
                            y - height,
                            x + width,
                            y
                    );
                    break;
                case esriAnchorPointEnum.esriTopMidPoint:
                    envelope.putCoords(
                            x - (width / 2),
                            y - height,
                            x + (width / 2),
                            y
                    );
                    break;
                case esriAnchorPointEnum.esriTopRightCorner:
                    envelope.putCoords(
                            x - width,
                            y - height,
                            x,
                            y
                    );
                    break;
                default:
            }

            element.setGeometry(envelope);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void positionElement(IElement element, IEnvelope oldEnv, int anchorPoint) {
        try {
            double xMin = oldEnv.getXMin();
            double yMin = oldEnv.getYMin();
            double xMax = oldEnv.getXMax();
            double yMax = oldEnv.getYMax();
            double width = oldEnv.getWidth();
            double height = oldEnv.getHeight();
            Envelope envelope = new Envelope();
            switch (anchorPoint) {
                case esriAnchorPointEnum.esriBottomLeftCorner:
                    //default case
                    envelope.putCoords(
                            xMin,
                            yMin,
                            xMin,
                            yMin
                    );
                    break;
                case esriAnchorPointEnum.esriBottomMidPoint:
                    envelope.putCoords(
                            xMin - (width / 2),
                            yMin,
                            xMin + (width / 2),
                            yMin + height
                    );
                    break;
                case esriAnchorPointEnum.esriBottomRightCorner:
                    envelope.putCoords(
                            xMin - width,
                            yMin,
                            xMin,
                            yMin + height
                    );
                    break;
                case esriAnchorPointEnum.esriCenterPoint:
                    envelope.putCoords(
                            xMin - (width / 2),
                            yMin - (height / 2),
                            xMin + (width / 2),
                            yMin + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriLeftMidPoint:
                    envelope.putCoords(
                            xMin,
                            yMin - (height / 2),
                            xMin + width,
                            yMin + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriRightMidPoint:
                    envelope.putCoords(
                            xMin - width,
                            yMin - (height / 2),
                            xMin,
                            yMin + (height / 2)
                    );
                    break;
                case esriAnchorPointEnum.esriTopLeftCorner:
                    envelope.putCoords(
                            xMin,
                            yMin - height,
                            xMin + width,
                            yMin
                    );
                    break;
                case esriAnchorPointEnum.esriTopMidPoint:
                    envelope.putCoords(
                            xMin - (width / 2),
                            yMin - height,
                            xMin + (width / 2),
                            yMin
                    );
                    break;
                case esriAnchorPointEnum.esriTopRightCorner:
                    envelope.putCoords(
                            xMin - width,
                            yMin - height,
                            xMin,
                            yMin
                    );
                    break;
                default:
                    System.err.println("\n\n\tNKNOWN anchor: " + anchorPoint + "\n\n");
            }

            element.setGeometry(envelope);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}