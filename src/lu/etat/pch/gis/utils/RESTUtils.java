package lu.etat.pch.gis.utils;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Apr 08, 2010
 * Time: 6:01:55 AM
 */


import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.server.json.JSONTokener;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class RESTUtils {
    public static String sendError(int code, String message, String details[]) {
        try {
            JSONObject errorObject = new JSONObject();
            JSONObject error = new JSONObject();
            error.put("code", code);
            error.put("message", message);
            if (details != null) {
                JSONArray detailsArray = new JSONArray();
                for (String detail : details) {
                    detailsArray.put(detail);
                }
                error.put("details", detailsArray);
            }
            errorObject.put("warning", error);
            return errorObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] getParameterValueFromOperationInput(String operationInputJSON, String parameterNames[]) {
        try {
            JSONTokener tokener = new JSONTokener(operationInputJSON);
            String parameterValues[] = new String[parameterNames.length];
            JSONObject parameterObject = new JSONObject(tokener);
            for (int i = 0; i < parameterValues.length; i++) {
                if (parameterObject.has(parameterNames[i]))
                    parameterValues[i] = parameterObject.getString(parameterNames[i]);
            }
            return parameterValues;
        } catch (JSONException e) {
            sendError(0, "Incorrect parameter name provided.", new String[]{
                    (new StringBuilder()).append("Cause: ").append(e.getCause().getMessage()).toString(), e.getMessage()
            });
        }
        return null;
    }

    public static JSONObject createOperation(String operationName, String parameterList, String supportedOutputFormatsList) {
        try {
            JSONObject operation = new JSONObject();
            if (operationName != null && operationName.length() > 0)
                operation.put("name", operationName);
            else
                throw new Exception("Operation must have valid name");
            if (parameterList != null && parameterList.length() > 0) {
                JSONArray operationParamArray = new JSONArray();
                String parameters[] = parameterList.split(",");
                for (String parameter : parameters) {
                    operationParamArray.put(parameter.trim());
                }
                operation.put("parameters", operationParamArray);
            } else {
                throw new Exception("Operation must have parameters. If your operation does not requires params, please convert it to a sub-resource!"
                );
            }
            if (supportedOutputFormatsList != null && supportedOutputFormatsList.length() > 0) {
                JSONArray outputFormatsArray = new JSONArray();
                String outputFormats[] = supportedOutputFormatsList.split(",");
                int len$ = outputFormats.length;
                for (int i$ = 0; i$ < len$; i$++) {
                    String outputFormat = outputFormats[i$];
                    outputFormatsArray.put(outputFormat.trim());
                }

                operation.put("supportedOutputFormats", outputFormatsArray);
            } else {
                throw new Exception("Operation must have supported output formats specified!");
            }
            return operation;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static JSONObject createOperation(String operationName, String[] parameterList, String supportedOutputFormatsList) {
        StringBuffer paramNames = new StringBuffer();
        for (String paramName : parameterList) {
            paramNames.append(paramName).append(",");
        }
        return createOperation(operationName, paramNames.toString(), supportedOutputFormatsList);
    }

    public static JSONObject createResource(String name, String description, boolean isCollection) {
        try {
            JSONObject json = new JSONObject();
            if (name.length() > 0 && name != null) {
                json.put("name", name);
            } else {
                throw new Exception("Resource must have a valid name.");
            }
            json.put("description", description);
            json.put("isCollection", isCollection);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean urlExists(SOELogger logger, String url) {
        String content = downloadContent(logger, url);
        if (content != null) {
            logger.debug("urlExists.content.size: " + content.length());
            boolean errorCode400 = content.contains("\"code\":400,");
            logger.debug("urlExists.content.errorCode400: " + errorCode400);
            if (errorCode400) {
                logger.debug("urlExists.content: " + content);
            }
            return !errorCode400;
        } else {
            logger.debug("urlExists.content: " + content + "  -> returning false");
            return false;
        }
    }

    public static String downloadContent(SOELogger logger, String url) {
        logger.warning("downloadContent.url: "+url);
        if (url != null) {
            BufferedInputStream bis = null;
            try {
                System.setProperty("java.net.useSystemProxies", "true");
                URLConnection con = new URL(url).openConnection();
                con.setConnectTimeout(500);
                bis = new BufferedInputStream(con.getInputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    sb.append(inputLine);
                in.close();
                String retContent = sb.toString();
                logger.debug("downloadContent.retConent.size: " + retContent.length());
                return retContent;
            } catch (IOException ioEx) {
                logger.debug("downloadContent.IOException: " + ioEx.getMessage());
                System.setProperty("java.net.useSystemProxies", "false");
                logger.debug("downloadContent.useSystemProxies=false");
                try {
                    URLConnection con = new URL(url).openConnection();
                    con.setConnectTimeout(500);
                    bis = new BufferedInputStream(con.getInputStream());
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String inputLine;
                    while ((inputLine = in.readLine()) != null)
                        sb.append(inputLine);
                    in.close();
                    String retContent = sb.toString();
                    logger.debug("downloadContent.noProxy.retConent.size: " + retContent.length());
                    return retContent;
                } catch (IOException e) {
                    logger.debug("downloadContent.noProxy.IOException: " + ioEx.getMessage());
                    e.printStackTrace();
                }
            } finally {
                if (bis != null)
                    try {
                        bis.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                        logger.debug("downloadContent.IOException2: " + ioe.getMessage());
                    }
            }
        }
        logger.error("downloadContent.url = null, return empty string");
        return "";
    }

    public static void downloadFile(SOELogger logger, String fileUrl, File destination) {
        logger.debug("downloadFile.fileUrl: " + fileUrl);
        logger.debug("downloadFile.destination: " + destination);
        if (fileUrl != null && destination != null) {
            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;
            try {
                System.setProperty("java.net.useSystemProxies", "true");
                URL url = new URL(fileUrl);
                URLConnection urlc = url.openConnection();
                urlc.setConnectTimeout(500);
                bis = new BufferedInputStream(urlc.getInputStream());
                bos = new BufferedOutputStream(new FileOutputStream(destination));
                int i;
                while ((i = bis.read()) != -1) {
                    bos.write(i);
                }
            } catch (FileNotFoundException e) {
                logger.error("downloadFile.FileNotFoundException: " + e.getMessage());
            } catch (MalformedURLException e) {
                logger.error("downloadFile.MalformedURLException: " + e.getMessage());
            } catch (IOException e) {
                logger.error("downloadFile.IOException: " + e.getMessage());
                logger.error("downloadFile.tryingWithNoSystemProxy");
                try {
                    System.setProperty("java.net.useSystemProxies", "false");
                    URL url = new URL(fileUrl);
                    URLConnection urlc = url.openConnection();
                    urlc.setConnectTimeout(500);
                    bis = new BufferedInputStream(urlc.getInputStream());
                    bos = new BufferedOutputStream(new FileOutputStream(destination));
                    int i;
                    while ((i = bis.read()) != -1) {
                        bos.write(i);
                    }
                } catch (FileNotFoundException e1) {
                    logger.error("downloadFile.noProxy.FileNotFoundException: " + e.getMessage());
                } catch (IOException e1) {
                    logger.error("downloadFile.noProxy.IOException: " + e.getMessage());
                }
            } finally {
                if (bis != null)
                    try {
                        bis.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                if (bos != null)
                    try {
                        bos.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
            }
        }
    }
}
  