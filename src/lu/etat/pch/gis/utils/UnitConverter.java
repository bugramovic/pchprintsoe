/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import com.esri.arcgis.system.esriUnits;

/**
 * converts between strings and the esriUnits-constants
 * 
 * @author abajramovic
 */
public class UnitConverter {

    /**
     * 
     * @param unitString kilometers|meters|centimeters|millimeters|miles|feet|years|inches|degrees|nauticalmiles|points
     * @return 
     */
    public static int getEsriUnitFromString(String unitString) {
        unitString = unitString.toLowerCase();
        //order is important, because "contains" is used, so we 
        //can match esriMillimeters and millimeters
        if (unitString == null || "".equals( unitString)) {
            return esriUnits.esriUnknownUnits;
        } else if ("kilometers".contains( unitString)) {
            return esriUnits.esriKilometers;
        } else if ("centimeters".contains( unitString)) {
            return esriUnits.esriCentimeters;
        } else if ("millimeters".contains( unitString)) {
            return esriUnits.esriMillimeters;
        } else if ("meters".contains( unitString)) {
            return esriUnits.esriMeters;
        } else if ("nauticalmiles".contains( unitString)) {
            return esriUnits.esriNauticalMiles;
        } else if ("miles".contains( unitString)) {
            return esriUnits.esriMiles;
        } else if ("feet".contains( unitString)) {
            return esriUnits.esriFeet;
        } else if ("yards".contains( unitString)) {
            return esriUnits.esriYards;
        } else if ("inches".contains( unitString)) {
            return esriUnits.esriInches;
        } else if ("degrees".contains( unitString)) {
            return esriUnits.esriDecimalDegrees;
        } else if ("points".contains( unitString)) {
            return esriUnits.esriPoints;
        } else {
            return esriUnits.esriUnknownUnits;
        }
    }
}
