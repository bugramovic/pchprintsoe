package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.gisclient.IWMSLayerDescription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/23/11
 * Time: 6:40 AM
 */
public class LayerUtils {
    public static void setVisibilityForLayerAndSubLayers(ILayer layer, boolean visibility) throws IOException {
        layer.setVisible(visibility);
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                setVisibilityForLayerAndSubLayers(compositeLayer.getLayer(i), visibility);
            }
        }
    }

    public static void setVisibilityForLayerAndSubLayers(ILayer layer, String[] visibleLayerList) throws IOException {
        if (layer instanceof ICompositeLayer) {
            ICompositeLayer compositeLayer = (ICompositeLayer) layer;
            layer.setVisible(true);
            for (int i = 0; i < compositeLayer.getCount(); i++) {
                setVisibilityForLayerAndSubLayers(compositeLayer.getLayer(i), visibleLayerList);
            }
        } else {
            layer.setVisible(false);
            WMSLayer wmsLayer = (WMSLayer) layer;

            IWMSLayerDescription ld = wmsLayer.getWMSLayerDescription();
            for (String aVisibleLayerList : visibleLayerList) {
                if (aVisibleLayerList.equalsIgnoreCase(ld.getName())) {
                    layer.setVisible(true);
                    break;
                }
            }
        }
    }

    public static int setVisibilityForAGSInternetSubLayers(ILayer layer, List<Integer> visibleLayerList, int currentId) throws IOException {
        if (layer instanceof IMapServerGroupLayer) {
            IMapServerGroupLayer compositeLayer = (IMapServerGroupLayer) layer;
            if (compositeLayer.getCount() > 0) {
                layer.setVisible(true);
            } else {
                if (visibleLayerList.contains(currentId)) {
                    layer.setVisible(true);
                } else {
                    layer.setVisible(false);
                }
            }
            currentId++;

            for (int i = 0; i < compositeLayer.getCount(); i++) {
                currentId = setVisibilityForAGSInternetSubLayers(compositeLayer.getLayer(i), visibleLayerList, currentId);
            }
        } else {
            if (visibleLayerList.contains(currentId)) {
                layer.setVisible(true);
            } else {
                layer.setVisible(false);
            }
            currentId++;
        }

        return currentId;
    }

    public static List<ILayer> getMapLayers(SOELogger logger, IMap map, boolean removeLayerFromMap) throws IOException {
        List<ILayer> originalLayerList = new ArrayList<ILayer>();
        int layerCount = map.getLayerCount();
        while (layerCount > 0) {
            ILayer layer = map.getLayer(0);
            originalLayerList.add(layer);
            if (removeLayerFromMap) {
                logger.debug("LayerUtils.getMapLayers.removing Layer: " + layer.getName());
                map.deleteLayer(layer);
                layerCount = map.getLayerCount();
            }else{
                layerCount--;
            }
        }
        return originalLayerList;
    }

    public static void restoreMapLayers(SOELogger logger, IMap map, List<ILayer> originalLayerList, PageLayout pageLayout) throws IOException {
        map.clearLayers();
        while (map.getLayerCount() > 0) {
            ILayer layer = map.getLayer(0);
            logger.debug("removing tempLayer: " + layer.getName());
            map.deleteLayer(layer);
        }
        Collections.reverse(originalLayerList);
        for (ILayer iLayer : originalLayerList) {
            logger.debug("adding origLayer: " + iLayer.getName());
            System.out.println("iLayer.getName() = " + iLayer.getName());
            map.addLayer(iLayer);
        }
    }
}
