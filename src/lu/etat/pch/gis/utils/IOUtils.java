/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * implements some features found RESTUtils, but can handle different URL "schemas"
 * Delegates work to RESTUtils for http-urls/schemas, but also handles classpath and file-resources
 * (see definied constants/schema-prefixes)
 * 
 * @author abajramovic
 */
public class IOUtils {

    public static String CLASSPATH_PREFIX = "classpath:";
    public static String FILE_PREFIX = "file:";
    public static String HTTP_PREFIX = "http:";

    /**
     * always returns a copy of the resource, you the consumer doesn't have to worry
     * about deleting something
     * @param logger
     * @param url
     * @param target 
     */
    public static void getResourceAsFile(SOELogger logger, String url, File target) {
        if (url == null) {
            return;
        }
        try {
            if (url.startsWith( HTTP_PREFIX )) {
                RESTUtils.downloadFile( logger, url, target );
            } else if (url.startsWith( CLASSPATH_PREFIX )) {
                FileOutputStream out = new FileOutputStream( target, false );
                InputStream in = IOUtils.class.getResourceAsStream( stripResourceUrl( url ) );
                copyAndCloseStreams( in, out );
            } else if (url.startsWith( FILE_PREFIX)) {
                //we need to copy the file, so the consumer can savely delete the (temp) file
                FileInputStream in =  new FileInputStream( stripResourceUrl( url) );
                FileOutputStream out = new FileOutputStream( target, false);
                copyAndCloseStreams( in, out );
            }
        } catch (IOException ex) {
            throw new RuntimeException( ex );
        }
    }

    public static boolean resourceExists(SOELogger logger, String url) {
        if (url == null) {
            return false;
        }
        try {
            if (url.toLowerCase().startsWith( HTTP_PREFIX )) {
                return RESTUtils.urlExists( logger, url );
            } else if (url.toLowerCase().startsWith( CLASSPATH_PREFIX )) {
                InputStream in = IOUtils.class.getResourceAsStream( stripResourceUrl( url ) );
                if (in == null) {
                    return false;
                } else {
                    in.close();
                    return true;
                }
            } else if (url.startsWith( FILE_PREFIX )) {
                return new File( stripResourceUrl( url ) ).exists();
            } else {
                return false;
            }
        } catch (IOException ex) {
            logger.normal( "could not open Resource at " + url + " cause:" + ex.getMessage() );
            return false;
        }

    }

    private static String stripResourceUrl(String url) {
        if (url.toLowerCase().startsWith( CLASSPATH_PREFIX )) {
            return url.substring( CLASSPATH_PREFIX.length(), url.length() );
        } else if (url.toLowerCase().startsWith( FILE_PREFIX )) {
            return url.substring( FILE_PREFIX.length(), url.length() );
        } else {
            return url;
        }
    }
    
    private static void copyAndCloseStreams(InputStream in, OutputStream out) throws IOException {
        int b;
        try {
            while (( b = in.read() ) > -1) {
                out.write( b );
            }
        } finally {
            out.close();
            in.close();
        }
    }
}
