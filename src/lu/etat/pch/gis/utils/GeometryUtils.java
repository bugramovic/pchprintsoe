package lu.etat.pch.gis.utils;

import com.esri.arcgis.geometry.*;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 3/15/11
 * Time: 6:49 AM
 */
public class GeometryUtils {
    public static IGeometry convertEnvvelope2Polygon(Envelope geom) throws IOException {
        Polygon polygon = new Polygon();
        polygon.addPoint(geom.getUpperLeft(), null, null);
        polygon.addPoint(geom.getUpperRight(), null, null);
        polygon.addPoint(geom.getLowerRight(), null, null);
        polygon.addPoint(geom.getLowerLeft(), null, null);
        polygon.addPoint(geom.getUpperLeft(), null, null);
        return polygon;
    }
}
