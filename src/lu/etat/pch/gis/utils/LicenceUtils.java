package lu.etat.pch.gis.utils;

import com.esri.arcgis.system.AoInitialize;
import com.esri.arcgis.system.EngineInitializer;
import com.esri.arcgis.system.esriLicenseProductCode;
import com.esri.arcgis.system.esriLicenseStatus;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 21, 2010
 * Time: 6:33:41 PM
 */
public class LicenceUtils {
    public static void initializeArcGISLicenses() {
        try {
            EngineInitializer.initializeVisualBeans();
            AoInitialize aoInit = new AoInitialize();
            AoInitialize ao = new AoInitialize();

            if (ao.isProductCodeAvailable(esriLicenseProductCode.esriLicenseProductCodeArcInfo)
                    == esriLicenseStatus.esriLicenseAvailable)
                ao.initialize(esriLicenseProductCode.esriLicenseProductCodeArcInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
