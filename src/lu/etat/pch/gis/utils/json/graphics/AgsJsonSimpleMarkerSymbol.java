package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.display.SimpleMarkerSymbol;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 6:02:03 PM
 */
public class AgsJsonSimpleMarkerSymbol extends AgsJsonSymbol {
    public static final String esriSMSCircle = "esriSMSCircle";
    public static final String esriSMSCross = "esriSMSCross";
    public static final String esriSMSDiamond = "esriSMSDiamond";
    public static final String esriSMSSquare = "esriSMSSquare";
    public static final String esriSMSX = "esriSMSX";


    private String style;
    private AgsJsonRGBColor color;
    private double size, angle;
    private double xOffset, yOffset;
    private AgsJsonSimpleLineSymbol outline;

    public AgsJsonSimpleMarkerSymbol(String style, AgsJsonRGBColor color, double size) {
        this.type = "esriSMS";
        this.style = style;
        this.color = color;
        this.size = size;
    }

    public AgsJsonSimpleMarkerSymbol(JSONObject jsonObject) {
        this.type = "esriSMS";
        try {
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
            if (!jsonObject.isNull("color")) {
                Object colorObj = jsonObject.get("color");
                color = new AgsJsonRGBColor(colorObj);
                /*
                String colorValue = jsonObject.getString("color");
                if (colorValue.startsWith("["))
                    color = new AgsJsonRGBColor(new JSONArray(colorValue));
                else color = new AgsJsonRGBColor(new JSONObject(colorValue));
                */
            }
            if (!jsonObject.isNull("size")) size = jsonObject.getDouble("size");
            if (!jsonObject.isNull("angle")) angle = jsonObject.getDouble("angle");
            if (!jsonObject.isNull("xOffset")) xOffset = jsonObject.getDouble("xOffset");
            if (!jsonObject.isNull("yOffset")) yOffset = jsonObject.getDouble("yOffset");
            if (!jsonObject.isNull("outline"))
                outline = new AgsJsonSimpleLineSymbol(jsonObject.getJSONObject("outline"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setXOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public void setYOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public AgsJsonSimpleLineSymbol getOutline() {
        return outline;
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("color", color.toJSON());
        symbObject.put("size", size);
        symbObject.put("angle", angle);
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        if (outline != null) {
            symbObject.put("outline", outline);
        }
        return symbObject;
    }

    public ISymbol toArcObject() throws IOException, JSONException {
        if (type.equalsIgnoreCase("esriSMS")) {
            SimpleMarkerSymbol simpleMarkerSymbol = new SimpleMarkerSymbol();
            if (style != null)
                simpleMarkerSymbol.setStyle(getEsriStyleCode(style));
            if (color != null)
                simpleMarkerSymbol.setColor(color.toArcObject());
            if (size > -1)
                simpleMarkerSymbol.setSize(size);
            simpleMarkerSymbol.setAngle(angle);
            if(outline!=null) {
                simpleMarkerSymbol.setOutline(true);
                simpleMarkerSymbol.setOutlineColor(outline.getColor().toArcObject());
                simpleMarkerSymbol.setOutlineSize(outline.getWidth());
            }
            /*
            if (alpha>=0) {
                IColor color = simpleMarkerSymbol.getColor();
                color.setTransparency((byte) (symbolObject.getDouble("alpha") * 255D));
                simpleMarkerSymbol.setColor(color);
            }
            */
            simpleMarkerSymbol.setXOffset(xOffset);
            simpleMarkerSymbol.setYOffset(yOffset);
            return simpleMarkerSymbol;
        }
        return null;
    }
    /*
{
"type" : "esriSMS",
"style" : "< esriSMSCircle | esriSMSCross | esriSMSDiamond | esriSMSSquare | esriSMSX >",
"color" : <color>,
"size" : <size>,
"angle" : <angle>,
"xoffset" : <xoffset>,
"yoffset" : <yoffset>,
"outline" : { //if outline has been specified
 "color" : <color>,
 "width" : <width>
}
}
    */
}
