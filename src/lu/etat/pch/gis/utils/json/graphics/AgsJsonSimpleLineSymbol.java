package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.display.SimpleLineSymbol;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 6:56:20 AM
 */
public class AgsJsonSimpleLineSymbol extends AgsJsonSymbol {
    public static final String esriSLSDash = "esriSLSDash";
    public static final String esriSLSDashDotDot = "esriSLSDashDotDot";
    public static final String esriSLSDot = "esriSLSDot";
    public static final String esriSLSNull = "esriSLSNull";
    public static final String esriSLSSolid = "esriSLSSolid";

    private String style;
    private AgsJsonRGBColor color;
    private double width;

    public AgsJsonSimpleLineSymbol(String style, AgsJsonRGBColor color, double width) {
        this.type = "esriSLS";
        this.style = style;
        this.color = color;
        this.width = width;
    }

    public AgsJsonSimpleLineSymbol(JSONObject jsonObject) {
        this.type = "esriSLS";
        try {
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
            if (!jsonObject.isNull("color")){
                String colorValue = jsonObject.getString("color");
                if (colorValue.startsWith("["))
                    color = new AgsJsonRGBColor(new JSONArray(colorValue));
                else color = new AgsJsonRGBColor(new JSONObject(colorValue));
            }
            if (!jsonObject.isNull("width")) width = jsonObject.getDouble("width");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public AgsJsonRGBColor getColor() {
        return color;
    }

    public void setColor(AgsJsonRGBColor color) {
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("color", color.toJSON());
        symbObject.put("width", width);
        return symbObject;
    }

    public ISymbol toArcObject() throws IOException, JSONException {
        if (type.equalsIgnoreCase("esriSLS")) {
            SimpleLineSymbol simpleLineSymbol = new SimpleLineSymbol();
            if (style != null)
                simpleLineSymbol.setStyle(getEsriStyleCode(style));
            if (color != null)
                simpleLineSymbol.setColor(color.toArcObject());
            if (width > 0)
                simpleLineSymbol.setWidth(width);
            return simpleLineSymbol;
        } else {
            return null;
        }
    }

}
