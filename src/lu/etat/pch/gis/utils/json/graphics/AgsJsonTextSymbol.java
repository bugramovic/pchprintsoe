package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.support.ms.stdole.StdFont;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 6, 2010
 * Time: 5:41:59 PM
 */
public class AgsJsonTextSymbol extends AgsJsonSymbol {
    private static final String TYPE = "esriTS";
    public static final String FONT_STYLE_ITALIC = "italic";
    public static final String FONT_STYLE_NORMAL = "normal";
    public static final String FONT_STYLE_OBLIQUE = "oblique";

    public static final String FONT_WEIGHT_BOLD = "bold";
    public static final String FONT_WEIGHT_BOLDER = "bolder";
    public static final String FONT_WEIGHT_LIGHTER = "lighter";
    public static final String FONT_WEIGHT_NORMAL = "normal";

    public static final String FONT_DECORATION_LINE_THROUGH = "line-through";
    public static final String FONT_DECORATION_UNDERLINE = "underline";
    public static final String FONT_DECORATION_NONE = "none";


    private String text = null;
    private String fontFamily = "Arial";
    private long fontSize = 12;
    private String fontStyle = FONT_STYLE_NORMAL;
    private String fontWeight = FONT_WEIGHT_NORMAL;
    private String fontDecoration = FONT_DECORATION_NONE;
    private double xOffset = 0, yOffset = 0, angle = 0;
    private AgsJsonRGBColor color;
    private AgsJsonRGBColor backgroundColor;

    public AgsJsonTextSymbol(String text, int fontSize) {
        this.type = TYPE;
        this.text = text;
        this.fontSize = fontSize;
    }

    public AgsJsonTextSymbol(String text, String fontFamily, int fontSize, String fontStyle, String fontWeight, String fontDecoration) {
        this.type = TYPE;
        this.text = text;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.fontWeight = fontWeight;
        this.fontDecoration = fontDecoration;
    }

    public AgsJsonTextSymbol(String text, String fontFamily, int fontSize, String fontStyle, String fontWeight, String fontDecoration, double xOffset, double yOffset, double angle) {
        this.type = TYPE;
        this.text = text;
        this.fontFamily = fontFamily;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.fontWeight = fontWeight;
        this.fontDecoration = fontDecoration;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.angle = angle;
    }

    public AgsJsonTextSymbol(JSONObject symbolObject) {
        this.type = TYPE;
        try {
            if (!symbolObject.isNull("font")) {
                JSONObject fontObject = symbolObject.getJSONObject("font");
                if (!fontObject.isNull("family")) fontFamily = (fontObject.getString("family"));
                if (!fontObject.isNull("size")) fontSize = (fontObject.getLong("size"));
                if (!fontObject.isNull("style")) fontStyle = fontObject.getString("style");
                if (!fontObject.isNull("weight")) fontWeight = fontObject.getString("weight");
                if (!fontObject.isNull("decoration")) fontDecoration = fontObject.getString("decoration");
            }
            if (!symbolObject.isNull("text")) text = (symbolObject.getString("text"));
            if (!symbolObject.isNull("xoffset")) xOffset = (symbolObject.getDouble("xoffset"));
            if (!symbolObject.isNull("yoffset")) yOffset = (symbolObject.getDouble("yoffset"));
            if (!symbolObject.isNull("angle")) angle = (symbolObject.getDouble("angle"));
            if (!symbolObject.isNull("color"))
                color = new AgsJsonRGBColor(symbolObject.get("color"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("text", text);
        if (color != null)
            symbObject.put("color", color.toJSON());
        symbObject.put("xOffset", xOffset);
        symbObject.put("yOffset", yOffset);
        symbObject.put("angle", angle);
        JSONObject fontObject = new JSONObject();
        fontObject.put("family", fontFamily);
        fontObject.put("size", fontSize);
        fontObject.put("style", fontStyle);
        fontObject.put("weight", fontWeight);
        fontObject.put("decoration", fontDecoration);
        symbObject.put("font", fontObject);
        return symbObject;
    }

    public ISymbol toArcObject() throws IOException {
        StdFont font = new StdFont();
        font.setName(fontFamily);
        font.setBold(true);
        if (fontStyle.equals(FONT_STYLE_ITALIC)) font.setItalic(true);
        //todo; font.setWeight()
        if (fontDecoration.equals(FONT_DECORATION_UNDERLINE)) font.setUnderline(true);
        else if (fontDecoration.equals(FONT_DECORATION_LINE_THROUGH)) font.setStrikethrough(true);

        TextSymbol textSymbol = new TextSymbol();
        textSymbol.setFont(font);
        textSymbol.setSize(fontSize);
        if (text != null) textSymbol.setText(text);
        textSymbol.setXOffset(xOffset);
        textSymbol.setYOffset(yOffset);
        textSymbol.setAngle(angle);
        if (color != null) textSymbol.setColor(color.toArcObject());
        if (backgroundColor != null) {
            LineCallout lineCallout = new LineCallout();
            SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
            simpleFillSymbol.setColor(backgroundColor.toArcObject());
            lineCallout.setBorderByRef(simpleFillSymbol);
            ILineSymbol accentBar = lineCallout.getAccentBar();
            lineCallout.setLeaderLineByRef(null);
            lineCallout.setAccentBarByRef(null);

            textSymbol.setBackgroundByRef(lineCallout);
        }
        return textSymbol;
    }
}
