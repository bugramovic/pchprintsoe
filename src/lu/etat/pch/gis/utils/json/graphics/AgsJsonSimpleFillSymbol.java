package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.*;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 7:55:40 AM
 */
public class AgsJsonSimpleFillSymbol extends AgsJsonSymbol {
    public static final String esriSFSBackwardDiagonal = "esriSFSBackwardDiagonal";
    public static final String esriSFSCross = "esriSFSCross";
    public static final String esriSFSDiagonalCross = "esriSFSDiagonalCross";
    public static final String esriSFSForwardDiagonal = "esriSFSForwardDiagonal";
    public static final String esriSFSHorizontal = "esriSFSHorizontal";
    public static final String esriSFSNull = "esriSFSNull";
    public static final String esriSFSSolid = "esriSFSSolid";
    public static final String esriSFSVertical = "esriSFSVertical";

    private String style;
    private AgsJsonRGBColor color;
    private AgsJsonSimpleLineSymbol outline;

    public AgsJsonSimpleFillSymbol(String style, AgsJsonRGBColor color) {
        this.type = "esriSFS";
        this.color = color;
        this.style = style;
    }

    public AgsJsonSimpleFillSymbol(String style, AgsJsonRGBColor color, AgsJsonSimpleLineSymbol outline) {
        this.type = "esriSFS";
        this.color = color;
        this.style = style;
        this.outline = outline;
    }

    public AgsJsonSimpleFillSymbol(JSONObject jsonObject) {
        this.type = "esriSFS";
        try {
            if (!jsonObject.isNull("color")) {
                Object colorObj = jsonObject.get("color");
                color = new AgsJsonRGBColor(colorObj);
            }else{
                color = new AgsJsonRGBColor(255,0,0, (byte) 0);
            }
            if (!jsonObject.isNull("outline"))
                outline = new AgsJsonSimpleLineSymbol(jsonObject.getJSONObject("outline"));
            if (!jsonObject.isNull("style")) style = jsonObject.getString("style");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("color", color.toJSON());
        if (outline != null) {
            symbObject.put("outline", outline.toJSON());
        }
        return symbObject;
    }

    public ISymbol toArcObject() throws IOException {
        if (type.equalsIgnoreCase("esriSFS")) {
            if (style == null || style.equalsIgnoreCase(esriSFSSolid) || style.equalsIgnoreCase(esriSFSNull)) {
                //only SOLID style is suported (see Javadoc: SimpleFillSymbol.setStyle)
                SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
                if (style != null) {
                    if (style.equalsIgnoreCase(esriSFSSolid))
                        simpleFillSymbol.setStyle(esriSimpleFillStyle.esriSFSSolid);
                    else if (style.equalsIgnoreCase(esriSFSNull))
                        simpleFillSymbol.setStyle(esriSimpleFillStyle.esriSFSNull);
                }
                if (color != null)
                    simpleFillSymbol.setColor(color.toArcObject());
                if (outline != null)
                    simpleFillSymbol.setOutline((ILineSymbol) outline.toArcObject());
                return simpleFillSymbol;
            } else {
                Double firstAngle = null;
                Double secondAngle = null;
                if (style.equalsIgnoreCase(esriSFSCross)) {
                    firstAngle = 0d;
                    secondAngle = 90d;
                } else if (style.equalsIgnoreCase(esriSFSDiagonalCross)) {
                    firstAngle = 45d;
                    secondAngle = 135d;
                } else if (style.equalsIgnoreCase(esriSFSForwardDiagonal))
                    firstAngle = 45d;
                else if (style.equalsIgnoreCase(esriSFSBackwardDiagonal))
                    firstAngle = 135d;
                else if (style.equalsIgnoreCase(esriSFSHorizontal))
                    firstAngle = 90d;
                else if (style.equalsIgnoreCase(esriSFSVertical))
                    firstAngle = 0d;

                MultiLayerFillSymbol multiLayerFillSymbol = new MultiLayerFillSymbol();
                if (secondAngle != null) {
                    LineFillSymbol lineFillSymbol1 = new LineFillSymbol();
                    lineFillSymbol1.setColor(color.toArcObject());
                    lineFillSymbol1.setAngle(secondAngle);
                    lineFillSymbol1.setSeparation(5);
                    ILineSymbol lineSymbol = (ILineSymbol) outline.toArcObject();
                    lineSymbol.setWidth(0);
                    lineFillSymbol1.setOutline(lineSymbol);
                    multiLayerFillSymbol.addLayer(lineFillSymbol1);
                }
                if (firstAngle != null) {
                    LineFillSymbol lineFillSymbol2 = new LineFillSymbol();
                    lineFillSymbol2.setColor(color.toArcObject());
                    lineFillSymbol2.setAngle(firstAngle);
                    lineFillSymbol2.setSeparation(5);
                    lineFillSymbol2.setOutline((ILineSymbol) outline.toArcObject());
                    multiLayerFillSymbol.addLayer(lineFillSymbol2);
                }
                return multiLayerFillSymbol;
            }
        }
        return new SimpleFillSymbol();
    }
}
