package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 6, 2010
 * Time: 8:20:16 AM
 */
public class AgsJsonPictureMarkerSymbol extends AgsJsonSymbol {
    private String url;
    private AgsJsonRGBColor color;
    private AgsJsonSimpleLineSymbol outline;
    private double size, angle, xoffset, yoffset, xscale, yscale;

    public AgsJsonPictureMarkerSymbol(AgsJsonRGBColor color, String url, double size) {
        this.type = "esriPMS";
        this.url = url;
        this.color = color;
        this.size = size;
    }

    public AgsJsonPictureMarkerSymbol(JSONObject jsonObject) {
        this.type = "esriPMS";
        try {
            url = jsonObject.getString("url");
            if (!jsonObject.isNull("outline")) outline = new AgsJsonSimpleLineSymbol(jsonObject.getJSONObject("outline"));
            if (!jsonObject.isNull("size")) size = jsonObject.getDouble("size");
            if (!jsonObject.isNull("angle")) angle = jsonObject.getDouble("angle");
            if (!jsonObject.isNull("xoffset")) xoffset = jsonObject.getDouble("xoffset");
            if (!jsonObject.isNull("yoffset")) yoffset = jsonObject.getDouble("yoffset");
            if (!jsonObject.isNull("xscale")) xscale = jsonObject.getDouble("xscale");
            if (!jsonObject.isNull("yscale")) yscale = jsonObject.getDouble("yscale");
            if (!jsonObject.isNull("color")) color = new AgsJsonRGBColor(jsonObject.getJSONObject("color"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setOutline(AgsJsonSimpleLineSymbol outline) {
        this.outline = outline;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setoffset(double xoffset, double yoffset) {
        this.xoffset = xoffset;
        this.yoffset = yoffset;
    }

    public void setScale(double xscale, double yscale) {
        this.xscale = xscale;
        this.yscale = yscale;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("url", url);
        symbObject.put("color", color.toJSON());
        if (outline != null) {
            symbObject.put("outline", outline.toJSON());
        }
        symbObject.put("size", size);
        symbObject.put("angle", angle);
        symbObject.put("xoffset", xoffset);
        symbObject.put("yoffset", yoffset);
        symbObject.put("xscale", xscale);
        symbObject.put("yscale", yscale);
        return symbObject;
    }

    @Override
    public ISymbol toArcObject() throws IOException {
        //todo: implement toArcObject cod
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /*
  {
"type" : "esriPFS",
"url" : "<pictureUrl>",
"color" : <color>,
"outline" : <simpleLineSymbol>, //if outline has been specified
"width" : <width>,
"height" : <height>,
   "angle" : <angle>,
   "xoffset" : <xoffset>,
   "yoffset" : <yoffset>,
"xscale": <xscale>,
"yscale": <yscale>
}
    */
}
