package lu.etat.pch.gis.utils.json.graphics;

import com.esri.arcgis.display.ISymbol;
import com.esri.arcgis.display.esriSimpleFillStyle;
import com.esri.arcgis.display.esriSimpleLineStyle;
import com.esri.arcgis.display.esriSimpleMarkerStyle;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 6:02:22 PM
 */
public abstract class AgsJsonSymbol {
    public static Map<String, Integer> esriStyleMap;
    protected String type;

    public abstract JSONObject toJSON() throws JSONException;
    public abstract ISymbol toArcObject() throws IOException;

    public static int getEsriStyleCode(String style) throws JSONException {
        if (esriStyleMap.containsKey(style)) {
            return esriStyleMap.get(style);
        }
        throw new JSONException("AgsJsonSymbol.getEsriStyleCode !! unknown style: " + style);
    }

    static {
        AgsJsonSymbol.esriStyleMap = new HashMap<String, Integer>();
        AgsJsonSymbol.esriStyleMap.put("esriSMSCircle", esriSimpleMarkerStyle.esriSMSCircle);
        AgsJsonSymbol.esriStyleMap.put("esriSMSCross", esriSimpleMarkerStyle.esriSMSCross);
        AgsJsonSymbol.esriStyleMap.put("esriSMSDiamond", esriSimpleMarkerStyle.esriSMSDiamond);
        AgsJsonSymbol.esriStyleMap.put("esriSMSSquare", esriSimpleMarkerStyle.esriSMSSquare);
        AgsJsonSymbol.esriStyleMap.put("esriSMSX", esriSimpleMarkerStyle.esriSMSX);

        AgsJsonSymbol.esriStyleMap.put("esriSLSSolid", esriSimpleLineStyle.esriSLSSolid);
        AgsJsonSymbol.esriStyleMap.put("esriSLSDash", esriSimpleLineStyle.esriSLSDash);
        AgsJsonSymbol.esriStyleMap.put("esriSLSDot", esriSimpleLineStyle.esriSLSDot);
        AgsJsonSymbol.esriStyleMap.put("esriSLSDashDot", esriSimpleLineStyle.esriSLSDashDot);
        AgsJsonSymbol.esriStyleMap.put("esriSLSDashDotDot", esriSimpleLineStyle.esriSLSDashDotDot);
        AgsJsonSymbol.esriStyleMap.put("esriSLSNull", esriSimpleLineStyle.esriSLSNull);
        AgsJsonSymbol.esriStyleMap.put("esriSLSInsideFrame", esriSimpleLineStyle.esriSLSInsideFrame);

        AgsJsonSymbol.esriStyleMap.put("esriSFSSolid", esriSimpleFillStyle.esriSFSSolid);
        AgsJsonSymbol.esriStyleMap.put("esriSFSNull", esriSimpleFillStyle.esriSFSNull);
        AgsJsonSymbol.esriStyleMap.put("esriSFSHollow", esriSimpleFillStyle.esriSFSHollow);
        AgsJsonSymbol.esriStyleMap.put("esriSFSHorizontal", esriSimpleFillStyle.esriSFSHorizontal);
        AgsJsonSymbol.esriStyleMap.put("esriSFSVertical", esriSimpleFillStyle.esriSFSVertical);
        AgsJsonSymbol.esriStyleMap.put("esriSFSForwardDiagonal", esriSimpleFillStyle.esriSFSForwardDiagonal);
        AgsJsonSymbol.esriStyleMap.put("esriSFSBackwardDiagonal", esriSimpleFillStyle.esriSFSBackwardDiagonal);
        AgsJsonSymbol.esriStyleMap.put("esriSFSCross", esriSimpleFillStyle.esriSFSCross);
        AgsJsonSymbol.esriStyleMap.put("esriSFSDiagonalCross", esriSimpleFillStyle.esriSFSDiagonalCross);
    }


}
