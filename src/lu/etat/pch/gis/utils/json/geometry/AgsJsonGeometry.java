package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geometry.*;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 6, 2010
 * Time: 6:59:19 PM
 */
public abstract class AgsJsonGeometry {
    public abstract IGeometry toArcObject() throws IOException;

    public abstract JSONObject toJSON() throws JSONException;

    public static AgsJsonGeometry convertJsonToAgsJsonGeom(JSONObject graphicGeomObj) throws JSONException, IOException {
        AgsJsonGeometry geom = null;
        if (!graphicGeomObj.isNull("x")) {
            geom = new AgsJsonPoint(graphicGeomObj);
        } else if (!graphicGeomObj.isNull("xmin")) {
            geom = new AgsJsonEnvelope(graphicGeomObj);
        } else if (!graphicGeomObj.isNull("paths")) {
            geom = new AgsJsonPolyline(graphicGeomObj);
        } else if (!graphicGeomObj.isNull("rings")) {
            geom = new AgsJsonPolygon(graphicGeomObj);
        }else{
            System.err.println("AgsJsonGeometry.convertJsonToAgsJsonGeom: unknown jsonObject !!! "+graphicGeomObj.toString());
        }
        return geom;
    }

    public static JSONObject convertGeomToJson(IGeometry geom) throws IOException, JSONException {
        return convertGeomToJSON(geom, true);
    }

    private static JSONObject convertGeomToJSON(IGeometry geom, boolean includeSpatialRef)
            throws IOException, JSONException {
        if (geom instanceof Point) {
            Point point = (Point) geom;

            JSONObject pointJson = new JSONObject();
            if (!Double.isNaN(point.getX())) pointJson.put("x", point.getX());
            if (!Double.isNaN(point.getY())) pointJson.put("y", point.getY());
            if (point.isZAware()) {
                if (!Double.isNaN(point.getZ())) pointJson.put("z", point.getZ());
            }
            if (includeSpatialRef && point.getSpatialReference() != null)
                pointJson.put("spatialReference", convertSpatialReferenceToJSON(point.getSpatialReference()));
            return pointJson;
        } else if (geom instanceof IEnvelope) {
            IEnvelope env = (IEnvelope) geom;
            JSONObject envJson = new JSONObject();
            if (!Double.isNaN(env.getXMin())) envJson.put("xmin", env.getXMin());
            if (!Double.isNaN(env.getYMin())) envJson.put("ymin", env.getYMin());
            if (!Double.isNaN(env.getXMax())) envJson.put("xmax", env.getXMax());
            if (!Double.isNaN(env.getYMax())) envJson.put("ymax", env.getYMax());
            if (includeSpatialRef && env.getSpatialReference() != null)
                envJson.put("spatialReference", convertSpatialReferenceToJSON(env.getSpatialReference()));
            return envJson;
        } else if (geom instanceof Polyline) {
            Polyline polyline = (Polyline) geom;
            JSONObject polylineJson = new JSONObject();
            JSONArray pathArrayJson = new JSONArray();
            for (int pathIndex = 0; pathIndex < polyline.getGeometryCount(); pathIndex++) {
                IGeometry geomPath = polyline.getGeometry(pathIndex);
                if (geomPath instanceof Path) {
                    Path path = (Path) geomPath;
                    JSONArray pointArray = new JSONArray();
                    for (int i = 0; i < path.getPointCount(); i++) {
                        IPoint point = path.getPoint(i);
                        pointArray.put(convertGeomToJSON(point, false));
                    }
                    pathArrayJson.put(pointArray);
                }
            }
            polylineJson.put("paths", pathArrayJson);
            if (includeSpatialRef && polyline.getSpatialReference() != null)
                polylineJson.put("spatialReference", convertSpatialReferenceToJSON(polyline.getSpatialReference()));
            return polylineJson;
        } else if (geom instanceof Polygon) {
            Polygon polygon = (Polygon) geom;
            JSONObject polygonJson = new JSONObject();
            JSONArray ringArrayJson = new JSONArray();
            for (int pathIndex = 0; pathIndex < polygon.getGeometryCount(); pathIndex++) {
                IGeometry geomPath = polygon.getGeometry(pathIndex);
                if (geomPath instanceof Ring) {
                    Ring ring = (Ring) geomPath;
                    JSONArray pointArray = new JSONArray();
                    for (int i = 0; i < ring.getPointCount(); i++) {
                        IPoint point = ring.getPoint(i);
                        pointArray.put(convertGeomToJSON(point, false));
                    }
                    ringArrayJson.put(pointArray);
                }
            }
            polygonJson.put("rings", ringArrayJson);
            if (includeSpatialRef && polygon.getSpatialReference() != null)
                polygonJson.put("spatialReference", convertSpatialReferenceToJSON(polygon.getSpatialReference()));
            return polygonJson;
        } else {
            System.err.println((new StringBuilder()).append("convertGeomToJson.UNKNOWN format: ").append(geom).toString());
            return null;
        }
    }

    public static JSONObject convertSpatialReferenceToJSON(ISpatialReference spatialReference) throws IOException, JSONException {
        JSONObject srObject = new JSONObject();
        srObject.put("wkid", spatialReference.getFactoryCode());
        return srObject;
    }

    public static String convertGeometryTypeToJsonString(int geometryType) {
        switch (geometryType) {
            case esriGeometryType.esriGeometryPoint:
                return "esriGeometryPoint";
            case esriGeometryType.esriGeometryMultipoint:
                return "esriGeometryMultipoint";
            case esriGeometryType.esriGeometryPolyline:
                return "esriGeometryPolyline";
            case esriGeometryType.esriGeometryPolygon:
                return "esriGeometryPolygon";
            case esriGeometryType.esriGeometryEnvelope:
                return "esriGeometryEnvelope";
            default:
                return "??esriGeometryType??";
        }
    }
}
