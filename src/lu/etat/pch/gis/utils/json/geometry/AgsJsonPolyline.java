package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geodatabase.JSONDeserializerGdb;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.geometry.IPolyline;
import com.esri.arcgis.geometry.Polyline;
import com.esri.arcgis.geometry.esriGeometryType;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.JSONReader;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:51:11 PM
 */
public class AgsJsonPolyline extends AgsJsonGeometry {
    private List<AgsJsonPath> paths = new ArrayList<AgsJsonPath>();

    public AgsJsonPolyline() {
    }

    public AgsJsonPolyline(JSONObject json) {
        if (json != null) {
            if (!json.isNull("paths")) {
                try {
                    JSONReader reader = new JSONReader();
                    reader.readFromString(json.toString());
                    JSONDeserializerGdb ds = new JSONDeserializerGdb();
                    ds.initDeserializer(reader, null);
                    Polyline polyline = (Polyline) ds.readGeometry(esriGeometryType.esriGeometryPolyline);
                    for (int i = 0; i < polyline.getPointCount(); i++) {
                        IPoint pt = polyline.getPoint(i);
                        addPoint(0, pt.getX(), pt.getY());
                    }
                } catch (AutomationException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                throw new JSONException("Input JSON does not contain path information");
            }
        } else {
            throw new JSONException("JSON is null");
        }
    }

    public void addPoint(int ringIndex, double x, double y) throws Exception {
        AgsJsonPoint agsJsonPoint = new AgsJsonPoint(x, y);
        addPoint(ringIndex, agsJsonPoint);
    }

    public void addPoint(int ringIndex, AgsJsonPoint agsJsonPoint) throws Exception {
        if (ringIndex < paths.size()) {
            paths.get(ringIndex).addPoint(agsJsonPoint);
        } else if (ringIndex == paths.size()) {
            //add a new ring
            AgsJsonPath agsJsonPath = new AgsJsonPath();
            agsJsonPath.addPoint(agsJsonPoint);
            paths.add(agsJsonPath);
        } else {
            throw new Exception("invalid pathIndex");
        }
    }


    private class AgsJsonPath {
        List<AgsJsonPoint> agsJsonPointList;

        private AgsJsonPath() {
            agsJsonPointList = new ArrayList<AgsJsonPoint>();
        }

        public void addPoint(AgsJsonPoint agsJsonPoint) {
            agsJsonPointList.add(agsJsonPoint);
        }

        public List<AgsJsonPoint> getPoints() {
            return agsJsonPointList;
        }

        public int getPointCount() {
            return agsJsonPointList.size();
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject geomObject = new JSONObject();
        JSONArray ringArray = new JSONArray();
        for (AgsJsonPath agsJsonPath : paths) {
            if (agsJsonPath.getPointCount() > 1) {
                JSONArray pointsArray = new JSONArray();
                for (AgsJsonPoint agsJsonPoint : agsJsonPath.getPoints()) {
                    JSONArray coordArray = new JSONArray();
                    coordArray.put(agsJsonPoint.getX());
                    coordArray.put(agsJsonPoint.getY());
                    pointsArray.put(coordArray);
                }
                ringArray.put(pointsArray);
            }
        }
        geomObject.put("paths", ringArray);
        return geomObject;
    }

    public IPolyline toArcObject() throws JSONException, IOException {
        Polyline polyline = new Polyline();
        for (AgsJsonPath agsJsonPath : paths) {
            List<AgsJsonPoint> pathPoints = agsJsonPath.getPoints();
            for (AgsJsonPoint agsJsonPoint : pathPoints) {
                polyline.addPoint((IPoint) agsJsonPoint.toArcObject(), null, null);
            }
        }
        return polyline;
    }
}
