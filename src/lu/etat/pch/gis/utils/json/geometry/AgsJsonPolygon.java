package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geodatabase.JSONDeserializerGdb;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.geometry.IPolygon;
import com.esri.arcgis.geometry.Polygon;
import com.esri.arcgis.geometry.esriGeometryType;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.JSONReader;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:21:19 PM
 */
public class AgsJsonPolygon extends AgsJsonGeometry {
    private List<AgsJsonRing> rings = new ArrayList<AgsJsonRing>();
    ;

    public AgsJsonPolygon() {
    }

    public AgsJsonPolygon(JSONObject json) {
        if (json != null) {
            if (!json.isNull("rings")) {
                try {
                    JSONReader reader = new JSONReader();
                    reader.readFromString(json.toString());
                    JSONDeserializerGdb ds = new JSONDeserializerGdb();
                    ds.initDeserializer(reader, null);
                    Polygon polygon = (Polygon) ds.readGeometry(esriGeometryType.esriGeometryPolygon);
                    for (int i = 0; i < polygon.getPointCount(); i++) {
                        IPoint pt = polygon.getPoint(i);
                        addPoint(0, pt.getX(), pt.getY());
                    }

                } catch (AutomationException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                throw new JSONException("Input JSON does not contain rings information");
            }
        } else {
            throw new JSONException("JSON is null");
        }

    }

    public void addPoint(int ringIndex, double x, double y) throws Exception {
        AgsJsonPoint agsJsonPoint = new AgsJsonPoint(x, y);
        if (ringIndex < rings.size()) {
            rings.get(ringIndex).addPoint(agsJsonPoint);
        } else if (ringIndex == rings.size()) {
            //add a new ring
            AgsJsonRing AgsJsonRing = new AgsJsonRing();
            AgsJsonRing.addPoint(agsJsonPoint);
            rings.add(AgsJsonRing);
        } else {
            throw new Exception("invalid ringIndex");
        }
    }


    private class AgsJsonRing {
        List<AgsJsonPoint> pointAgses;

        private AgsJsonRing() {
            pointAgses = new ArrayList<AgsJsonPoint>();
        }

        public void addPoint(AgsJsonPoint agsJsonPoint) {
            pointAgses.add(agsJsonPoint);
        }

        public List<AgsJsonPoint> getPoints() {
            return pointAgses;
        }

        public int getPointCount() {
            return pointAgses.size();
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject graphics2Geom = new JSONObject();

        JSONArray ringArray = new JSONArray();
        for (AgsJsonRing AgsJsonRing : rings) {
            if (AgsJsonRing.getPointCount() > 1) {
                JSONArray pointsArray = new JSONArray();
                for (AgsJsonPoint agsJsonPoint : AgsJsonRing.getPoints()) {
                    JSONArray coordArray = new JSONArray();
                    coordArray.put(agsJsonPoint.getX());
                    coordArray.put(agsJsonPoint.getY());
                    pointsArray.put(coordArray);
                }
                pointsArray.put(pointsArray.get(0));
                ringArray.put(pointsArray);
            }
        }
        graphics2Geom.put("rings", ringArray);
        return graphics2Geom;
    }

    public IPolygon toArcObject() throws IOException {
        Polygon polygon = new Polygon();
        for (AgsJsonRing agsJsonRing : rings) {
            List<AgsJsonPoint> ringPoints = agsJsonRing.getPoints();
            for (AgsJsonPoint agsJsonPoint : ringPoints) {
                polygon.addPoint((IPoint) agsJsonPoint.toArcObject(), null, null);
            }
        }
        return polygon;
    }

}
