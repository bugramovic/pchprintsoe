package lu.etat.pch.gis.utils.json.geometry;

import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.Point;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:15:29 PM
 */

public class AgsJsonPoint extends AgsJsonGeometry {
    private double x, y;

    public AgsJsonPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public AgsJsonPoint(JSONObject json) {
        try {
            this.x = json.getDouble("x");
            this.y = json.getDouble("y");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject grapchics1Geom = new JSONObject();
        grapchics1Geom.put("x", x);
        grapchics1Geom.put("y", y);
        return grapchics1Geom;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public IGeometry toArcObject() throws IOException {
        Point pt = new Point();
        pt.putCoords(x, y);
        return pt;
    }
}
