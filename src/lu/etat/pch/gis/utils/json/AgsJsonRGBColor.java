package lu.etat.pch.gis.utils.json;

import com.esri.arcgis.display.IColor;
import com.esri.arcgis.display.RgbColor;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 4, 2010
 * Time: 5:43:59 PM
 */
public class AgsJsonRGBColor {
    private int red = -1, green = -1, blue = -1;
    private byte alpha = (byte) 255;
    private int rGB = -1;

    public AgsJsonRGBColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public AgsJsonRGBColor(int red, int green, int blue, byte alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public AgsJsonRGBColor(IColor color) {
        if(color instanceof RgbColor ) {
            RgbColor rgbColor= (RgbColor) color;
            try {
                this.red=((RgbColor) color).getRed();
                this.green=((RgbColor) color).getGreen();
                this.blue=((RgbColor) color).getBlue();
                this.alpha=((RgbColor) color).getTransparency();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public AgsJsonRGBColor(Object obj) {
        try {
            if(obj instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) obj;
                red = jsonObject.getInt("red");
                green = jsonObject.getInt("green");
                blue = jsonObject.getInt("blue");
                alpha = (byte) jsonObject.getInt("alpha");
            }else if (obj instanceof JSONArray) {
                JSONArray jsonArray = (JSONArray) obj;
                if (jsonArray != null) {
                    if (jsonArray.length() == 4) {
                        red = (jsonArray.getInt(0));
                        green = (jsonArray.getInt(1));
                        blue = (jsonArray.getInt(2));
                        alpha = ((byte) jsonArray.getInt(3));
                    } else if (jsonArray.length() == 3) {
                        red = (jsonArray.getInt(0));
                        green = (jsonArray.getInt(1));
                        blue = (jsonArray.getInt(2));
                    }

                }
            } else if (obj instanceof Integer) {
                rGB = (Integer) obj;
            } else if (obj instanceof String) {
                int colorVal = Integer.parseInt((String) obj);
                rGB = (colorVal);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray toJSON() {
        JSONArray graphics2ColorObject = new JSONArray();
        graphics2ColorObject.put(red);
        graphics2ColorObject.put(green);
        graphics2ColorObject.put(blue);
        graphics2ColorObject.put(alpha);
        return graphics2ColorObject;
    }

    public RgbColor toArcObject() throws IOException {
        RgbColor color = null;
        if (rGB > -1) {
            color = new RgbColor();
            color.setRGB(rGB);
        } else if (red > -1 && green > -1 && blue > -1) {
            color = new RgbColor();
            color.setRed(red);
            color.setGreen(green);
            color.setBlue(blue);
            color.setTransparency(alpha);
        }
        return color;
        /*
        if (colorObj instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) colorObj;
            if (jsonArray != null) {
                if (jsonArray.length() == 4) {
                    color = new RgbColor();
                    color.setRed(jsonArray.getInt(0));
                    color.setGreen(jsonArray.getInt(1));
                    color.setBlue(jsonArray.getInt(2));
                    color.setTransparency((byte) jsonArray.getInt(3));
                } else {
                    throw new JSONException("Input Color-JSONArray must contain 4 elements");
                }
            } else {
                throw new JSONException("JSON is null");
            }
        } else if (colorObj instanceof Integer) {
            color = new RgbColor();
            color.setRGB(((Integer) colorObj).intValue());
        } else if (colorObj instanceof String) {
            int colorVal = Integer.parseInt((String) colorObj);
            color = new RgbColor();
            color.setRGB(colorVal);
        } else {
            throw new JSONException("invalid color values, can't convert to json!!");
        }
        return color;
        */
    }

}
