package lu.etat.pch.gis.utils.json;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.*;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.GeometryUtils;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleFillSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleLineSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleMarkerSymbol;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSymbol;
import lu.etat.pch.gis.utils.json.layoutElements.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 7, 2010
 * Time: 7:34:28 AM
 */
public class AgsJsonElement {
    private String name;
    private AgsJsonGeometry geometry;
    private AgsJsonSymbol symbol;
    private AgsJsonLayoutElementObject layoutElementObject;
    private int anchorPoint;

    public AgsJsonElement(String name, AgsJsonGeometry geometry, AgsJsonSymbol symbol) {
        this.name = name;
        this.geometry = geometry;
        this.symbol = symbol;
    }

    public AgsJsonElement(String name, AgsJsonGeometry geometry, AgsJsonSymbol symbol, int anchorPoint) {
        this.name = name;
        this.geometry = geometry;
        this.anchorPoint = anchorPoint;
        this.symbol = symbol;
    }

    public AgsJsonElement(String name, AgsJsonGeometry geometry, AgsJsonLayoutElementObject layoutElementObject) {
        this.name = name;
        this.geometry = geometry;
        this.layoutElementObject = layoutElementObject;
    }

    public AgsJsonElement(String name, AgsJsonGeometry geometry, AgsJsonLayoutElementObject layoutElementObject, int anchorPoint) {
        this.name = name;
        this.geometry = geometry;
        this.layoutElementObject = layoutElementObject;
        this.anchorPoint = anchorPoint;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("name", name);
        symbObject.put("geometry", geometry.toJSON());
        if (symbol != null) symbObject.put("symbol", symbol.toJSON());
        return symbObject;
    }

    public IElement toArcObject(PageLayout pageLayout) throws IOException {
        IGeometry geom = geometry.toArcObject();
        IElement layoutElem = null;
        if (layoutElementObject != null) {
            layoutElem = layoutElementObject.toArcObject(pageLayout, geom, anchorPoint);
            if (geom instanceof IPoint && anchorPoint != esriAnchorPointEnum.esriTopLeftCorner) {
                IEnvelope layoutElemEnvelope = layoutElem.getGeometry().getEnvelope();
                if (layoutElemEnvelope != null) {
                    LayoutUtils.positionElement(layoutElem, layoutElemEnvelope, anchorPoint);
                }
            }
        }
        return layoutElem;
    }

    public void setAnchorPoint(int anchorPoint) {
        this.anchorPoint = anchorPoint;
    }

    public int getAnchorPoint() {
        return anchorPoint;
    }

    public static IElement parseJson(SOELogger logger, IMapFrame mapFrame, JSONObject jsonObj, IEnvelope layoutEnvelope, PageLayout pageLayout) throws JSONException, IOException {
        JSONObject geomJson = jsonObj.getJSONObject("geometry");
        AgsJsonGeometry agsGeom = AgsJsonGeometry.convertJsonToAgsJsonGeom(geomJson);
        if (!jsonObj.has("symbol") || jsonObj.isNull("symbol")) return null;
        JSONObject graphicSymbolObj = jsonObj.getJSONObject("symbol");
        if (graphicSymbolObj.isNull("type")) return null;
        String type = graphicSymbolObj.getString("type");
        String name = null;
        if (!graphicSymbolObj.isNull("name")) name = graphicSymbolObj.getString("name");

        IElement retElement = null;
        if (mapFrame != null) {
            IGeometry geom = agsGeom.toArcObject();
            if (jsonObj.has("width") && !jsonObj.isNull("width") && jsonObj.has("height") && !jsonObj.isNull("height")) {
                if (geom instanceof IPoint) {
                    IPoint pt = (IPoint) geom;
                    Envelope env = new Envelope();
                    String width = jsonObj.getString("width");
                    String height = jsonObj.getString("height");
                    if (width.length() > 0 && height.length() > 0) {
                        try {
                            env.putCoords(pt.getX(), pt.getY(), pt.getX() + Double.parseDouble(width), pt.getY() + Double.parseDouble(height));
                            geom = env;
                        } catch (NumberFormatException e) {
                            logger.error("width<>height NumberFormatException: " + width + "<>" + height);
                        }
                    }
                }
            }
            AgsJsonLayoutElementObject layoutElementObject = null;
            if (type.equalsIgnoreCase(AgsJsonScaleText.TYPE)) {
                layoutElementObject = new AgsJsonScaleText(logger, graphicSymbolObj);
            } else if (type.equalsIgnoreCase(AgsJsonScaleBar.TYPE)) {
                layoutElementObject = new AgsJsonScaleBar(logger, graphicSymbolObj);
            } else if (type.equalsIgnoreCase(AgsJsonLegend.TYPE)) {
                layoutElementObject = new AgsJsonLegend(logger, graphicSymbolObj);
            } else if (type.equalsIgnoreCase(AgsJsonNorthArrow.TYPE)) {
                layoutElementObject = new AgsJsonNorthArrow(logger, graphicSymbolObj);
            } else if (type.equalsIgnoreCase(AgsJsonPictureElement.TYPE)) {
                layoutElementObject = new AgsJsonPictureElement(logger, graphicSymbolObj);
            } else if (type.equalsIgnoreCase(AgsJsonTextElement.TYPE) || type.equalsIgnoreCase("esriTS")) {
                layoutElementObject = new AgsJsonTextElement(logger, graphicSymbolObj);
            }
            int anchor = esriAnchorPointEnum.esriBottomLeftCorner;
            if (!jsonObj.isNull("anchor")) {
                anchor = convertAnchorTextToEsriInt(jsonObj.getString("anchor"));
            }
            if (layoutElementObject != null)
                retElement = layoutElementObject.toArcObject(pageLayout, geom, anchor);
        } else if (type.equalsIgnoreCase("esriSMS") || type.equalsIgnoreCase("esriPMS")) {
            MarkerElement markerElement = new MarkerElement();
            ISymbol markerSymbol = new AgsJsonSimpleMarkerSymbol(graphicSymbolObj).toArcObject();
            if (markerSymbol == null) {
                markerSymbol = new SimpleMarkerSymbol();
                ((SimpleMarkerSymbol) markerSymbol).setSize(40D);
                ((SimpleMarkerSymbol) markerSymbol).setStyle(0);
            }
            IGeometry geom = agsGeom.toArcObject();
            markerElement.setSymbol((IMarkerSymbol) markerSymbol);
            if (geom instanceof IPoint)
                markerElement.setGeometry(geom);
            retElement = markerElement;
        } else if (type.equalsIgnoreCase("esriSLS")) {
            LineElement lineElement = new LineElement();
            ISymbol lineSymbol = new AgsJsonSimpleLineSymbol(graphicSymbolObj).toArcObject();
            if (lineSymbol == null) {
                lineSymbol = new SimpleLineSymbol();
                ((SimpleLineSymbol) lineSymbol).setStyle(esriSimpleLineStyle.esriSLSSolid);
                ((SimpleLineSymbol) lineSymbol).setWidth(100D);
            }
            IGeometry geom = agsGeom.toArcObject();
            if (geom instanceof IPolyline)
                lineElement.setGeometry(geom);
            lineElement.setSymbol((ILineSymbol) lineSymbol);
            retElement = lineElement;
        } else if (type.equalsIgnoreCase("esriSFS")) {
            PolygonElement polygonElement = new PolygonElement();
            if (name != null) polygonElement.setName(name);
            IGeometry geom = agsGeom.toArcObject();
            if (geom instanceof Envelope) {
                geom = GeometryUtils.convertEnvvelope2Polygon((Envelope) geom);
            }
            polygonElement.setGeometry(geom);
            IFillSymbol fillSymbol = (IFillSymbol) new AgsJsonSimpleFillSymbol(graphicSymbolObj).toArcObject();
            polygonElement.setSymbol(fillSymbol);
            retElement = polygonElement;
        } else if (type.equalsIgnoreCase("esriTS")) {
            AgsJsonTextElement agsJsonTextElement = new AgsJsonTextElement(logger, graphicSymbolObj);
            retElement = agsJsonTextElement.toArcObject(pageLayout, agsGeom.toArcObject(), esriAnchorPointEnum.esriBottomLeftCorner);
        }
        return retElement;
    }

    private static int convertAnchorTextToEsriInt(String anchorTxt) {
        if (anchorTxt.equals("bottomleft"))
            return esriAnchorPointEnum.esriBottomLeftCorner;
        else if (anchorTxt.equals("bottommid"))
            return esriAnchorPointEnum.esriBottomMidPoint;
        else if (anchorTxt.equals("bottomright"))
            return esriAnchorPointEnum.esriBottomRightCorner;
        else if (anchorTxt.equals("midleft"))
            return esriAnchorPointEnum.esriLeftMidPoint;
        else if (anchorTxt.equals("midmid"))
            return esriAnchorPointEnum.esriCenterPoint;
        else if (anchorTxt.equals("midright"))
            return esriAnchorPointEnum.esriRightMidPoint;
        else if (anchorTxt.equals("topleft"))
            return esriAnchorPointEnum.esriTopLeftCorner;
        else if (anchorTxt.equals("topmid"))
            return esriAnchorPointEnum.esriTopMidPoint;
        else if (anchorTxt.equals("topright"))
            return esriAnchorPointEnum.esriTopRightCorner;
        else
            return esriAnchorPointEnum.esriBottomLeftCorner;
    }

    public static List<IElement> getElementsCollectionFromJson(SOELogger logger, IMapFrame mapFrame, JSONArray elementsArray, IEnvelope layoutEnvelope, PageLayout pageLayout) {
        List<IElement> elementList = new ArrayList<IElement>();
        for (int i = 0; i < elementsArray.length(); i++) {
            try {
                JSONObject elementJsonObj = elementsArray.getJSONObject(i);
                IElement element = parseJson(logger, mapFrame, elementJsonObj, layoutEnvelope, pageLayout);
                if (element != null)
                    elementList.add(element);
            } catch (JSONException e) {
                logger.error("AgsJsonElement.getElementsCollectionFromJson.JSONException: " + e.getMessage());
                e.printStackTrace();
            } catch (AutomationException e) {
                logger.error("AgsJsonElement.getElementsCollectionFromJson.AutomationException: " + e.getMessage());
                e.printStackTrace();
            } catch (UnknownHostException e) {
                logger.error("AgsJsonElement.getElementsCollectionFromJson.UnknownHostException: " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                logger.error("AgsJsonElement.getElementsCollectionFromJson.IOException: " + e.getMessage());
                e.printStackTrace();
            }
        }
        return elementList;
    }
}
