package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.IElement;
import com.esri.arcgis.carto.PageLayout;
import com.esri.arcgis.carto.PictureElement;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import uk.co.jaimon.test.SimpleImageInfo;

import java.io.*;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/1/11
 * Time: 4:22 PM
 */
public class AgsJsonPictureElement extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchPictureElement";
    private String pictureUrl;

    public AgsJsonPictureElement(SOELogger logger, String pictureUrl) {
        super(TYPE, logger);
        this.pictureUrl = pictureUrl;
    }

    public AgsJsonPictureElement(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("pictureUrl")) this.pictureUrl = jsonObj.getString("pictureUrl");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("pictureUrl", pictureUrl);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom, int anchor) throws IOException {
        File pFile = new File(pictureUrl);
        logger.error("AgsJsonPictureElement.toArcObject.pFile.canRead() = " + pFile.canRead());
        String pictureTmpFile = null;
        if (pFile.canRead()) {
            pictureTmpFile = pFile.getAbsolutePath();
        } else {
            try {
                URL url = new URL(pictureUrl);
                System.out.println("url = " + url);
                //Image img = ImageIO.read(pFile);
                File tmpFile = File.createTempFile("pictureElem", pictureUrl.substring(pictureUrl.lastIndexOf(".")));
                System.out.println("tmpFile = " + tmpFile);
                BufferedInputStream in = new BufferedInputStream(url.openStream());
                FileOutputStream fos = new java.io.FileOutputStream(tmpFile);
                BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
                byte[] data = new byte[1024];
                int x = 0;
                while ((x = in.read(data, 0, 1024)) >= 0) {
                    bout.write(data, 0, x);
                }
                bout.close();
                in.close();
                pictureTmpFile = tmpFile.getAbsolutePath();
            } catch (UnknownHostException e) {
                logger.error("AgsJsonPictureElement.toArcObject.UnknownHostException: " + e.getMessage());
                return null;
            } catch (IOException e) {
                logger.error("AgsJsonPictureElement.toArcObject.IOException: " + e.getMessage());
                return null;
            }
        }
        if (pictureTmpFile != null) {
            PictureElement pictureElement = new PictureElement();
            pictureElement.importPictureFromFile(pictureTmpFile);
            IEnvelope envelope = null;
            if (geom instanceof IPoint) {
                IPoint pt1 = (IPoint) geom;
                SimpleImageInfo simpleImageInfo = new SimpleImageInfo(new File(pictureTmpFile));
                int height = simpleImageInfo.getHeight();
                int width = simpleImageInfo.getWidth();
                System.out.println("height = " + height);
                System.out.println("width = " + width);

                double heightCm = (height / 50) * 2.54;
                double widthCm = (width / 50) * 2.54;
                System.out.println("heightCm = " + heightCm);
                System.out.println("widthCm = " + widthCm);

                envelope = new Envelope();
                envelope.putCoords(pt1.getX(), pt1.getY(), pt1.getX() + widthCm, pt1.getY() + heightCm);
                pictureElement.setGeometry(envelope);
            } else if (geom instanceof IEnvelope) {
                envelope = (IEnvelope) geom;
                pictureElement.setGeometry(envelope);
            }
            LayoutUtils.positionElement(pictureElement, envelope, anchor);
            return pictureElement;
        }
        logger.error("AgsJsonPictureElement.pictureTmpFile is null!");
        return null;
    }
}