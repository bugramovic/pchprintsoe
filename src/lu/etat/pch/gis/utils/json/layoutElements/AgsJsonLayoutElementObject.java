package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.IElement;
import com.esri.arcgis.carto.PageLayout;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/2/11
 * Time: 10:25 AM
 */
public abstract class AgsJsonLayoutElementObject {
    protected String type;
    protected SOELogger logger;

    protected AgsJsonLayoutElementObject(String type, SOELogger logger) {
        this.type = type;
        this.logger = logger;
    }

    public abstract JSONObject toJSON() throws JSONException;
    public abstract IElement toArcObject(PageLayout pageLayout,IGeometry geom,int anchor) throws IOException;
}
