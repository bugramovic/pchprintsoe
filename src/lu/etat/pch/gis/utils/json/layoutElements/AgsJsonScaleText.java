package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.display.LineCallout;
import com.esri.arcgis.display.SimpleFillSymbol;
import com.esri.arcgis.display.TextSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.*;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.AgsJsonRGBColor;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 7, 2010
 * Time: 6:12:49 PM
 */
public class AgsJsonScaleText extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchScaleText";
    private int style = esriScaleTextStyleEnum.esriScaleTextAbsolute; //esriScaleTextStyleEnum
    private int roundingOption = -1;     //esriRoundingOptionEnum
    private int roundingValue = -1;
    private String mapUnitLabel;
    private String pageUnitLabel;
    private int mapUnits = esriUnits.esriMeters;
    private int pageUnits = esriUnits.esriCentimeters;
    private String separator;
    private AgsJsonRGBColor backgroundColor;
    private AgsJsonTextSymbol textSymbol;

    public AgsJsonScaleText(SOELogger logger,int roundingOption, int roundingValue) {
        super(TYPE,logger);
        this.mapUnits = 0;
        this.pageUnits = 0;
        this.roundingOption = roundingOption;
        this.roundingValue = roundingValue;
        this.separator = null;
    }

    public AgsJsonScaleText(SOELogger logger,int mapUnits, int pageUnits, String mapUnitLabel, String pageUnitLabel, int roundingOption, int roundingValue) {
        super(TYPE,logger);
        this.style = esriScaleTextStyleEnum.esriScaleTextRelative;
        this.mapUnits = mapUnits;
        this.pageUnits = pageUnits;
        this.mapUnitLabel = mapUnitLabel;
        this.pageUnitLabel = pageUnitLabel;
        this.roundingOption = roundingOption;
        this.roundingValue = roundingValue;
    }

    public AgsJsonScaleText(SOELogger logger,JSONObject jsonObject) {
        super(TYPE,logger);
        try {
            type = jsonObject.getString("type");
            if (!jsonObject.isNull("style")) style = jsonObject.getInt("style");
            if (!jsonObject.isNull("mapUnits")) mapUnits = jsonObject.getInt("mapUnits");
            if (!jsonObject.isNull("mapUnitLabel")) mapUnitLabel = jsonObject.getString("mapUnitLabel");
            if (!jsonObject.isNull("pageUnits")) pageUnits = jsonObject.getInt("pageUnits");
            if (!jsonObject.isNull("pageUnitLabel")) pageUnitLabel = jsonObject.getString("pageUnitLabel");
            if (!jsonObject.isNull("numberFormat") && !jsonObject.isNull("numberFormat")) {
                JSONObject numberFormatObj = jsonObject.getJSONObject("numberFormat");
                if (!jsonObject.isNull("roundingOption")) roundingOption = numberFormatObj.getInt("roundingOption");
                if (!jsonObject.isNull("roundingValue")) roundingValue = numberFormatObj.getInt("roundingValue");
            }
            if (!jsonObject.isNull("separator")) separator = jsonObject.getString("separator");
            if (!jsonObject.isNull("backgroundColor")) {
                JSONObject bkGroundObj = jsonObject.getJSONObject("backgroundColor");
                backgroundColor = new AgsJsonRGBColor(bkGroundObj);
            }
            if (!jsonObject.isNull("textSymbol"))
                this.textSymbol = new AgsJsonTextSymbol(jsonObject.getJSONObject("textSymbol"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("style", style);
        symbObject.put("mapUnits", mapUnits);
        if (mapUnitLabel != null) symbObject.put("mapUnitLabel", mapUnitLabel);
        symbObject.put("pageUnits", pageUnits);
        if (pageUnitLabel != null) symbObject.put("pageUnitLabel", pageUnitLabel);
        JSONObject numberFormatObj = new JSONObject();
        numberFormatObj.put("roundingOption", roundingOption);
        numberFormatObj.put("roundingValue", roundingValue);
        symbObject.put("numberFormat", numberFormatObj);
        symbObject.put("separator", separator);
        symbObject.put("textSymbol", textSymbol.toJSON());
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom,int anchor) throws IOException {
        IUID scaleTextUID = new UID();
        scaleTextUID.setValue(ScaleText.getClsid());

        IFrameElement mapFrameElement = pageLayout.findFrame(pageLayout.getFocusMap());
        IMapFrame mapFrame = (IMapFrame) mapFrameElement;

        MapSurroundFrame scaleTextFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(scaleTextUID, null);
        ScaleText scaleText = (ScaleText) scaleTextFrame.getMapSurround();
        scaleText.setStyle(style);
        if (roundingOption > -1 && roundingValue > -1) {
            INumberFormat numberFormat = scaleText.getNumberFormat();
            if (numberFormat instanceof NumericFormat) {
                NumericFormat numericFormat = (NumericFormat) numberFormat;
                numericFormat.setRoundingOption(roundingOption);
                numericFormat.setRoundingValue(roundingValue);
                scaleText.setNumberFormat(numericFormat);
            }
        }
        if(textSymbol!=null) {
            scaleText.setSymbol((ITextSymbol) textSymbol.toArcObject());
        }
        if (backgroundColor != null) {
            if (scaleText.getSymbol() instanceof TextSymbol) {
                TextSymbol textSymbol = (TextSymbol) scaleText.getSymbol();
                LineCallout lineCallout = new LineCallout();
                SimpleFillSymbol simpleFillSymbol = new SimpleFillSymbol();
                simpleFillSymbol.setColor(backgroundColor.toArcObject());
                lineCallout.setBorderByRef(simpleFillSymbol);
                lineCallout.setLeaderLineByRef(null);
                lineCallout.setAccentBarByRef(null);
                textSymbol.setBackgroundByRef(lineCallout);

                scaleText.setSymbol(textSymbol);
            } else {
                System.err.println("scaleText.getSymbol() is NOT a TextSymbol !!");
            }
        }
        scaleText.setMapUnits(mapUnits);
        scaleText.setPageUnits(pageUnits);
        if (mapUnitLabel != null) scaleText.setMapUnitLabel(mapUnitLabel);
        if (pageUnitLabel != null) scaleText.setPageUnitLabel(pageUnitLabel);
        if (separator != null)
            scaleText.setSeparator(separator);

        if (geom instanceof IPoint) {
            scaleTextFrame.setGeometry(geom);
            scaleTextFrame.setAnchorPoint(anchor);
            LayoutUtils.positionElement(scaleTextFrame, (IPoint) geom, anchor, pageLayout.getScreenDisplay());
        } else
            scaleTextFrame.setGeometry(geom);
        return scaleTextFrame;
    }
}
