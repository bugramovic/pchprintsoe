package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 2/22/11
 * Time: 6:25 AM
 */
public class AgsJsonNorthArrow extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchNorthArrow";

    private double size = -1;

    public AgsJsonNorthArrow(SOELogger logger) {
        super(TYPE,logger);
    }

    public AgsJsonNorthArrow(SOELogger logger,JSONObject jsonObj) {
        super(TYPE,logger);
        try {
            if (!jsonObj.isNull("size")) this.size = jsonObj.getDouble("size");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public double getSize() {
        return size;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("size", size);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom, int anchor) throws IOException {
        IUID northArrowUID = new UID();
        northArrowUID.setValue(MarkerNorthArrow.getClsid());
        IFrameElement mapFrameElement = pageLayout.findFrame(pageLayout.getFocusMap());
        IMapFrame mapFrame = (IMapFrame) mapFrameElement;

        MapSurroundFrame northArrowFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(northArrowUID, null);
        MarkerNorthArrow northArrow = (MarkerNorthArrow) northArrowFrame.getMapSurround();
        if (size > -1) northArrow.setSize(size);
//        northArrow.setMarkerSymbol(agsJsonNorthArrow.getMarkerSymbol());
        //northArrow.set

        if (geom instanceof IPoint) {
            northArrowFrame.setGeometry(geom);
            northArrowFrame.setAnchorPoint(anchor);
            LayoutUtils.positionElement(northArrowFrame, (IPoint) geom, anchor, pageLayout.getScreenDisplay());
        } else
            northArrowFrame.setGeometry(geom);

        return northArrowFrame;
    }

}
