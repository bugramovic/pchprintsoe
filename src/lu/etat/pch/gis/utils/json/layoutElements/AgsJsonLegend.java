package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.IFillSymbol;
import com.esri.arcgis.geometry.IEnvelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonSimpleFillSymbol;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 28, 2010
 * Time: 8:36:58 AM
 */
public class AgsJsonLegend extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchLegend";
    private String legendTitle = "Dynamic legend:";
    private int legendColumns = 1;
    private boolean includeAllLayers = true;
    private String[] exceptionLayerNames = new String[0];
    private boolean showLayerNames = false;
    private boolean showLabels = true;
    private boolean showHeadings = false;
    private AgsJsonSimpleFillSymbol legendBackground;

    public AgsJsonLegend(SOELogger logger) {
        super(TYPE, logger);
    }

    public AgsJsonLegend(SOELogger logger, String legendTitle, int legendColumns, boolean includeAllLayers, String[] exceptionLayerNames, boolean showLayerNames, boolean showLabels, boolean showHeadings) {
        super(TYPE, logger);
        this.legendTitle = legendTitle;
        this.legendColumns = legendColumns;
        this.includeAllLayers = includeAllLayers;
        this.exceptionLayerNames = exceptionLayerNames;
        this.showLayerNames = showLayerNames;
        this.showLabels = showLabels;
        this.showHeadings = showHeadings;
    }

    public AgsJsonLegend(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("legendTitle")) legendTitle = jsonObj.getString("legendTitle");
            if (!jsonObj.isNull("legendColumns")) legendColumns = jsonObj.getInt("legendColumns");
            if (!jsonObj.isNull("includeAllLayers")) includeAllLayers = jsonObj.getBoolean("includeAllLayers");
            if (!jsonObj.isNull("exceptionLayerNames")) {
                JSONArray tmpArray = jsonObj.getJSONArray("exceptionLayerNames");
                exceptionLayerNames = new String[tmpArray.length()];
                for (int i = 0; i < exceptionLayerNames.length; i++) {
                    exceptionLayerNames[i] = tmpArray.getString(i);
                }
            }
            if (!jsonObj.isNull("showLayerNames")) showLayerNames = jsonObj.getBoolean("showLayerNames");
            if (!jsonObj.isNull("showLabels")) showLabels = jsonObj.getBoolean("showLabels");
            if (!jsonObj.isNull("showHeadings")) showHeadings = jsonObj.getBoolean("showHeadings");
            if (!jsonObj.isNull("legendBackground") && !jsonObj.isNull("legendBackground"))
                legendBackground = new AgsJsonSimpleFillSymbol(jsonObj.getJSONObject("legendBackground"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getLegendTitle() {
        return legendTitle;
    }

    public void setLegendTitle(String legendTitle) {
        this.legendTitle = legendTitle;
    }

    public boolean isIncludeAllLayers() {
        return includeAllLayers;
    }

    public void setIncludeAllLayers(boolean includeAllLayers) {
        this.includeAllLayers = includeAllLayers;
    }

    public String[] getExceptionLayerNames() {
        return exceptionLayerNames;
    }

    public void setExceptionLayerNames(String[] exceptionLayerNames) {
        this.exceptionLayerNames = exceptionLayerNames;
    }

    public boolean isShowLayerNames() {
        return showLayerNames;
    }

    public void setShowLayerNames(boolean showLayerNames) {
        this.showLayerNames = showLayerNames;
    }

    public boolean isShowLabels() {
        return showLabels;
    }

    public void setShowLabels(boolean showLabels) {
        this.showLabels = showLabels;
    }

    public boolean isShowHeadings() {
        return showHeadings;
    }

    public void setShowHeadings(boolean showHeadings) {
        this.showHeadings = showHeadings;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("legendTitle", legendTitle);
        symbObject.put("legendColumns", legendColumns);
        symbObject.put("includeAllLayers", includeAllLayers);
        JSONArray execpionLayerNamesJsonArray = new JSONArray();
        for (String exceptionLayerName : exceptionLayerNames) {
            execpionLayerNamesJsonArray.put(exceptionLayerName);
        }
        symbObject.put("exceptionLayerNames", execpionLayerNamesJsonArray);
        symbObject.put("showLayerNames", showLayerNames);
        symbObject.put("showLabels", showLabels);
        symbObject.put("showHeadings", showHeadings);
        if (legendBackground != null) {
            symbObject.put("legendBackground", legendBackground.toJSON());
        }
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom, int anchor) throws IOException {
        List<String> exceptionLayerNamesList = new ArrayList<String>(Arrays.asList(exceptionLayerNames));

        //Create Legend
        IUID uidLegend = new UID();
        uidLegend.setValue(Legend.getClsid());

        IFrameElement mapFrameElement = pageLayout.findFrame(pageLayout.getFocusMap());
        IMapFrame mapFrame = (IMapFrame) mapFrameElement;
        IMap map = mapFrame.getMap();

        MapSurroundFrame legendMapSurroundFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(uidLegend, null);
        Legend legend = (Legend) legendMapSurroundFrame.getMapSurround();

        legend.setTitle(legendTitle);
        legend.setMapByRef(map);

        legend.clearItems();

        List<ComparableLayer> addedLayersOnLegend = new ArrayList<ComparableLayer>();
        for (int i = 0; i < map.getLayerCount(); i++) {
            ILayer layer = map.getLayer(i);
            addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        }
        legend.adjustColumns(legendColumns);
        if (legendBackground != null) {
            SymbolBackground symbolBackground = new SymbolBackground();
            //symbolBackground.setFillSymbol((IFillSymbol) new AgsJsonSimpleFillSymbol("esriSFSSolid", new AgsJsonRGBColor(255, 255, 255)).toArcObject());
            symbolBackground.setFillSymbol((IFillSymbol) legendBackground.toArcObject());
            legendMapSurroundFrame.setBackground(symbolBackground);
        }
        if (geom instanceof IPoint) {
            legendMapSurroundFrame.setGeometry(geom);
            legendMapSurroundFrame.setAnchorPoint(anchor);
            LayoutUtils.positionElement(legendMapSurroundFrame, (IPoint) geom, anchor, pageLayout.getScreenDisplay());
        } else {
            if (geom instanceof IEnvelope) {
                legendMapSurroundFrame.setGeometry(geom);
                LayoutUtils.positionElement(legendMapSurroundFrame, (IEnvelope) geom, anchor);
            } else {
                legendMapSurroundFrame.setGeometry(geom);
            }
        }
        return legendMapSurroundFrame;
    }

    private void addLayerToLegend(ILayer layer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        if (layer instanceof GroupLayer) {
            GroupLayer groupLayer = (GroupLayer) layer;
            addLayerToLegend(groupLayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        } else if (layer instanceof MapServerLayer) {
            MapServerLayer mapServerLayer = (MapServerLayer) layer;
            addLayerToLegend(mapServerLayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        } else if (layer instanceof MapServerIdentifySublayer) {
            MapServerIdentifySublayer mapServerIdentifySublayer = (MapServerIdentifySublayer) layer;
            addLayerToLegend(mapServerIdentifySublayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        } else if (layer instanceof BasemapLayer) {
            BasemapLayer basemapLayer = (BasemapLayer) layer;
            addLayerToLegend(basemapLayer, legend, addedLayersOnLegend, exceptionLayerNamesList);
        } else {
            try {
                String layerName = layer.getName();
                boolean shouldBeAdded = includeAllLayers;
                if (exceptionLayerNamesList.contains(layerName)) shouldBeAdded = !shouldBeAdded;
                if (layerName.contains("Bing Maps ")) shouldBeAdded = false;
                if (layer instanceof WMSMapLayer) shouldBeAdded = false;
                if (shouldBeAdded) {
                    ComparableLayer compLayer = new ComparableLayer(layer);
                    if (!addedLayersOnLegend.contains(compLayer)) {
                        try {
                            HorizontalLegendItem horiLegendItem = new HorizontalLegendItem();
                            horiLegendItem.setLayerByRef(layer);
                            horiLegendItem.setShowLayerName(showLayerNames);
                            horiLegendItem.setShowLabels(showLabels);
                            horiLegendItem.setShowHeading(showHeadings);

                            addedLayersOnLegend.add(compLayer);
                            legend.addItem(horiLegendItem);
                            //System.out.println("legend.added.layerName = " + layerName);
                        } catch (AutomationException e) {
                            e.printStackTrace();
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //System.out.println("legend.NOT.added.alreadyIn.layerName = " + layer.getName());
                    }
                } else {
                    //System.out.println("legend.execptionLayerFound.layerName = " + layerName);
                }
            } catch (AutomationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addLayerToLegend(GroupLayer groupLayer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            for (int i = 0; i < groupLayer.getCount(); i++) {
                ILayer layer = groupLayer.getLayer(i);
                addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addLayerToLegend(MapServerLayer mapServerLayer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            for (int i = 0; i < mapServerLayer.getCount(); i++) {
                ILayer layer = mapServerLayer.getLayer(i);
                addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addLayerToLegend(MapServerIdentifySublayer mapServerIdentifySublayer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            for (int i = 0; i < mapServerIdentifySublayer.getCount(); i++) {
                ILayer layer = mapServerIdentifySublayer.getLayer(i);
                addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addLayerToLegend(BasemapLayer basemapLayer, Legend legend, List<ComparableLayer> addedLayersOnLegend, List<String> exceptionLayerNamesList) {
        try {
            for (int i = 0; i < basemapLayer.getCount(); i++) {
                ILayer layer = basemapLayer.getLayer(i);
                addLayerToLegend(layer, legend, addedLayersOnLegend, exceptionLayerNamesList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ComparableLayer {
        private ILayer layer;

        private ComparableLayer(ILayer layer) {
            this.layer = layer;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ComparableLayer) {
                ComparableLayer compLayer2 = (ComparableLayer) obj;
                obj = compLayer2.layer;
            }
            if (obj instanceof ILayer) {
                ILayer layer2 = (ILayer) obj;
                try {
                    if (!layer.getClass().getCanonicalName().equals(layer2.getClass().getCanonicalName()))
                        return false;
                    if (!layer.getName().equals(layer2.getName()))
                        return false;
                    return true;
                } catch (AutomationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }
    }
}