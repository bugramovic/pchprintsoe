package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import com.esri.arcgis.system.IUID;
import com.esri.arcgis.system.UID;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;

import java.io.IOException;
import lu.etat.pch.gis.utils.UnitConverter;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Jul 8, 2010
 * Time: 5:38:35 AM
 */
public class AgsJsonScaleBar extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchScaleBar";
    private double barHeight = -1;
    private String unit = null;

    public AgsJsonScaleBar(SOELogger logger) {
        super(TYPE,logger);
    }

    public AgsJsonScaleBar(SOELogger logger,double barHeight) {
        super(TYPE,logger);
        this.barHeight = barHeight;
    }

    public AgsJsonScaleBar(SOELogger logger,JSONObject jsonObject) {
        super(TYPE,logger);
        try {
            if (!jsonObject.isNull("barHeight")) this.barHeight = jsonObject.getDouble("barHeight");
            if (!jsonObject.isNull( "unit")) this.unit = jsonObject.getString( "unit");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("barHeight", barHeight);
        symbObject.put("unit", unit);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom,int anchor) throws IOException {
        IUID alternatingScaleBar = new UID();
        alternatingScaleBar.setValue(AlternatingScaleBar.getClsid());

        IFrameElement mapFrameElement = pageLayout.findFrame(pageLayout.getFocusMap());
        IMapFrame mapFrame = (IMapFrame) mapFrameElement;

        MapSurroundFrame scaleBarFrame = (MapSurroundFrame) mapFrame.createSurroundFrame(alternatingScaleBar, null);
        AlternatingScaleBar scaleBar = (AlternatingScaleBar) scaleBarFrame.getMapSurround();
        scaleBar.useMapSettings();
        if (barHeight > -1) {
            scaleBar.setBarHeight(barHeight);
            ITextSymbol labelTextSymbol = scaleBar.getLabelSymbol();
            labelTextSymbol.setSize(barHeight * 2);
            scaleBar.setUnitLabelSymbol(labelTextSymbol);
            scaleBar.setLabelSymbol(labelTextSymbol);
        }
        if (this.unit != null) {
            scaleBar.setUnits( UnitConverter.getEsriUnitFromString( this.unit ) );
        }
        if (geom instanceof IPoint) {
            scaleBarFrame.setGeometry(geom);
            scaleBarFrame.setAnchorPoint(anchor);
            LayoutUtils.positionElement(scaleBarFrame, (IPoint) geom, anchor, pageLayout.getScreenDisplay());
        } else
            scaleBarFrame.setGeometry(geom);

        return scaleBarFrame;
    }
}