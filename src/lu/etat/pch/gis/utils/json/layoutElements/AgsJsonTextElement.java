package lu.etat.pch.gis.utils.json.layoutElements;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.display.ITextSymbol;
import com.esri.arcgis.geometry.Envelope;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.geometry.Polygon;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.LayoutUtils;
import lu.etat.pch.gis.utils.SOELogger;
import lu.etat.pch.gis.utils.json.graphics.AgsJsonTextSymbol;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 4/1/11
 * Time: 4:22 PM
 */
public class AgsJsonTextElement extends AgsJsonLayoutElementObject {
    public static final String TYPE = "pchTextElement";
    private String text;
    private AgsJsonTextSymbol textSymbol;

    public AgsJsonTextElement(SOELogger logger, String text) {
        super(TYPE, logger);
        this.text = text;
    }

    public AgsJsonTextElement(SOELogger logger, JSONObject jsonObj) {
        super(TYPE, logger);
        try {
            if (!jsonObj.isNull("text")) this.text = jsonObj.getString("text");
            if (!jsonObj.isNull("type") && jsonObj.getString("type").equalsIgnoreCase("esriTS"))
                this.textSymbol = new AgsJsonTextSymbol(jsonObj);
            else if (!jsonObj.isNull("textSymbol"))
                this.textSymbol = new AgsJsonTextSymbol(jsonObj.getJSONObject("textSymbol"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AgsJsonTextSymbol getTextSymbol() {
        return textSymbol;
    }

    public void setTextSymbol(AgsJsonTextSymbol textSymbol) {
        this.textSymbol = textSymbol;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject symbObject = new JSONObject();
        symbObject.put("type", type);
        symbObject.put("text", text);
        if (textSymbol != null) symbObject.put("textSymbol", textSymbol);
        return symbObject;
    }

    @Override
    public IElement toArcObject(PageLayout pageLayout, IGeometry geom, int anchor) throws IOException {
        if (geom instanceof Envelope || geom instanceof Polygon) {
            ParagraphTextElement paragraphTextElement = new ParagraphTextElement();
            paragraphTextElement.setText(text);
            if (textSymbol != null) {
                paragraphTextElement.setSymbol((ITextSymbol) textSymbol.toArcObject());
            }
            paragraphTextElement.setGeometry(geom);
            if (pageLayout == null) {
                anchor = esriAnchorPointEnum.esriCenterPoint;
            }
            LayoutUtils.positionTextElement(paragraphTextElement, geom, anchor);
            return paragraphTextElement;
        }
        TextElement textElement = new TextElement();
        textElement.setText(text);
        if (textSymbol != null) {
            textElement.setSymbol((ITextSymbol) textSymbol.toArcObject());
        }
        textElement.setGeometry(geom);
        if (pageLayout == null) {
            anchor = esriAnchorPointEnum.esriCenterPoint;
        }
        LayoutUtils.positionTextElement(textElement, geom, anchor);
        return textElement;
    }
}
