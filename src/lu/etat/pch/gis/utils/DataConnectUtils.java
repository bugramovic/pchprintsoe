package lu.etat.pch.gis.utils;

import com.esri.arcgis.carto.*;
import com.esri.arcgis.datasourcesGDB.AccessWorkspaceFactory;
import com.esri.arcgis.datasourcesGDB.FileGDBWorkspaceFactory;
import com.esri.arcgis.geodatabase.FeatureClass;
import com.esri.arcgis.geodatabase.Workspace;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.gisclient.*;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 21, 2010
 * Time: 6:17:21 PM
 */
public class DataConnectUtils {
    public static ILayer createFileGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        FileGDBWorkspaceFactory fGDB = new FileGDBWorkspaceFactory();
        Workspace workspace = new Workspace(fGDB.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public static ILayer createAccessGeoDataBaseLayer(String geoDB, String featureClassName) throws IOException {
        AccessWorkspaceFactory awFact = new AccessWorkspaceFactory();
        Workspace workspace = new Workspace(awFact.openFromFile(geoDB, 0));
        FeatureClass fClass = new FeatureClass(workspace.openFeatureClass(featureClassName));
        FeatureLayer fLayer = new FeatureLayer();
        fLayer.setName(featureClassName);
        fLayer.setFeatureClassByRef(fClass);
        return fLayer;
    }

    public static void printOutPropSet(IPropertySet propSet) {
        Object[] a = new Object[1];
        Object[] b = new Object[1];
        try {
            propSet.getAllProperties(a, b);
            Object[] aa = (Object[]) a[0];
            Object[] bb = (Object[]) b[0];
            for (int i = 0; i < bb.length; i++) {
                System.out.println(aa[i]+" = " + bb[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static ILayer createArcGISService(String url, String mapService) throws IOException {
        //Set up connection
        IAGSServerConnectionFactory connectionFactory = new AGSServerConnectionFactory();
        IPropertySet propertySet = new PropertySet();
        IAGSServerConnection connection;
        propertySet.setProperty("url", url);
        connection = connectionFactory.open(propertySet, 0);


        IAGSEnumServerObjectName serverObjectNames = connection.getServerObjectNames();
        serverObjectNames.reset();

        IAGSServerObjectName serverObjectName;
        while ((serverObjectName = serverObjectNames.next()) != null) {
            if (serverObjectName.getName().equals(mapService) && serverObjectName.getType().equals("MapServer")) {
                if (serverObjectName instanceof AGSServerObjectName) {
                    AGSServerObjectName name = (AGSServerObjectName) serverObjectName;
                    IPropertySet propertySet2 = name.getAGSServerConnectionName().getConnectionProperties();
                    Object[] a = new Object[propertySet.getCount()], b = new Object[propertySet.getCount()];
                    propertySet2.getAllProperties(a, b);
                    IMapServer mapServer = (IMapServer) name.open();
                    MapServerLayer layer = new MapServerLayer();
                    layer.serverConnect(serverObjectName, mapServer.getDefaultMapName());
                    return layer;
                }
            }
        }
        return null;
    }
    //todo: make a simpler AGS connection 

    public static void testAGS() throws IOException {
        MapServerLayer mapServerLayer = new MapServerLayer();
        AGSServerConnectionName connName = new AGSServerConnectionName();
        PropertySet propSet = new PropertySet();
        propSet.setProperty("url", "http://localhost:8399/arcgis/services");
        propSet.setProperty("HTTPTIMEOUT", 60);
        propSet.setProperty("MESSAGEFORMAT", 2);
        propSet.setProperty("TOKENSERVICEURL", "http://babifer:8399/arcgis/tokens");
        connName.setConnectionProperties(propSet);
        connName.open();
        mapServerLayer.connect(connName);
    }

    public static WMSMapLayer createWMSLayer(String wmsURL) throws IOException {
        //wmsUrl =  "http://ws.geoportail.lu/natura2000?"
        WMSMapLayer wmsMapLayer = new WMSMapLayer();
        WMSConnectionName connName = new WMSConnectionName();
        IPropertySet propSet = new PropertySet();
        propSet.setProperty("URL", wmsURL);
        propSet.setProperty("version", "1.0.0");
        connName.setConnectionProperties(propSet);
        wmsMapLayer.connect(connName);
        for (int i = 0; i < wmsMapLayer.getCount(); i++) {
            ILayer layer = wmsMapLayer.getLayer(i);
            SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
            IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
            ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
            layer.setSpatialReferenceByRef(spatialReference);
            layer.setVisible(true);
            if (layer instanceof WMSGroupLayer) {
                WMSGroupLayer wmsGroupLayer = (WMSGroupLayer) layer;
                for (int j = 0; j < wmsGroupLayer.getCount(); j++) {
                    ILayer childLayer = wmsGroupLayer.getLayer(j);
                    childLayer.setVisible(true);
                }
            }
        }
        return wmsMapLayer;
    }


    public static void main(String[] args) {
        LicenceUtils.initializeArcGISLicenses();
        try {
            FeatureClass fc = FeatureClassUtils.createFeatureClassFromArcSDEBasedDataSource("localhost", "5151", "sde", "sde", "sde", "DEFAULT", "dem5m2003");
            System.out.println("fc.getShapeFieldName() = " + fc.getShapeFieldName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ISpatialReference getSpatialReferenceLuxembourg() throws IOException {
        SpatialReferenceEnvironment spatialReferenceEnvironment = new SpatialReferenceEnvironment();
        IProjectedCoordinateSystem geographicCoordinateSystem = spatialReferenceEnvironment.createProjectedCoordinateSystem(esriSRProjCS4Type.esriSRProjCS_Luxembourg1930_Gauss);
        ISpatialReference spatialReference = new ISpatialReferenceProxy(geographicCoordinateSystem);
        return spatialReference;
    }
}
