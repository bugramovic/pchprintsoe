package lu.etat.pch.gis.utils;

import com.esri.arcgis.datasourcesGDB.AccessWorkspaceFactory;
import com.esri.arcgis.datasourcesGDB.FileGDBWorkspaceFactory;
import com.esri.arcgis.datasourcesGDB.SdeWorkspaceFactory;
import com.esri.arcgis.geodatabase.FeatureClass;
import com.esri.arcgis.geodatabase.IWorkspaceFactory;
import com.esri.arcgis.geodatabase.Workspace;
import com.esri.arcgis.geodatabase.WorkspaceFactory;
import com.esri.arcgis.system.IPropertySet;
import com.esri.arcgis.system.PropertySet;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 19, 2010
 * Time: 9:44:03 PM
 */
public class FeatureClassUtils {
    public static Workspace open_File_Workspace(String fileDS) throws IOException {
        WorkspaceFactory workspaceFactory = null;
        if (fileDS.endsWith(".mdb")) {
            workspaceFactory = new WorkspaceFactory(new AccessWorkspaceFactory());
        } else if (fileDS.endsWith(".gdb")) {
            workspaceFactory = new WorkspaceFactory(new FileGDBWorkspaceFactory());
        } else if (fileDS.endsWith(".sde")) {
            workspaceFactory = new WorkspaceFactory(new SdeWorkspaceFactory());
        }
        if (workspaceFactory != null) {
            Workspace workspace = new Workspace(workspaceFactory.openFromFile(fileDS, 0));
            return workspace;
        }
        return null;
    }

    public static Workspace open_ArcSDE_Workspace(String server, String instance, String user,
                                                  String password, String database, String version) throws IOException {
        IPropertySet propertySet = new PropertySet();
        propertySet.setProperty("SERVER", server);
        propertySet.setProperty("INSTANCE", instance);
        propertySet.setProperty("DATABASE", database);
        propertySet.setProperty("USER", user);
        propertySet.setProperty("PASSWORD", password);
        propertySet.setProperty("VERSION", version);

        IWorkspaceFactory workspaceFactory = new SdeWorkspaceFactory();
        Workspace workspace = (Workspace) workspaceFactory.open(propertySet, 0);
        return workspace;
    }


    public static FeatureClass createFeatureClassFromFileBasedDataSource(String fileDS, String fcName) throws IOException {
        Workspace workspace = FeatureClassUtils.open_File_Workspace(fileDS);
        if (workspace != null) {
            FeatureClass featureClass = new FeatureClass(workspace.openFeatureClass(fcName));
            return featureClass;
        }
        return null;
    }

    public static FeatureClass createFeatureClassFromArcSDEBasedDataSource(String server, String instance, String user,
                                                                           String password, String database, String version, String fcName) throws IOException {
        Workspace workspace = FeatureClassUtils.open_ArcSDE_Workspace(server, instance, user, password, database, version);
        if (workspace != null) {
            FeatureClass featureClass = new FeatureClass(workspace.openFeatureClass(fcName));
            return featureClass;
        }
        return null;
   }
}