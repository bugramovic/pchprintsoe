package lu.etat.pch.gis.beans.height;

import com.esri.arcgis.geometry.IPoint;
import com.esri.arcgis.interop.AutomationException;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: 17/10/11
 * Time: 09:53
 */
public class HeightResultBean {
    private double x;
    private double y;
    private double z;

    public HeightResultBean(double z) {
        this.z = z;
    }

    public HeightResultBean(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public HeightResultBean(IPoint pt) {
        try {
            this.x = pt.getX();
            this.y = pt.getY();
            this.z = pt.getZ();
        } catch (AutomationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
