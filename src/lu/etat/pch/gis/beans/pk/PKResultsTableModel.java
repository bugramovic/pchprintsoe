package lu.etat.pch.gis.beans.pk;

import javax.swing.table.DefaultTableModel;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 20, 2010
 * Time: 9:36:23 PM
 */
public class PKResultsTableModel extends DefaultTableModel {
    String[] tableColumnNames = new String[]{"Route", "PK", "Offset", "X", "Y"};

    public PKResultsTableModel(List<PKResultBean> pkResults) {
        setColumnIdentifiers(tableColumnNames);
        for (PKResultBean pkResultBean : pkResults) {
            Object[] objects = new Object[5];
            objects[0] = pkResultBean.getRoute();
            objects[1] = pkResultBean.getPk();
            objects[2] = pkResultBean.getOffset();
            objects[3] = pkResultBean.getX();
            objects[4] = pkResultBean.getY();
            addRow(objects);
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
