package lu.etat.pch.gis.beans.htmlPopup;

import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.server.json.JSONArray;
import com.esri.arcgis.server.json.JSONException;
import com.esri.arcgis.server.json.JSONObject;
import lu.etat.pch.gis.utils.json.geometry.AgsJsonGeometry;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: May 13, 2010
 * Time: 10:22:20 AM
 */
public class HtmlPopupQueryFeature {
    private Map<String, Object> attributes;
    private IGeometry geometry;
    private String htmlPopup;
    private String htmlPopupUrl;

    public HtmlPopupQueryFeature(Map<String, Object> attributes, IGeometry geometry,String htmlPopup, String htmlPopupUrl) {
        this.attributes = attributes;
        this.geometry = geometry;
        this.htmlPopup = htmlPopup;
        this.htmlPopupUrl=htmlPopupUrl;
    }


    public JSONObject toJSON() throws IOException, JSONException {
        JSONObject jsonObj = new JSONObject();
        JSONArray attriArray = new JSONArray();
        jsonObj.put("attributes", attriArray);
        Set<Map.Entry<String, Object>> attriSet = attributes.entrySet();
        for (Map.Entry<String, Object> attriEntry : attriSet) {
            JSONObject attriObj = new JSONObject();
            attriObj.put(attriEntry.getKey(), attriEntry.getValue());
            attriArray.put(attriObj);
        }
        if (geometry != null) {
            jsonObj.put("geometry", AgsJsonGeometry.convertGeomToJson(geometry));
        }
        jsonObj.put("htmlPopup", htmlPopup);
        jsonObj.put("htmlPopupUrl",htmlPopupUrl);
        return jsonObj;
    }

    public IGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(IGeometry geometry) {
        this.geometry = geometry;
    }
    /*
   {
   "attributes" : {
       "ST" : "CA",
       "POP2000" : 3694820,
       "AREANAME" : "Los Angeles"
   },
   "geometry" : { "x" : -118.37, "y" : 34.086 }
   },

    */
}
