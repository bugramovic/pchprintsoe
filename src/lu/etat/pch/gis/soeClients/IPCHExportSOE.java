package lu.etat.pch.gis.soeClients;

import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.server.json.JSONException;
import lu.etat.pch.gis.beans.height.HeightResultBean;
import lu.etat.pch.gis.beans.pk.PKResultBean;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Feb 23, 2010
 * Time: 9:28:41 PM
 */
@ArcGISExtension
public interface IPCHExportSOE {
    public String getMetadataByID(int layerId) throws IOException;
    public PKResultBean[] getPKsByCoord(double coordX, double coordY, String wkid,double tolerance) throws IOException;
    public double getHeightForPoint(double coordX, double coordY) throws IOException;
    public HeightResultBean[] getHeightForLine(String lineGeom) throws IOException, JSONException;

    public String exportLayers();
    public int getLayerCount() throws IOException;
}
