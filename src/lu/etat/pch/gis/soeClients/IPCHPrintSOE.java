package lu.etat.pch.gis.soeClients;

import com.esri.arcgis.interop.extn.ArcGISExtension;
import com.esri.arcgis.server.json.JSONObject;

/**
 * Created by IntelliJ IDEA.
 * User: schullto
 * Date: Mar 1, 2010
 * Time: 7:14:16 AM
 */
@ArcGISExtension
public interface IPCHPrintSOE {
    public String printMap(JSONObject json);

    public String printLayout(JSONObject json);

    public String printMap(String jsonString);

    public String printLayout(String jsonString);
}
