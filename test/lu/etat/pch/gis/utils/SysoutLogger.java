/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.ILog;
import java.io.IOException;

/**
 * ILog implementation, that can be used in local tests
 * 
 * @author abajramovic
 */
public class SysoutLogger implements ILog {

    @Override
    public void addMessage(int i, int i1, String string) throws IOException, AutomationException {
        System.out.println(string);
    }
    
}
