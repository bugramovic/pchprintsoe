/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lu.etat.pch.gis.utils;

import java.io.File;
import java.io.IOException;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author abajramovic
 */
public class IOUtilsTest {

    @Test
    public void testClassPath() throws IOException {
        String OSM_LYR = IOUtils.CLASSPATH_PREFIX + "/layerfiles/OpenStreetMap.lyr";
        Assert.assertTrue( IOUtils.resourceExists( new SOELogger( new SysoutLogger(), 1 ),
                OSM_LYR ) );
        File tmp = File.createTempFile( "OSM", ".lyr" );
        try {
            IOUtils.getResourceAsFile( new SOELogger( new SysoutLogger(), -1 ), OSM_LYR, tmp );
            Assert.assertTrue( tmp.exists() );
            Assert.assertTrue( tmp.length() > 0 );
        } finally {
            tmp.delete();
        }
    }
}
